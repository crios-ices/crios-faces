function []=cost_altimeter(dirModel,dirMat);
%object:       compute or plot the various sea level statistics
%              (std model-obs, model, obs, leading to cost function terms)
%inputs:       dirModel is the model run directory
%              dirMat is the directory where diagnozed .mat files will be saved
%                     -> set it to '' to use the default [dirModel 'mat/']
%              doComp is the switch from computation to plot
%keyboard
doComp=1;
if doComp==1;
    doSave=1;
    doPlot=0;
else;
    doSave=0;
    doPlot=1;
end;
doPrint=0;

%%%%%%%%%%%
%load grid:
%%%%%%%%%%%

%gcmfaces_global;
global mygrid myparms
%if ~isfield(mygrid,'XC'); grid_load('./GRID/',5,'compact'); end;
if ~isfield(mygrid,'XC'); grid_load('./GRID/',5,'compact',2); end;
if ~isfield(mygrid,'LATS_MASKS'); gcmfaces_lines_zonal; end;
if ~isfield(mygrid,'mskC');mskC=mygrid.hFacC;mskC(mskC==0)=NaN;mskC(mskC>0)=1;mygrid.mskC=mskC;end

%%%%%%%%%%%%
%define ASTE
%%%%%%%%%%%%
if(sum(mygrid.ioSize-[270*1350 1])==0);
  use_atn_wetpt=1;
  ioSize=mygrid.ioSize;
  dimSum=2;
  if isfield(myparms,'yearFirst');yrStart=myparms.yearFirst; yrEnd=myparms.yearLast;
  else; 
    error(...
     'inside cost_altimeter: missing myparms.[yearFirst,yearLast], need to run diags_pre_process');
    %yrStart=2002;yrEnd=2013; 
  end;
else;
  dimSum=3;
  yrStart=1992;yrEnd=2011;
end;
    
%%%%%%%%%%%%%%%
%define pathes:
%%%%%%%%%%%%%%%

%search for nctiles files
useNctiles=0;
dirNctiles=[dirModel 'nctiles_remotesensing/sealevel/'];
if ~isempty(dir(dirNctiles)); useNctiles=1; end;

if(dimSum==2);
  %dirSigma='/nobackupp2/atnguye4/llc270/aste_270x450x180/run_template/input_err/';
  dirSigma='/home/atnguyen/llc270/aste_270x450x180/run_template/input_err/';
  nameSigma={'mdt_dtu13_error_m_areascaled_fill9iU42Ef_noStLA.bin',...  %'sigma_mdt.bin',...
             'slaerr_largescale_r2_llc270Am9Eb.err',...
             'slaerr_gridscale_r2_llc270Am9Eb.err'}; 
  maxLatObs=90
else;
nameSigma={'sigma_MDT_glob_eccollc.bin','slaerr_largescale_r5.err','slaerr_gridscale_r5.err'}; maxLatObs=90
dirSigma=dirModel;
if ~isempty(dir([dirModel 'inputfiles/' nameSigma{1}]));
  dirSigma=[dirModel 'inputfiles/'];
end;
end;

dirEcco=dirModel;
if ~isempty(dir([dirModel 'barfiles']));
  dirEcco=[dirModel 'barfiles' filesep];
end;

if isempty(dirMat); dirMat=[dirModel 'mat/']; else; dirMat=[dirMat '/']; end;
if isempty(dir(dirMat));     mkdir([dirMat]); end;
if ~isdir([dirMat 'cost/']); mkdir([dirMat 'cost/']); end;
runName=pwd; tmp1=strfind(runName,filesep); runName=runName(tmp1(end)+1:end);

clear tmp1

%%%%%%%%%%%%%%%%%
%global means:
%%%%%%%%%%%%%%%%%

tmpName=dir([dirEcco 'm_eta_day*data']);
avisoName=which('MSL_aviso.bin');
%atn: change to read in m_eta_day anyway even without avisoName:
%if length(tmpName)==1&~isempty(avisoName);

%atn 9.Jul.2017: using wet pt to save memory
if use_atn_wetpt
  tmpMskC1=convert2gcmfaces(mygrid.mskC(:,:,1));
  tmpRAC  =convert2gcmfaces(mygrid.RAC);
  tmpYC   =convert2gcmfaces(mygrid.YC);
  iwet=find(tmpMskC1==1);Lwet=length(iwet);				%146620
end;

if length(tmpName)==1;

  if (dimSum==3)
    m_eta_day=rdmds2gcmfaces([dirEcco tmpName.name(1:end-5)]);					%gcmfaces 3d, F0
    nt=size(m_eta_day{1},3); 
  elseif(dimSum==2);

%atn: read in in raw binary format to save memory, in addtion: keep only wet point
    nt=tmpName.bytes/mygrid.ioSize(1)/mygrid.ioSize(2)/4;	%assume real*4			%5113
    if ~use_atn_wetpt
      m_eta_day=readbin([dirEcco tmpName.name],[ioSize(1) ioSize(2) nt]);			%[ioSize nt], F0, 8GB
    else;
      m_eta_day=cost_altimeter_read_wet([dirEcco tmpName.name],1:nt);				%[Lwet,nt], F0, 3GB
    end;
  end;
%75sec, 27GB memory total

  avisoName=which('MSL_aviso.bin');
  if ~isempty(avisoName);
    etaglo_aviso=read2memory(avisoName)';		%[1 length], note the prime
  end;

  %global mean
  etaglo=NaN*zeros(1,nt);

  if ~use_atn_wetpt;
    area=mygrid.mskC(:,:,1).*mygrid.RAC; areatot=nansum(area);
  else;
    area=tmpRAC.*tmpMskC1; area=area(iwet);
    areatot=sum(area);%this must not be NaN because mskC1 is already used
  end;

  tic
  for tt=1:nt;
    if(mod(tt,365)==0);fprintf('%i ',tt);end;
    if(dimSum==3);
      etaglo(tt)=nansum(area.*m_eta_day(:,:,tt))/areatot;
    else;
      etaglo(tt)=sum(area.*m_eta_day(:,tt))/areatot;
    end;
  end;
  toc
  fprintf('\n');	%3sec

  %monthly mean
  etaglo_mon=NaN*zeros(1,length(yrStart:yrEnd)*12);
  etaglo_aviso_mon=NaN*zeros(1,length(yrStart:yrEnd)*12);
  etadate=datenum([yrStart 1 1 12 0 0])+[1:nt]-1;
  for mm=1:length(yrStart:yrEnd)*12;
    t0=datenum([yrStart 1+(mm-1) 1 0 0 0]);
    t1=datenum([yrStart 1+mm 1 0 0 0]);
    tt=find(etadate>t0&etadate<t1);
    etaglo_mon(mm)=mean(etaglo(tt));
    if ~isempty(avisoName);
      etaglo_aviso_mon(mm)=mean(etaglo_aviso(tt));
    end;
  end;
  %
  if doSave; eval(['save ' dirMat 'cost/cost_altimeter_etaglo.mat etaglo*;']); end;
end;
clear m_eta_day etaglo_aviso avisoName tmpName nt etaglo area areatot etaglo_mon etaglo_aviso_mon etadate t0 t1 tt %clear F0

%%%%%%%%%%%%%%%%%
%uncertainties:
%%%%%%%%%%%%%%%%%

%these are 2d fields, leave in gcmfaces 2d for now because we're saving them

sig_mdt=NaN*mygrid.RAC;										%gcmfaces, 2d, f1
file0=[dirSigma nameSigma{1}];
if ~isempty(dir(file0)); sig_mdt=read_bin(file0,1,0); end;					%gcmfaces, 2d, f1
%missing field:
if useNctiles; sig_mdt=read_nctiles([dirNctiles 'sealevel'],'mdt_sigma'); end;

sig_sladiff_smooth=NaN*mygrid.RAC;								%gcmfaces, 2d, f2
file0=[dirSigma nameSigma{2}];
if ~isempty(dir(file0)); sig_sladiff_smooth=read_bin(file0,1,0); end;				%gcmfaces, 2d, f2
if useNctiles; sig_sladiff_smooth=read_nctiles([dirNctiles 'sealevel'],'lsc_sigma_r5'); end;

sig_sladiff_point=NaN*mygrid.RAC;								%gcmfaces, 2d, f3
file0=[dirSigma nameSigma{3}];									%gcmfaces, 2d, f3
if ~isempty(dir(file0)); sig_sladiff_point=read_bin(file0,1,0);  end;
if useNctiles; sig_sladiff_point=read_nctiles([dirNctiles 'sealevel'],'point_sigma_r5'); end;

sig_mdt(find(sig_mdt==0))=NaN;									%f1
sig_sladiff_point(find(sig_sladiff_point==0))=NaN;						%f2
sig_sladiff_smooth(find(sig_sladiff_smooth==0))=NaN;						%f3

myflds.sig_mdt=sig_mdt;										%gcmfaces, 2d, f1
myflds.sig_sladiff_point=sig_sladiff_point;							%gcmfaces, 2d, f2
myflds.sig_sladiff_smooth=sig_sladiff_smooth;							%gcmfaces, 2d, f3

clear sig_mdt sig_sladiff_point sig_sladiff_smooth file0;

fprintf('done with init\n');

%%%%%%%%%%%%%%%%%
%do computations:
%%%%%%%%%%%%%%%%%

%do to memory crash, need to use raw vector instead of gcmfaces format, and probably reading in 1yr at a time?
%for doDifObsOrMod=1:3;
for doDifObsOrMod=2:3;	%loop 1

    if doDifObsOrMod==1; suf='modMobs'; elseif doDifObsOrMod==2; suf='obs'; else; suf='mod'; end;
    
    if doComp==0;	%loop 2
        eval(['load ' dirMat 'cost/cost_altimeter_' suf '.mat myflds;']);
    else;
        
%atn: leave in gcmfaces because we're saving:
        %mdt cost function term (misfit plot)
        file0=[dirEcco 'mdtdiff_smooth.data'];	%[ioSize(1) ioSize(2) 1]
        dif_mdt=NaN*mygrid.RAC;
        if ~isempty(dir(file0));dif_mdt=read_bin(file0,1,0);end;
        if useNctiles; dif_mdt=read_nctiles([dirNctiles 'sealevel'],'mdt_misfit'); end;
        %
        myflds.dif_mdt=dif_mdt;									%gcmfaces 2d, f4
        clear dif_mdt file0

        fprintf('done with mdt\n');

%============= sladiff_smooth ========================================================
     if ~useNctiles;        %loop 3: ~useNctiles
        %skip blanks:
        tmp1=dir([dirEcco 'sladiff_smooth*data']);

%this section below seems to be very specific to eccov4, skip for llc270aste:
      if(dimSum==3);
        nrec=tmp1.bytes/90/1170/4;
        ttShift=17;				%skip 1992
        listRecs=[1+365-ttShift:17+365*18-ttShift];
        nRecs=length(listRecs); %TT=yrStart+1+[0:nRecs-1]/365.25;

      elseif(dimSum==2);
%note also: there's a hard-coded number 2002 here, need to be an input params.  
%           Should have had myparms present inside this subroutine
        nrec=tmp1.bytes/mygrid.ioSize(1)/mygrid.ioSize(2)/4;%
        ttShift=0;
        listRecs=1:nrec;
        nRecs=length(listRecs);  %TT=yrStart+[0:nRecs-1]/365.25;
      end;

        tic;

        %pre-load lsc cost function term to add it back to pointwise/1day terms:
        if doDifObsOrMod==1;	%loop 4, doDifObsOrMod
          tmp1=dir([dirEcco 'sladiff_smooth*.data']);	%DIFF
          if(dimSum==3);
            sladiff_smooth=cost_altimeter_read([dirEcco tmp1.name(1:end-5)],listRecs);		%gcmfaces, 3D, F1
          elseif(dimSum==2);
            if use_atn_wetpt 
              sladiff_smooth=cost_altimeter_read_wet([dirEcco tmp1.name],listRecs);		%[Lwet nRecs],F1
            else;
              sladiff_smooth=readbin([dirEcco tmp1.name],[mygrid.ioSize(1),nRecs]);    %<-BUG before: read slaobs, [ioSize nRecs],F1
            end;
          end;
          clear tmp1

        elseif doDifObsOrMod==2;%loop 4, doDifObsOrMod
          tmp1=dir([dirEcco 'slaobs_smooth*.data']);	%OBS
          if(dimSum==3);
            sladiff_smooth=cost_altimeter_read([dirEcco tmp1.name(1:end-5)],listRecs);		%gcmfaces, 3D, F1
          elseif(dimSum==2);
            if use_atn_wetpt
              sladiff_smooth=cost_altimeter_read_wet([dirEcco tmp1.name],listRecs);		%[Lwet nRecs],F1
            else;
              sladiff_smooth=readbin([dirEcco tmp1.name],[mygrid.ioSize(1),nRecs]);		%[ioSize nRecs], F1
            end;
          end;
          clear tmp1
          %63sec, 27GB memory total

        else;			%loop 4, doDifObsOrMod
          tmp1=dir([dirEcco 'sladiff_smooth*.data']);			%DIFF
          tmp2=dir([dirEcco 'slaobs_smooth.data']);			%OBS
          if(dimSum==3);
            sladiff_smooth=cost_altimeter_read([dirEcco tmp1.name(1:end-5)],listRecs);		%gcmfaces, 3D, F1
            sladiff_smooth=sladiff_smooth+cost_altimeter_read([dirEcco tmp2.name(1:end-5)],listRecs);
          elseif(dimSum==2);
            if use_atn_wetpt
              sladiff_smooth=cost_altimeter_read_wet([dirEcco tmp1.name],listRecs);		 %[Lwet nRecs],F1
              sladiff_smooth=sladiff_smooth+cost_altimeter_read_wet([dirEcco tmp2.name],listRecs);
            else;
              sladiff_smooth=readbin([dirEcco tmp1.name],[mygrid.ioSize(1),nRecs]);		 %[ioSize nRecs], F1
              sladiff_smooth=sladiff_smooth+readbin([dirEcco tmp2.name],[mygrid.ioSize(1),nRecs]);
            end;
            clear tmp1 tmp2
          end;
          %27.1GB 1st read, 62sec; 32.6GB 2nd read, 50sec
        end;			%end loop 4, doDifObcsOrMod =1,2,or3
        toc;
     end;%if ~useNctiles;	%end loop 3

     if useNctiles;
        if doDifObsOrMod==1; 
            sladiff_smooth=read_nctiles([dirNctiles 'sealevel'],'lsc_misfit');
        elseif doDifObsOrMod==2;
            sladiff_smooth=read_nctiles([dirNctiles 'sealevel'],'lsc_sla');
        else;
            sladiff_smooth=read_nctiles([dirNctiles 'sealevel'],'lsc_misfit');
            sladiff_smooth=sladiff_smooth+read_nctiles([dirNctiles 'sealevel'],'lsc_sla');
        end;
        nRecs=size(sladiff_smooth{1},3);
        TT=1992+[0:nRecs-1]/365.25;
     end;

        fprintf('done with lsc\n');								%have sladiff_smooth, F1

%=============================================================================
        %pointwise/1day terms:
        if(dimSum==3);
          sladiff_point=repmat(0*mygrid.RAC,[1 1 nRecs]); count_point=sladiff_point;
        elseif(dimSum==2);
          sladiff_point=0.*sladiff_smooth;							%[Lwet nRecs],F2
	  count_point=sladiff_point;								%[Lwet nRecs],F3
        end;
        %33GB memory total
        tic
        for ii=1:3;	%loop 3, ii
            if ii==1; myset='tp'; elseif ii==2; myset='gfo'; else; myset='ers'; end;
            %topex pointwise misfits:

     if ~useNctiles;	%loop 4, ~useNctiles

            if doDifObsOrMod==1;	%loop 5, doDifObsOrMod
              tmp1=dir([dirEcco 'sladiff_' myset '_raw.data']);
              if(dimSum==3);
                sladiff_tmp=cost_altimeter_read([dirEcco tmp1.name(1:end-5)],ttShift+listRecs);	%gcmfaces,3d,F4
              elseif(dimSum==2);
                if use_atn_wetpt
                  sladiff_tmp=cost_altimeter_read_wet([dirEcco tmp1.name],ttShift+listRecs);	%[Lwet nRecs],F4
                else;
                  sladiff_tmp=readbin([dirEcco tmp1.name],[mygrid.ioSize(1) nRecs]);		%[ioSize nRecs],F4
                end;
                clear tmp1
              end;

            elseif doDifObsOrMod==2;	%loop 5, doDifObsOrMod
              tmp1=dir([dirEcco 'slaobs_' myset '_raw.data']);
              if(dimSum==3);
                sladiff_tmp=cost_altimeter_read([dirEcco tmp1.name(1:end-5)],ttShift+listRecs);	%gcmfaces, 3d, F4
              elseif(dimSum==2);
                if use_atn_wetpt
                  sladiff_tmp=cost_altimeter_read_wet([dirEcco tmp1.name],ttShift+listRecs);	%[Lwet nrecs],F4
                else;
                  sladiff_tmp=readbin([dirEcco tmp1.name],[mygrid.ioSize(1) nRecs]);		%[ioSize nRecs],F4
                end;
                clear tmp1
              end;
              %38.2 GB memory total, 60sec to read

            else;			%loop 5, doDifObsOrMod
              tmp1=dir([dirEcco 'sladiff_' myset '_raw.data']);
              tmp2=dir([dirEcco 'slaobs_' myset '_raw.data']);
              if(dimSum==3);
                sladiff_tmp=cost_altimeter_read([dirEcco tmp1.name(1:end-5)],ttShift+listRecs);	%gcmfaces, 3d, F4
                sladiff_tmp=sladiff_tmp+cost_altimeter_read([dirEcco tmp2.name(1:end-5)],ttShift+listRecs);
              elseif(dimSum==2);
                if use_atn_wetpt
                  sladiff_tmp=cost_altimeter_read_wet([dirEcco tmp1.name],ttShift+listRecs);	%[Lwet nRecs],F4
                  sladiff_tmp=sladiff_tmp+cost_altimeter_read_wet([dirEcco tmp2.name],ttShift+listRecs);
                else; 
                  sladiff_tmp=readbin([dirEcco tmp1.name],[mygrid.ioSize(1) nRecs]);		%[ioSize nRecs],F4
                  sladiff_tmp=sladiff_tmp+readbin([dirEcco tmp2.name],[mygrid.ioSize(1) nRecs]);
                end
                clear tmp1 tmp2
              end;
              %38.2GB memory 1st read 60sec, 43.7GB memory 2nd read 50sec
            end;			%loop5, doDifObcsOrMod

            %add back the smoothed values that has been subtracted in cost_gencost_sshv4
            sladiff_smooth_tmp=sladiff_smooth;							%[Lwet nRecs],F5
            sladiff_smooth_tmp(sladiff_tmp==0)=0;						%[Lwet nRecs],F5
            %44.4 GB memory total

            sladiff_tmp=sladiff_tmp+sladiff_smooth_tmp;						%[Lwet nRecs],F4

            clear sladiff_smooth_tmp								%clear F5
            %35.2 GB memory total
     end;%if ~useNctiles;, 		%loop 4

     if useNctiles;
            if doDifObsOrMod==1;
                sladiff_tmp=read_nctiles([dirNctiles 'sealevel'],[myset '_misfit']);
            elseif doDifObsOrMod==2;
                sladiff_tmp=read_nctiles([dirNctiles 'sealevel'],[myset '_sla']);
            else;
                sladiff_tmp=read_nctiles([dirNctiles 'sealevel'],[myset '_misfit']);
                sladiff_tmp=sladiff_tmp+read_nctiles([dirNctiles 'sealevel'],[myset '_sla']);
            end;
            sladiff_tmp(isnan(sladiff_tmp))=0;
     end;

            %add to overall data set:
            sladiff_point=sladiff_point+sladiff_tmp; 						%[Lwet nRecs],F2, tp+gfo+ers
            %43.8 GB memory total
	    count_point=count_point+(sladiff_tmp~=0);						%[Lwet nRecs],F3, tp+gfo+ers
            %no change in memory

            %finalize data set:
            sladiff_tmp(sladiff_tmp==0)=NaN;							%[Lwet nRecs],F4
            %49.8 GB memory total to calc, then falls back to 43.7GB

%-----
            %compute rms:
            count_tmp=sum(~isnan(sladiff_tmp),dimSum); count_tmp(find(count_tmp<10))=NaN; 	%[Lwet 1]
            msk_tmp=1+0*count_tmp;	%atn: not sure why i commented out, now add back in. 9.jul.2017	%[Lwet 1]

            %eval(['myflds.rms_' myset '=sqrt(nansum(sladiff_tmp.^2,3)./count_tmp);']);
            clear temp;temp=sqrt(nansum(sladiff_tmp.^2,dimSum)./count_tmp);			%[Lwet 1]
            %56.1 GB memory total to calc, falls back to 43.7 GB when done
            if(dimSum==2);
              if use_atn_wetpt
                temp1=NaN.*ones(ioSize);temp1(iwet)=temp;temp=temp1;clear temp1;		%ioSize, NaN bc count_tmp
              end;
              temp=convert2gcmfaces(temp);							%gcmfaces, 2d
            end;
            eval(['myflds.rms_' myset '=temp;']);						%gcmfaces, 2d, f5

            %eval(['myflds.std_' myset '=nanstd(sladiff_tmp,0,3).*msk_tmp;']);
            clear temp;temp=nanvar(sladiff_tmp,dimSum);temp=sqrt(temp).*msk_tmp;		%[Lwet 1]
            %56.8 (72% of system!!) GB memory total to calc, falls back to 43.7 GB when done
            if(dimSum==2);
              if use_atn_wetpt
                temp1=NaN.*ones(ioSize);temp1(iwet)=temp;temp=temp1;clear temp1			%ioSize, NaN b/c msk_tmp
              end;
              temp=convert2gcmfaces(temp);							%gcmfaces global
            end;
            eval(['myflds.std_' myset '=temp;']);						%gcmfaces, 2d, f6

            clear sladiff_tmp temp count_tmp msk_tmp						%clear F4
            %falls back to 38.2 GB memory
        end;			%loop 3, ii, tp+gfo+ers
        toc
        %~161 sec
        fprintf('done with point\n');						%have sladiff_point (F2), count_point (F3)
        %at the end of this step: 38.2 GB memory total
        %save([dirMat 'temp_altimeter.mat'],'myflds');
        %writebin([dirMat 'temp_altimeter2.bin'],count_point);
        %writebin([dirMat 'temp_altimeter3.bin'],sladiff_point);

%====================================================================

        %finalize overall data set:
        count_point(count_point==0)=NaN;							%[Lwet nRecs],F3
        %44.4GB memory total
        sladiff_point=sladiff_point./count_point;						%[Lwet nRecs],F2
        %
        msk_point=1*(count_point>0);								%[Lwet nRecs],F6(4), where is this used?
        %44.4GB memory total
        clear count_point									%clear F3
        %back to 38.2 GB memory

        %compute overall rms,std:
        count_tmp=sum(~isnan(sladiff_point),dimSum); count_tmp(find(count_tmp<10))=NaN; 	%[Lwet 1]
        msk_tmp=1+0*count_tmp;									%[Lwet 1]

        clear temp;temp=sqrt(nansum(sladiff_point.^2,dimSum)./count_tmp);			%[Lwet 1]
        %50.4 GB memory, back to 38.2GB when done calc
        if(dimSum==2);
          if use_atn_wetpt
            temp1=NaN.*ones(ioSize);temp1(iwet)=temp;temp=temp1;clear temp1;			%ioSize, NaN b/c count_tmp
          end
          temp=convert2gcmfaces(temp);								%gcmfaces, 2d
        end;
        myflds.rms_sladiff_point=temp;								%gcmfaces, 2d, f7


        %%myflds.std_sladiff_point=nanstd(sladiff_point,0,3).*msk_tmp;
        clear temp;temp=nanvar(sladiff_point,dimSum);temp=sqrt(temp).*msk_tmp;			%[Lwet 1]
        %51.1GB, falls back to 38.2GB when done
        if(dimSum==2);
          if use_atn_wetpt
            temp1=NaN.*ones(ioSize);temp1(iwet)=temp;temp=temp1;clear temp1;			%ioSize, NaN b/c msk_tmp
          end
          temp=convert2gcmfaces(temp);								%gcmfaces, 2d
        end;
        myflds.std_sladiff_point=temp;								%gcmfaces, 2d, f8


        %fill blanks:
        warning('off','MATLAB:divideByZero');
        msk=mygrid.mskC(:,:,1); msk(find(abs(mygrid.YC)>maxLatObs))=NaN;			%global gcmfaces,2d
        myflds.xtrp_rms_sladiff_point=diffsmooth2D_extrap_inv(myflds.rms_sladiff_point,msk);	%gcmfaces, 2d, f9
        myflds.xtrp_std_sladiff_point=diffsmooth2D_extrap_inv(myflds.std_sladiff_point,msk);	%gcmfaces, 2d, f10
        %400% cpu, no jump in memory
        warning('on','MATLAB:divideByZero');

        clear msk temp

     if ~useNctiles;       %loop 3, ~useNctiles 
        %computational mask : only points that were actually observed, and in 35d average
      tmp1=dir([dirEcco 'sladiff_raw.data']);
      if(dimSum==3);
        msk_point35d=cost_altimeter_read([dirEcco tmp1.name(1:end-5)],listRecs);		%gcmfaces, 3d, F7
      elseif(dimSum==2);
        if use_atn_wetpt
          msk_point35d=cost_altimeter_read_wet([dirEcco tmp1.name],listRecs);			%[Lwet nRecs], F7
        else;
          msk_point35d=readbin(tmp1.name,[mygrid.ioSize(1) nRecs]);				%[ioSize nRecs],F7
        end;
        clear tmp1
      end;
      %43.7 GB
        msk_point35d=1*(msk_point35d~=0);							%[Lwet nRecs],F7
        %55.5 GB, falls back to 43.7 GB when done
% commenting out tmp[1,2] to save memory
        if(dimSum==3);
        tmp1=sum(msk_point35d==0&msk_point~=0);
        tmp2=sum(msk_point35d~=0&msk_point~=0);
        fprintf('after masking : %d omitted, %d retained \n',tmp1,tmp2);
        end;
        msk_point=msk_point.*msk_point35d;							%[Lwet nRecs],F6
        clear msk_point35d									%clear F7
        %38.2 GB
     end;%if ~useNctiles; %loop 3

        %plotting mask : regions of less than 100 observations are omitted
        msk_point(msk_point==0)=NaN;								%[Lwet nRecs],F6
        msk_100pts=1*(nansum(msk_point,dimSum)>=100);						%[Lwet 1]
        msk_100pts(msk_100pts==0)=NaN;								%[Lwet 1]
        %50.4GB calc, 38.2GB when done
        %store
        if(dimSum==2);
          if use_atn_wetpt
            tmp=NaN.*ones(ioSize);tmp(iwet)=msk_100pts;msk_100pts=tmp;clear tmp;		%ioSize, NaN b/c of msk_100pts NaN above
          end;
          msk_100pts=convert2gcmfaces(msk_100pts);						%gcmfaces, 2d
        end;
        myflds.msk_100pts=msk_100pts;								%gcmfaces, 2d, f11

        fprintf('done with msk100\n');
        clear msk_100pts tmp
        
%did not do anything to sladiff_smooth, which were already read in above, so now remove read below:
     if ~useNctiles; 		%loop 3
        tic;
      if(dimSum==3);
        %lsc cost function term:
        if doDifObsOrMod==1;
          sladiff_smooth=cost_altimeter_read([dirEcco 'sladiff_smooth'],listRecs);
        elseif doDifObsOrMod==2;
          sladiff_smooth=cost_altimeter_read([dirEcco 'slaobs_smooth'],listRecs);
        else;
          sladiff_smooth=cost_altimeter_read([dirEcco 'sladiff_smooth'],listRecs);
          sladiff_smooth=sladiff_smooth+cost_altimeter_read([dirEcco 'slaobs_smooth'],listRecs);
        end;
      end;%atn: dimSum

        %mask missing points:
        sladiff_smooth(sladiff_smooth==0)=NaN;							%[Lwet nRecs], F1
        sladiff_smooth=sladiff_smooth.*msk_point;						%[Lwet nRecs], F1 (*F6)
        count_tmp=sum(~isnan(sladiff_smooth),dimSum); count_tmp(find(count_tmp<10))=NaN; 	%[Lwet 1]
        msk_tmp=1+0*count_tmp;

        %compute rms:
        rms_sladiff_smooth=msk_tmp.*sqrt(nanmean(sladiff_smooth.^2,dimSum));			%[Lwet 1]
        %50.4GB to calc, 38.2GB when done
        %%std_sladiff_smooth=msk_tmp.*nanstd(sladiff_smooth,0,dimSum);
        temp=msk_tmp.*nanvar(sladiff_smooth,dimSum);temp=sqrt(temp);std_sladiff_smooth=msk_tmp.*temp;%[Lwet 1]
        %51.1GB to calc, 38.2GB when done

        if(dimSum==2);
          if use_atn_wetpt
            temp1=NaN.*ones(ioSize);temp1(iwet)=rms_sladiff_smooth;rms_sladiff_smooth=temp1;	%ioSize, NaN b/c of msk_tmp
            temp1=NaN.*ones(ioSize);temp1(iwet)=std_sladiff_smooth;std_sladiff_smooth=temp1;	%ioSize, NaN b/c of msk_tmp
          end;
          rms_sladiff_smooth=convert2gcmfaces(rms_sladiff_smooth);				%gcmfaces, 2d
          std_sladiff_smooth=convert2gcmfaces(std_sladiff_smooth);				%gcmfaces, 2d
        end;
        
        toc;
        clear count_tmp msk_tmp;
     end;%if ~useNctiles;	%loop 3

     if useNctiles;
        rms_sladiff_smooth=sqrt(nanmean(sladiff_smooth.^2,3));
        std_sladiff_smooth=nanstd(sladiff_smooth,0,3);
     end;

        %store:
        myflds.rms_sladiff_smooth=rms_sladiff_smooth;						%gcmfaces, 2d, f12
        myflds.std_sladiff_smooth=std_sladiff_smooth;						%gcmfaces, 2d, f13

     if ~useNctiles
       clear rms_sladiff_smooth std_sladiff_smooth
     end;
     clear temp temp1										%still have F1, F2, F6
        fprintf('done with rms\n');

%==========================================================================================
        
     if ~useNctiles; 		%loop 3
        tic;
        %pointwise/point35days cost function term:
        if doDifObsOrMod==1;	%loop 4
          tmp1=dir([dirEcco 'sladiff_raw.data']);
          if(dimSum==3);
            sladiff_point35d=cost_altimeter_read([dirEcco tmp1.name(1:end-5)],listRecs);	%gcmfaces 3d nRecs, F8
          elseif(dimSum==2);
            if use_atn_wetpt
              sladiff_point35d=cost_altimeter_read_wet([dirEcco tmp1.name],listRecs);		%[Lwet nRecs], F8
            else;
              sladiff_point35d=readbin([dirEcco tmp1.name],[mygrid.ioSize(1) nRecs]);		%[ioSize nRecs], F8
            end;
          end;
          clear tmp1

        elseif doDifObsOrMod==2;%loop 4
          tmp1=dir([dirEcco 'slaobs_raw.data']);
          if(dimSum==3);
            sladiff_point35d=cost_altimeter_read([dirEcco tmp1.name(1:end-5)],listRecs);	%gcmfaces 3d nRecs, F8
          elseif(dimSum==2);
            if use_atn_wetpt
              sladiff_point35d=cost_altimeter_read_wet([dirEcco tmp1.name],listRecs);		%[Lwet nRecs], F8
            else;
              sladiff_point35d=readbin([dirEcco tmp1.name],[mygrid.ioSize(1) nRecs]);		%[ioSize nRecs], F8
            end;
          end;
          clear tmp1
          %43.7GB, 60sec to read

        else;			%loop 4
          tmp1=dir([dirEcco 'sladiff_raw.data']);
          tmp2=dir([dirEcco 'slaobs_raw.data']);
          if(dimSum==3);
            sladiff_point35d=cost_altimeter_read([dirEcco tmp1.name(1:end-5)],listRecs);	%gcmfaces 3d nRecs, F8
            sladiff_point35d=sladiff_point35d+cost_altimeter_read([dirEcco tmp2.name(1:end-5)],listRecs);
          elseif(dimSum==2);
            if(use_atn_wetpt==1);
              sladiff_point35d=cost_altimeter_read_wet([dirEcco tmp1.name],listRecs);		%[Lwet nRecs], F8
              sladiff_point35d=sladiff_point35d+cost_altimeter_read_wet([dirEcco tmp2.name],listRecs);
            else;
              sladiff_point35d=readbin([dirEcco tmp1.name],[mygrid.ioSize(1) nRecs]);		%[ioSize nRecs], F8
              sladiff_point35d=sladiff_point35d+readbin([dirEcco tmp2.name],[mygrid.ioSize(1) nRecs]);
            end;
          end;
          clear tmp1 tmp2
        end;			%end loop 4

        %mask missing points:
        sladiff_point35d(sladiff_point35d==0)=NaN;						%[Lwet nRecs], F8
        %47.5GB to calc, 43.7GB when done
        sladiff_point35d=sladiff_point35d.*msk_point;						%[Lwet nRecs], F8 (need F6)
        clear msk_point;			 						%clear F6
        %38.2GB
        count_tmp=sum(~isnan(sladiff_point35d),dimSum); count_tmp(find(count_tmp<10))=NaN; msk_tmp=1+0*count_tmp;%[Lwet 1]

        %compute rms:
        rms_sladiff_point35d=msk_tmp.*sqrt(nanmean(sladiff_point35d.^2,dimSum));		%[Lwet 1] or gcmfaces 2d
        %50.5GB to calc, 38.2GB when done
        %std_sladiff_point35d=msk_tmp.*nanstd(sladiff_point35d,0,dimSum);
        temp=nanvar(sladiff_point35d,dimSum);temp=sqrt(temp);std_sladiff_point35d=msk_tmp.*temp;%[Lwet 1] or gcmfaces 2d
        %51.1GB to calc, 38.2GB when done

        if(dimSum==2);
          if(use_atn_wetpt==1);
            temp1=NaN.*ones(ioSize);temp1(iwet)=rms_sladiff_point35d;rms_sladiff_point35d=temp1;%ioSize, NaN b/c msk_tmp
            temp1=NaN.*ones(ioSize);temp1(iwet)=std_sladiff_point35d;std_sladiff_point35d=temp1;%ioSize, NaN b/c msk_tmp
          end;
          rms_sladiff_point35d=convert2gcmfaces(rms_sladiff_point35d);				%gcmfaces, 2d
          std_sladiff_point35d=convert2gcmfaces(std_sladiff_point35d);				%gcmfaces, 2d
        end;

        %store:
        myflds.rms_sladiff_point35d=rms_sladiff_point35d;					%gcmfaces, 2d, f14
        myflds.std_sladiff_point35d=std_sladiff_point35d;					%gcmfaces, 2d, f15

        clear rms_sladiff_point35d std_sladiff_point35d count_tmp temp temp1 msk_point		%still have F1, F2, F8
        
        if 0;			%loop 4
          fprintf('inside strange zero loop\n');
            %difference between scales
            rms_sladiff_35dMsmooth=msk_tmp.*sqrt(nanmean((sladiff_point35d-sladiff_smooth).^2,dimSum));%[Lwet 1] (F8-F1)
            temp=nanvar((sladiff_point35d-sladiff_smooth),dimSum);temp=sqrt(temp);std_sladiff_35dMsmooth=msk_tmp.*temp;%[Lwet 1]
            %std_sladiff_35dMsmooth=msk_tmp.*nanstd((sladiff_point35d-sladiff_smooth),0,dimSum);
            %store:
            if(dimSum==2);
              if(use_atn_wetpt==1);
                temp1=NaN.*ones(ioSize);temp1(iwet)=rms_sladiff_35dMsmooth;rms_sladiff_35dMsmooth=temp1;%ioSize, NaN bc msk_tmp
                temp1=NaN.*ones(ioSize);temp1(iwet)=std_sladiff_35dMsmooth;std_sladiff_35dMsmooth=temp1;%ioSize, NaN bc msk_tmp
              end;
              rms_sladiff_35dMsmooth=convert2gcmfaces(rms_sladiff_35dMsmooth);			%gcmfaces, 2d
              std_sladiff_35dMsmooth=convert2gcmfaces(std_sladiff_35dMsmooth);			%gcmfaces, 2d
            end;
            myflds.rms_sladiff_35dMsmooth=rms_sladiff_35dMsmooth;				%gcmfaces, 2d, f16
            myflds.std_sladiff_35dMsmooth=std_sladiff_35dMsmooth;				%gcmfaces, 2d, f17

            %difference between scales
            rms_sladiff_pointMpoint35d=msk_tmp.*sqrt(nanmean((sladiff_point-sladiff_point35d).^2,dimSum));%[Lwet 1], F2-F8
            temp=nanvar((sladiff_point-sladiff_point35d),dimSum);temp=sqrt(temp);std_sladiff_pointMpoint35d=msk_tmp.*temp;%F2-F8
            %std_sladiff_pointMpoint35d=msk_tmp.*nanstd((sladiff_point-sladiff_point35d),0,dimSum);
            %store:
            if(dimSum==2);
              if(use_atn_wetpt==1);
                temp1=NaN.*ones(ioSize);temp1(iwet)=rms_sladiff_pointMpoint35d;rms_sladiff_pointMpoint35d=temp1;%ioSize
                temp1=NaN.*ones(ioSize);temp1(iwet)=std_sladiff_pointMpoint35d;std_sladiff_pointMpoint35d=temp1;%ioSize
              end;
              rms_sladiff_pointMpoint35d=convert2gcmfaces(rms_sladiff_pointMpoint35d);		%gcmfaces, 2d
              std_sladiff_pointMpoint35d=convert2gcmfaces(std_sladiff_pointMpoint35d);		%gcmfaces, 2d
            end;
            myflds.rms_sladiff_pointMpoint35d=rms_sladiff_pointMpoint35d;			%gcmfaces, 2d, f18
            myflds.std_sladiff_pointMpoint35d=std_sladiff_pointMpoint35d;			%gcmfaces, 2d, f19
        end;	%strange zero loop 4
        toc;
        clear msk_tmp temp rms_sladiff_35dMsmooth std_sladiff_35dMsmooth sladiff_point35d sladiff_smooth sladiff_point temp1 %F1,F2,F8
        %21.5GB memory
     end;%if ~useNctiles;	%loop 3

     if useNctiles;
            %store:
            myflds.rms_sladiff_point35d=NaN*rms_sladiff_smooth;
            myflds.std_sladiff_point35d=NaN*std_sladiff_smooth;
     end;
        
     %save([dirMat 'temp_altimeter.mat'],'myflds');

        %compute zonal mean/median:		(all gcmfaces, 2d)
        for ii=1:4;		%loop 3, ii
            switch ii;
                case 1; tmp1='mdt'; cost_fld=(mygrid.mskC(:,:,1).*myflds.dif_mdt./myflds.sig_mdt).^2;
                case 2; tmp1='lsc'; cost_fld=(mygrid.mskC(:,:,1).*myflds.rms_sladiff_smooth./myflds.sig_sladiff_smooth).^2;
                case 3; tmp1='point35d'; cost_fld=(mygrid.mskC(:,:,1).*myflds.rms_sladiff_point35d./myflds.sig_sladiff_point).^2;
                case 4; tmp1='point'; cost_fld=(mygrid.mskC(:,:,1).*myflds.rms_sladiff_point./myflds.sig_sladiff_point).^2;
            end;
            cost_zmean=calc_zonmean_T(cost_fld);     eval(['mycosts_mean.' tmp1 '=cost_zmean;']);
            cost_zmedian=calc_zonmedian_T(cost_fld); eval(['mycosts_median.' tmp1 '=cost_zmedian;']);
            clear cost_zmean cost_zmedian cost_fld
        end;			%end loop 3, ii

        fprintf(['done with ' suf '\n']);
        
        %write to disk:
        if doSave; eval(['save ' dirMat 'cost/cost_altimeter_' suf '.mat myflds mycosts_mean mycosts_median;']); end;
        
        clear  mycosts_mean mycosts_median
    end;%if doComp, loop 2
    
    if doPlot;
        cc=[-0.4:0.05:-0.25 -0.2:0.03:-0.05 -0.03:0.01:0.03 0.05:0.03:0.2 0.25:0.05:0.4];
        figure; m_map_gcmfaces(myflds.dif_mdt,0,{'myCaxis',cc}); drawnow;
        cc=[0:0.005:0.02 0.03:0.01:0.05 0.06:0.02:0.1 0.14:0.03:0.2 0.25:0.05:0.4];
        figure; m_map_gcmfaces(myflds.rms_sladiff_smooth,0,{'myCaxis',cc}); drawnow;
        figure; m_map_gcmfaces(myflds.rms_sladiff_point35d,0,{'myCaxis',cc}); drawnow;
        figure; m_map_gcmfaces(myflds.rms_sladiff_point,0,{'myCaxis',cc}); drawnow;
    end;
    
    if doPlot&doPrint;
        dirFig='../figs/altimeter/'; ff0=gcf-4;
        for ff=1:4;
            figure(ff+ff0); saveas(gcf,[dirFig runName '_' suf num2str(ff)],'fig');
            eval(['print -depsc ' dirFig runName '_' suf num2str(ff) '.eps;']);
            eval(['print -djpeg90 ' dirFig runName '_' suf num2str(ff) '.jpg;']);
        end;
    end;
    
end;%for doDifObsOrMod=1:3;, loop 1



function [fldOut]=cost_altimeter_read(fileIn,recIn);

nrec=length(recIn);
global mygrid;% siz=[size(convert2gcmfaces(mygrid.XC)) nrec];
siz=[mygrid.ioSize nrec];
lrec=siz(1)*siz(2)*4;
myprec='float32';

fldOut=zeros(siz);
fid=fopen([fileIn '.data'],'r','b');
for irec=1:nrec;
    status=fseek(fid,(recIn(irec)-1)*lrec,'bof');
    fldOut(:,:,irec)=fread(fid,siz(1:2),myprec);
end;

fldOut=convert2gcmfaces(fldOut);


function [fldOut]=cost_altimeter_read_wet(fileIn,recIn);
%75sec to read 5113 rec
nrec=length(recIn);
global mygrid;
myprec='real*4';

ioSize=mygrid.ioSize;
tmpMskC=convert2gcmfaces(mygrid.mskC(:,:,1));
iwet=find(tmpMskC(:)==1); Lwet=length(iwet);

fldOut=NaN.*ones(Lwet,nrec);
fprintf('cost_altimeter: cost_altimeter_read_wet, length= %i\n',nrec);
fprintf('%s\n',fileIn);
tic
for it=1:nrec
  tmp=squeeze(read_slice(fileIn,ioSize(1),ioSize(2),recIn(it),myprec));
  fldOut(:,it)=tmp(iwet);
  if(mod(it,365)==0);fprintf('%i ',it);end;
end;
toc
fprintf('\n');

