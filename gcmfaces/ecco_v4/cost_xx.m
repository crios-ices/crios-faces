function []=cost_xx(dirModel,dirMat,doComp,dirTex,nameTex);
%object:	compute cost function term for atmospheric controls
%inputs:	dirModel is the model directory
%               dirMat is the directory where diagnozed .mat files will be saved
%                     -> set it to '' to use the default [dirModel 'mat/']
%		doComp is a switch (1->compute; 0->display)
%optional:      dirTex is the directory where tex and figures files are created
%                 (if not specified then display all results to screen instead)
%               nameTex is the tex file name (default : 'myPlots')

if isempty(dirModel); dirModel=[dirMat '../'];end;
if isempty(dirMat); dirMat=[dirModel 'mat' filesep]; else; dirMat=[dirMat filesep]; end;
if isempty(dir(dirMat));     mkdir([dirMat]); end;

%determine if and where to create tex and figures files
dirMat=[dirMat filesep];
if isempty(who('dirTex'));
  addToTex=0;
else;
  if ~ischar(dirTex); error('mis-specified dirTex'); end;
  addToTex=1;
  if isempty(who('nameTex')); nameTex='myPlots'; end;
  fileTex=[dirTex filesep nameTex '.tex'];
end;

for iloop=1:2
  if iloop==1; jxx=7;else;jxx=5;end;
  for ii=1:jxx;
    if(iloop==1);
      switch ii;
        case 1; xxName='atemp'; nameWeight='wTair_Ev2linearcapsmm9Eb_MezenB.bin'; cc=2; uni='K';
        case 2; xxName='aqh';   nameWeight='wQair_Ev2linearcapsmm9Eb_MezenB.bin'; cc=2; uni='g/kg';
        case 3; xxName='uwind'; nameWeight='wUwind_Ev2linearcapsmm9Eb_MezenB.bin'; cc=0.5; uni='m/s';
        case 4; xxName='vwind'; nameWeight='wVwind_Ev2linearcapsmm9Eb_MezenB.bin'; cc=0.5; uni='m/s';
        case 5; xxName='lwdown'; nameWeight='wLwDn_Ev2linearcapsmm9Eb_MezenB.bin'; cc=20; uni='W/m2';
        case 6; xxName='swdown'; nameWeight='wSwDn_Ev2linearcapsmm9Eb_MezenB.bin'; cc=40; uni='W/m2';
        case 7; xxName='precip'; nameWeight='wRain_Ev2linearcapsmm9Eb_MezenB.bin';cc=2.5e-8; uni='m/s';
      end;
    else;
      switch ii;
        case 1; xxName='theta';  nameWeight='weight_T_mad_feb2013_mod01DpClim_llc270m9Eb.bin';cc=0.05; uni='C';
        case 2; xxName='salt';   nameWeight='weight_S_mad_feb2013_mod01DpClim_llc270m9Eb.bin';cc=0.01; uni='psu';
        case 3; xxName='diffkr' ;nameWeight='weight_diffkr_basin_v1m9EfC.bin'; cc=1e-6; uni='m2/s';
        case 4; xxName='kapredi';nameWeight='weight_kapredi.bin';              cc=5.0;  uni='m2/s';
        case 5; xxName='kapgm'  ;nameWeight='weight_kapgm.bin';                cc=20.0; uni='m2/s';

      end;
    end;

      gcmfaces_global;
      if ~isfield(mygrid,'XC'); grid_load('./GRID/',5,'compact'); end;
      if ~isfield(mygrid,'LATS_MASKS'); gcmfaces_lines_zonal; end;
        %dirWei='/home/atnguyen/llc270/aste_270x450x180/run_template/input_weight/';
        dirWei='/scratch/atnguyen/aste_270x450x180/run_template/input_weight/';

        if ~isempty(dir([dirModel 'xx_' xxName '.effective.*data']));
          dirXX=dirModel;
        else;
          dirXX=[dirModel 'ADXXfiles' filesep];
        end;
        use_atn_readcompact=1;
        use_atn_wetpt=1;
        if use_atn_readcompact
          ioSize=mygrid.ioSize;
          nx=mygrid.facesExpand(1);
          ny=prod(ioSize)/nx;
          nz=length(mygrid.RC);
          if use_atn_wetpt
            msk2d=convert2gcmfaces(mygrid.hFacC(:,:,1));iwet2d=find(msk2d(:)>0);Lwet2d=length(iwet2d);
          end;
        end

    if doComp;

%load grid
      %gcmfaces_global;
      %if ~isfield(mygrid,'XC'); grid_load('./GRID/',5,'compact'); end;
      %if ~isfield(mygrid,'LATS_MASKS'); gcmfaces_lines_zonal; end;

%read model cost output
      if iloop==1 ;

        %dirWei='/home/atnguyen/llc270/aste_270x450x180/run_template/input_weight/';

        %if ~isempty(dir([dirModel 'xx_' xxName '.effective.*data']));
        %  dirXX=dirModel;
        %else;
        %  dirXX=[dirModel 'ADXXfiles' filesep];
        %end;

        %use_atn_readcompact=1;
        %use_atn_wetpt=1;
        %if use_atn_readcompact
        %  ioSize=mygrid.ioSize;
        %  nx=mygrid.facesExpand(1);
        %  ny=prod(ioSize)/nx;
        %  nz=length(mygrid.RC);
        %  if use_atn_wetpt
        %    msk2d=convert2gcmfaces(mygrid.hFacC(:,:,1));iwet2d=find(msk2d(:)>0);Lwet2d=length(iwet2d);
        %  end;
        %end

        tmp1=dir([dirXX 'xx_' xxName '.effective.*data']);
        nt=tmp1.bytes/ioSize(1)/ioSize(2)/4;	%assume real4

%below is the default way to read everything at once into memory, then convert to global
        if ~use_atn_readcompact
          tmp2=size(convert2gcmfaces(mygrid.XC)); 
          fld_xx=read2memory([dirXX tmp1.name],[tmp2 tmp1.bytes/tmp2(1)/tmp2(2)/4]);

          fld_wei=read_bin([dirWei nameWeight],1,0);
          jj=find(fld_wei==0);fld_sig=1./sqrt(fld_wei);fld_sig(jj)=0;

          if strcmp(xxName,'aqh'); fld_xx=fld_xx*1000; fld_sig=fld_sig*1000; end;

          fld_rms=sqrt(mean(fld_xx.^2,3));
          fld_mean=mean(fld_xx,3);
          fld_std=std(fld_xx,[],3);
        else;%use_atn_readcompact
%now use read_slice to reduce memory footprint as follows:

          if use_atn_wetpt
            fld_xx_mean=zeros(Lwet2d,1);
          else
            fld_xx_mean=zeros(ioSize);
          end;
          fld_xt_mean=fld_xx_mean;
          fld_xx_sq  =fld_xx_mean;
          fld_t_mean =fld_xx_mean;
          fld_t_sq   =fld_xx_mean;
          dt=2*7*24*3600;	%two weeks, from data.ctrl
          fprintf('read %s ',tmp1.name);fprintf(' of length: %i;',nt);
          for j=1:nt;
            tmp2=read_slice([dirXX tmp1.name],ioSize(1),ioSize(2),j);
            if j==1
              fld_wei=readbin([dirWei nameWeight],[ioSize(1) ioSize(2) 1]);
              fld_sig=0.*fld_wei;jj=find(fld_wei==0);fld_sig=1./sqrt(fld_wei);fld_sig(jj)=0;
            end;
            if use_atn_wetpt;
              if strcmp(xxName,'aqh'); tmp2=tmp2.*1000;       
                if(j==1);fld_sig=fld_sig.*1000;end;
              end;
              fld_xx_mean=fld_xx_mean+tmp2(iwet2d);
              fld_xx_sq  =fld_xx_sq  +tmp2(iwet2d).^2;
              if(j==1);fld_sig=fld_sig(iwet2d);end;
              fld_xt_mean=fld_xt_mean+(j-1)*dt*tmp2(iwet2d);		%cross term
              fld_t_mean =fld_t_mean+(j-1)*dt.*ones(Lwet2d,1);
              fld_t_sq   =fld_t_sq  +((j-1)*dt).^2.*ones(Lwet2d,1);
            else;
                %fld_xx(:,:,j)=tmp2;
              fld_xx_mean=fld_xx_mean+tmp2;
              fld_xx_sq  =fld_xx_sq  +tmp2.^2;
              fld_xt_mean=fld_xt_mean+ (j-1)*dt*tmp2;			%cross term
              fld_t_mean =fld_t_mean + (j-1)*dt.*ones(ioSize);
              fld_t_sq   =fld_t_sq   +((j-1)*dt).^2.*ones(ioSize);
            end;
            if(mod(j,50)==0);fprintf('%i ',j);end;
          end;%jloop
          fprintf('\n');

%compute xx stats
          fld_mean   =fld_xx_mean./nt;
          fld_std    =sqrt((fld_xx_sq - nt.*fld_mean.^2)./(nt-1));
          fld_rms    =sqrt(fld_xx_sq./nt);
          det        =fld_t_sq.*nt - fld_t_mean.^2;
          fld_slo    =(fld_xt_mean.*nt - fld_xx_mean.*fld_t_mean)./det;
          fld_int    =(fld_t_sq.*fld_xx_mean - fld_t_mean.*fld_xt_mean)./det;
          if use_atn_wetpt
            tmp=nan(nx,ny,1);tmp(iwet2d)=fld_mean;fld_mean=tmp; clear tmp
            tmp=nan(nx,ny,1);tmp(iwet2d)=fld_std; fld_std =tmp; clear tmp
            tmp=nan(nx,ny,1);tmp(iwet2d)=fld_rms; fld_rms =tmp; clear tmp
            tmp=nan(nx,ny,1);tmp(iwet2d)=fld_sig; fld_sig =tmp; clear tmp
            tmp=nan(nx,ny,1);tmp(iwet2d)=fld_slo; fld_slo =tmp; clear tmp
            tmp=nan(nx,ny,1);tmp(iwet2d)=fld_int; fld_int =tmp; clear tmp
          end;
%now convert back to gcmfaces:
          fld_mean=convert2gcmfaces(fld_mean);
          fld_std =convert2gcmfaces(fld_std);
          fld_rms =convert2gcmfaces(fld_rms);
          fld_sig =convert2gcmfaces(fld_sig);
          fld_slo =convert2gcmfaces(fld_slo);
          fld_int =convert2gcmfaces(fld_int);
        end;	%use_atn_compact

%mask
        fld_rms=fld_rms.*mygrid.mskC(:,:,1); 
        fld_sig=fld_sig.*mygrid.mskC(:,:,1);
        fld_mean=fld_mean.*mygrid.mskC(:,:,1);
        fld_std=fld_std.*mygrid.mskC(:,:,1);
        fld_slo=fld_slo.*mygrid.mskC(:,:,1);
        fld_int=fld_int.*mygrid.mskC(:,:,1);

        clear fld_xx;
        clear fld_xx* fld_wei fld_t* fld_xt*;

        if ~isdir([dirMat 'cost/']); mkdir([dirMat 'cost/']); end;
        eval(['save ' dirMat '/cost/cost_xx_' xxName '.mat fld_* cc uni;']);
      end	%iloop

    else;%display previously computed results

      global mygrid;

      if iloop==1
        if isdir([dirMat 'cost/']); dirMat=[dirMat 'cost/']; end;

        eval(['load ' dirMat '/cost_xx_' xxName '.mat;']);

        figure(1);clf; 
        m_map_gcmfaces(fld_sig,0,{'myCaxis',[0:0.05:0.5 0.6:0.1:1 1.25]*cc});
        myCaption={['prior uncertainty -- ' xxName ' (' uni ')']};
        if addToTex; write2tex(fileTex,2,myCaption,gcf); end;

        figure(1);clf; 
        m_map_gcmfaces(fld_rms,0,{'myCaxis',[0:0.05:0.5 0.6:0.1:1 1.25]*cc});
        myCaption={['rms adjustment -- ' xxName ' (' uni ')']};
        if addToTex; write2tex(fileTex,2,myCaption,gcf); end;

        figure(1);clf;
        m_map_gcmfaces(fld_std,0,{'myCaxis',[0:0.05:0.5 0.6:0.1:1 1.25]*cc});
        myCaption={['std adjustment -- ' xxName ' (' uni ')']};
        if addToTex; write2tex(fileTex,2,myCaption,gcf); end;

        figure(1);clf;
        m_map_gcmfaces(fld_mean,0,{'myCaxis',[-0.5:0.05:0.5]*cc});
        myCaption={['mean adjustment -- ' xxName ' (' uni ')']};
        if addToTex; write2tex(fileTex,2,myCaption,gcf); end;

        figure(1);clf;
        convfac=24*3600*365.25;	%sec/yr
        m_map_gcmfaces(fld_slo.*convfac,0,{'myCaxis',[-0.05:0.005:0.05]*cc});
        myCaption={['slope adjustment -- ' xxName ' (' uni '/yr)']};
        if addToTex; write2tex(fileTex,2,myCaption,gcf); end;

        figure(1);clf;
        m_map_gcmfaces(fld_int,0,{'myCaxis',[-0.5:0.05:0.5]*cc});
        myCaption={['intercept adjustment -- ' xxName ' (' uni ')']};
        if addToTex; write2tex(fileTex,2,myCaption,gcf); end;

      else	%iloop==2, 3d fields, read in right here to plot
        klev=[1:5:40];
        tmp1=dir([dirXX 'xx_' xxName '.*data']);
        fld_xx=read_slice([dirXX tmp1(1).name],ioSize(1),ioSize(2),klev);
        fld_xx=convert2gcmfaces(fld_xx).*mygrid.mskC(:,:,klev);

        fld_sig=read_slice([dirWei nameWeight],ioSize(1),ioSize(2),klev);
        jj=find(fld_sig==0);fld_sig=1./sqrt(fld_sig);fld_sig(jj)=0;
        fld_sig=convert2gcmfaces(fld_sig).*mygrid.mskC(:,:,klev);

        rC=abs(round(squeeze(mygrid.RC)));
 
        for k=1:length(klev)
          tmp=fld_xx(:,:,k).*fld_sig(:,:,k);
          figure(1);clf;
          m_map_gcmfaces(tmp,0,{'myCaxis',[-1.0:0.1:1.0]*cc});
          myCaption={['adjustment -- ' xxName ' z(k=' num2str(klev(k)) ')=' num2str(rC(klev(k))) 'm (' uni ')']};
          if addToTex; write2tex(fileTex,2,myCaption,gcf); end;
        end

      end	%iloop==1|iloop==2
    end;	%doComp
  end;%for ii=1:6;
end;%iloop
