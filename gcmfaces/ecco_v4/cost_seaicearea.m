function []=cost_seaicearea(dirModel,dirMat,doComp,dirTex,nameTex);
%object:        compute cost function term for sea ice data
%inputs:        dimodel is the model directory
%               dirMat is the directory where diagnozed .mat files will be saved
%                     -> set it to '' to use the default [dirModel 'mat/']
%               doComp is a switch (1->compute; 0->display)
%optional:      dirTex is the directory where tex and figures files are created
%                 (if not specified then display all results to screen instead)
%               nameTex is the tex file name (default : 'myPlots')

%atn 06.Jun.2016: after investigation, if daily, best to get rid of leap-year extra day

if isempty(dirMat); dirMat=[dirModel 'mat' filesep]; else; dirMat=[dirMat filesep]; end;
if isempty(dir(dirMat));     mkdir([dirMat]); end;

%determine if and where to create tex and figures files
dirMat=[dirMat filesep];
if isempty(who('dirTex'));
  addToTex=0;
else;
  if ~ischar(dirTex); error('mis-specified dirTex'); end;
  addToTex=1;
  if isempty(who('nameTex')); nameTex='myPlots'; end;
  fileTex=[dirTex filesep nameTex '.tex'];
end;

%grid, params and inputs
gcmfaces_global; global myparms;
%if ~isfield(mygrid,'XC'); grid_load('./GRID/',5,'compact'); end;
if ~isfield(mygrid,'XC'); grid_load('./GRID/',5,'compact',2); end;
if ~isfield(mygrid,'LATS_MASKS'); gcmfaces_lines_zonal; end;
if ~isfield(mygrid,'mskC');mskC=mygrid.hFacC;mskC(mskC==0)=NaN;mskC(mskC>0)=1;mygrid.mskC=mskC;end
if isfield(myparms,'yearsta'); yearsta=myparms.yearsta; yearend=myparms.yearend;
else; yearsta=myparms.yearFirst;yearend=myparms.yearLast;%;1992; yearend=2011;
end;

%%%%%%%%%%%%
%define ASTE
%%%%%%%%%%%%
if(sum(mygrid.ioSize-[270*1350 1])==0);
  use_atn_wetpt=1;
  ioSize=mygrid.ioSize;
  dimSum=2;
  if isfield(myparms,'yearFirst');  yearsta=myparms.yearFirst; yearend=myparms.yearLast;
  elseif isfield(myparms,'yearsta'); yearsta=myparms.yearsta; yearend=myparms.yearend;
  else;
    error(...
     'inside cost_altimeter: missing myparms.[yearFirst,yearLast], need to run diags_pre_process');
    %yrStart=2002;yrEnd=2013; 
  end;
  get_southern=0;
else;
  dimSum=3;
  yrStart=1992;yrEnd=2011;
  get_southern=1;
end;

if(get_southern==1);
  [lonPairs,latPairs,names] = line_greatC_TUV_MASKS_core2_antarctic;
  lonLims=[lonPairs(1:5,1);lonPairs(1,1)];
end;
   lonLims=[-180,-155,-80,-45,20,145,180]';	%Chukchi,CB+CAA,LAB,GS,Eu,Makarov

%integrals :
nlon=(length(lonLims)-1);			%6 regions
areamskN=NaN*repmat(mygrid.RAC,[1 1 nlon+1]);	%gcmfaces3d,6
areamskN(:,:,1)=mygrid.RAC.*(mygrid.YC>0);	%northern
if(get_southern==1);
  areamskS=NaN*repmat(mygrid.RAC,[1 1 nlon+1]);	%gcmfaces3d
  areamskS(:,:,1)=mygrid.RAC.*(mygrid.YC<0);
end;
for kk=1:nlon;
  tmpmsk=0.*mygrid.XC;
  if lonLims(kk+1) > lonLims(kk)
    tmpmsk(find(mygrid.XC >= lonLims(kk) & mygrid.XC < lonLims(kk+1)))=1.;
  else
    tmpmsk(find(mygrid.XC >= lonLims(kk) & mygrid.XC <= 180.))=1.;
    tmpmsk(find(mygrid.XC >= -180. & mygrid.XC < lonLims(kk+1)))=1.;
  end
  areamskN(:,:,1+kk)=mygrid.RAC.*tmpmsk.*(mygrid.YC>0);	%gcmfaces3d, 6
  if(get_southern==1);areamskS(:,:,1+kk)=mygrid.RAC.*tmpmsk.*(mygrid.YC<0);end;
end;

%search for nctiles files
useNctiles=0;
dirNctiles=[dirModel 'nctiles_remotesensing/sic/'];
if ~isempty(dir(dirNctiles)); useNctiles=1; end;

if doComp;
%grid, params and inputs
if(dimSum==3);
  fld_err=0.5*ones(90,1170);
elseif(dimSum==2);
  n1=ioSize(1);n2=ioSize(2);
  fld_err=ones(n1,n2);						%ioSize
end;
fld_err=convert2gcmfaces(fld_err);				%gcmfaces2d
if useNctiles; fld_err=read_nctiles([dirNctiles 'sic'],'sic_sigma'); end;
%fld_w=fld_err.^-2;	%comment out, not used

dirEcco=dirModel;
if ~isempty(dir([dirModel 'barfiles']));
  dirEcco=[dirModel 'barfiles' filesep];
end;

%nameData='nsidc79_monthly_';
%dirData=dirModel;
%if ~isempty(dir([dirModel 'inputfiles/' nameData '*']));
%  dirData=[dirModel 'inputfiles/'];
%end;

fileModel='unknown';
file0=[dirEcco 'm_siv4_area*data'];
if ~isempty(dir(file0)); fileModel=dir(file0); fileModel=fileModel.name; end;

nyears=yearend-yearsta+1;
nmonths=12*nyears;				%168
%temp=dir([dirModel fileModel]);nrec=temp.bytes/n1/n2/4;clear temp
temp=dir([dirEcco fileModel]);nrec=temp.bytes/n1/n2/4;clear temp

flag_monthly=1;
if(floor(nrec/nyears)==365);flag_monthly=0;end;

if ~useNctiles;
%misfits :

%if(flag_monthly==1);
if (dimSum==3);
  fld_estim=convert2gcmfaces(NaN*ones(n1,n2,nmonths));		%gcmfaces3d,168,F1
  fld_nsidc=convert2gcmfaces(NaN*ones(n1,n2,nmonths));		%gcmfaces3d,168,F2
  fld_dif=convert2gcmfaces(NaN*ones(n1,n2,nmonths));		%gcmfaces3d,168,F3
  %dirData='/nobackupp2/atnguye4/llc270/aste_270x450x180/run_template/input_nsidc_all/';
  dirData='/home/atnguyen/llc270/aste_270x450x180/run_template/input_nsidc_all/';
  nameData='NSIDC0079_v2_monthly_';
  %mmm=12;
  mskC=mygrid.mskC(:,:,1);
  RAC=mygrid.RAC;
  YC=mygrid.YC;
%elseif(flag_monthly==0);
elseif(dimSum==2);
  %dirData='/nobackupp2/atnguye4/llc270/aste_270x450x180/run_template/input_icon_reproc/';
  dirData='/home/atnguyen/llc270/aste_270x450x180/run_template/input_icon_reproc/';
  nameData='ICECONC_v1p1r1_daily_';
  %mmm=365;
  lat_cut=55;
  mskC=squeeze(convert2gcmfaces(mygrid.mskC(:,:,1)));		%ioSize
  RAC=squeeze(convert2gcmfaces(mygrid.RAC));			%ioSize
  YC=squeeze(convert2gcmfaces(mygrid.YC));			%ioSize
  %iwet=find(YC>lat_cut);					%154710	, 15yrs = 3.4GB/fld
  %iwet=find(mskC==1);						%146620 , 15yrs = 3.2GB/fld
  if use_atn_wetpt
    iwet=find(YC>lat_cut & mskC==1);				%80595 !!, yay, 15yrs = 1.8GB/fld
  else
    iwet=1:ioSize(1);%1:n1*n2;					%364500
  end
  Lwet=length(iwet);
  areamskN=squeeze(convert2gcmfaces(areamskN));areamskN=areamskN(iwet,:);	%[Lwet,6]
  mskC=mskC(iwet);
  RAC=RAC(iwet);
  YC=YC(iwet);
  %ileap=leapyear(yearsta:yearend);
  yr=yearsta:yearend;
  for iyr=1:length(yr);
    nday=datenum(yr(iyr),12,31)-datenum(yr(iyr),1,0);
    ileap(iyr)=(nday==366);
  end;
  fld_estim1=NaN*ones(Lwet,nrec);				%[Lwet nrec],F4 , 3.4GB|1.8GB
  fld_nsidc1=NaN*ones(Lwet,nrec);				%[Lwet nrec],F5 , 3.4GB|1.8GB
  fld_dif1  =NaN*ones(Lwet,nrec);				%[Lwet nrec],F6 , 3.4GB|1.8GB
end;

%computational loop :
%keyboard
%entering loop below; 30.1GB during each year, 11sec loading
mm_yrprev=0;mmm=0;
for ycur=yearsta:yearend;
 fprintf('%i ',ycur);tic;
 file0=[dirData nameData num2str(ycur)];
 temp=dir(file0);
 mm_yrprev=mm_yrprev+mmm;
 mmm=temp.bytes/n1/n2/4;		%either 12 or 365 or 366
 for mcur=1:mmm;			%mcur: 1:12 or 1:365(366)
  if(flag_monthly==1);
    mm=(ycur-yearsta)*mmm+mcur;		%mm: 1:nrec
  elseif(flag_monthly==0);
    mm=mm_yrprev+mcur;
  end;

%read in data, mcur [1:12 or 1:365(366)]
%read_bin reads in and convert to gcmfaces
  fld_dat=NaN*RAC;						%gcmfaces2d | ioSize
  if ~isempty(dir(file0));
    if(dimSum==3);fld_dat=read_bin(file0,mcur,0);		%gcmfaces2d
    elseif(dimSum==2);
      fld_dat=read_slice(file0,ioSize(1),ioSize(2),mcur);	%ioSize
      fld_dat=fld_dat(iwet);					%ioSize | [Lwet 1]
    end;
  end;
  %if(flag_monthly==0);fld_dat=convert2gcmfaces(fld_dat);fld_dat=fld_dat(iwet);end;
  fld_dat=fld_dat.*mskC;%land mask				%gcmfaces2d|ioSize|[Lwet 1]
  fld_dat(find(fld_dat<-99))=NaN;%missing data
  msk=1+0*fld_dat;%combined mask				%gcmfaces2d|ioSize|[Lwet 1]

%read in model barfiles, mm [1:nrec]
  file0=[dirEcco fileModel];
  fld_mod=NaN*RAC;
  if ~isempty(dir(file0));
    if(dimSum==3);fld_mod=read_bin(file0,mm,0); 		%gcmfaces2d
    elseif(dimSum==2);
      fld_mod=read_slice(file0,ioSize(1),ioSize(2),mm);		%ioSize
      fld_mod=fld_mod(iwet);					%ioSize|[Lwet 1]
    end;
  end;
  %if(flag_monthly==0);fld_mod=convert2gcmfaces(fld_mod);fld_mod=fld_mod(iwet);end;
  fld_mod=fld_mod.*msk;%mask consistent with fld_dat		%gcmfaces2d|ioSize|[Lwet 1]

  %misfits :
  %if(flag_monthly==1);
  if(dimSum==3);
    fld_estim(:,:,mm)=fld_mod;					%gcmfaces3d,F1
    fld_nsidc(:,:,mm)=fld_dat;					%gcmfaces3d,F2
    fld_dif(:,:,mm)=fld_mod-fld_dat;				%gcmfaces3d,F3
  %elseif(flag_monthly==0);
  elseif(dimSum==2);
    fld_estim1(:,mm)=fld_mod;					%[ioSize nrec]|[Lwet nrec],F4
    fld_nsidc1(:,mm)=fld_dat;					%[ioSize nrec]|[Lwet nrec],F5
    fld_dif1(:,mm) =fld_mod-fld_dat;				%[ioSize nrec]|[Lwet nrec],F6
  end;
 end;
 toc;
end;
end;%if ~useNctiles;

if useNctiles;
 fld_estim=read_nctiles([dirNctiles 'sic'],'sic_ECCOv4r2');
 fld_nsidc=read_nctiles([dirNctiles 'sic'],'sic_NSIDC');
 fld_estim(isnan(fld_nsidc))=NaN;
 fld_nsidc(isnan(fld_estim))=NaN;
 fld_dif=fld_estim-fld_nsidc;
end;

%mean climatology :
if(flag_monthly==1);
  climMod=reshape(fld_estim,[1 1 12 nyears]); climMod=nanmean(climMod,4);%gcmfaced3d,12mo,F7
  climObs=reshape(fld_nsidc,[1 1 12 nyears]); climObs=nanmean(climObs,4);%gcmfaces3d,12mo,F8
elseif(flag_monthly==0);
%atn 06.Jun.2016: after investigation, if daily, best to get rid of leap-year extra day
  yr=yearsta:yearend;%ileap=leapyear(yr);
  climMod1=NaN(Lwet,366,nyears);climObs1=climMod1;			%[Lwet 366 15] , 3.4GB, F9,F10
  i2=0;
  for iyr=1:nyears
    nday=datenum(yr(iyr),12,31)-datenum(yr(iyr),1,0);
    ileap(iyr)=(nday==366);
    ij=[1:59 61:366];mmm=365;	%skip 29/Feb for leap year
    if(ileap(iyr)==1);ij=1:366;mmm=366;end;
    i1=i2+1;i2=i2+mmm;fprintf('%i %i %i\n',yr(iyr),i1,i2);
    climMod1(:,ij,iyr)=fld_estim1(:,i1:i2);				%[Lwet 366 15], F9
    climObs1(:,ij,iyr)=fld_nsidc1(:,i1:i2);				%[Lwet 366 15], F10
  end;
  clear fld_estim1							%clear F4

%atn now remove 29/Feb:
  climMod1=climMod1(:,[1:59 61:366],:);					%[Lwet 365 15], F9
  climObs1=climObs1(:,[1:59 61:366],:);					%[Lwet 365 15], F10

%now putting everything into monthly:
  dd=[0 31 28 31 30 31 30 31 31 30 31 30 31];dd=cumsum(dd);
  climMod=nan(Lwet,12,nyears);climObs=climMod;				%[Lwet 12 15],F7,F8
  for imo=1:length(dd)-1		%imo: 1:12
    ij=dd(imo)+1:dd(imo+1);
    climMod(:,imo,:)=nanmean(climMod1(:,ij,:),2);			%[Lwet 12 15],F7
    climObs(:,imo,:)=nanmean(climObs1(:,ij,:),2);			%[Lwet 12 15],F8
  end;

%atn now swapping out fld_estim and fld_nsidc with monthly fields
  clear climMod1 climObs1						%clear F9 F10
  fld_estim=reshape(climMod,Lwet,nmonths,1);				%[Lwet 12*15],F1
  fld_nsidc=reshape(climObs,Lwet,nmonths,1);				%[Lwet 12*15],F2
  fld_dif=fld_estim-fld_nsidc;						%[Lwet 12*15],F3

%atn now taking mean
  climMod=squeeze(nanmean(climMod,3));					%[Lwet 12], F7
  climObs=squeeze(nanmean(climObs,3));					%[Lwet 12], F8
end;

%monthly integrals :
IceAreaNorthMod=NaN*zeros(1+nlon,nmonths);
IceAreaNorthObs=NaN*zeros(1+nlon,nmonths);

for mm=1:nlon+1;
  if(flag_monthly==1);
  tmp1=fld_estim.*repmat(areamskN(:,:,mm),[1 1 nmonths]);
  IceAreaNorthMod(mm,:)=nansum(tmp1,0);
  tmp1=fld_nsidc.*repmat(areamskN(:,:,mm),[1 1 nmonths]);
  IceAreaNorthObs(mm,:)=nansum(tmp1,0);
  
  elseif(flag_monthly==0);
    tmp1=fld_estim.*repmat(areamskN(:,mm),[1 nmonths]);
    IceAreaNorthMod(mm,:)=nansum(tmp1,1);
    tmp1=fld_nsidc.*repmat(areamskN(:,mm),[1 nmonths]);
    IceAreaNorthObs(mm,:)=nansum(tmp1,1);
  end;
  fprintf('%i ',mm);
end;
fprintf('\n');

%misfits :
%hits 37GB , falls back to 28.1GB
if(flag_monthly==1);
  mis_rms=sqrt(nanmean(fld_dif.^2,3));
  obs_std=nanvar(fld_nsidc,3);        obs_std=sqrt(obs_std);
  mod_std=nanvar(fld_nsidc+fld_dif,3);mod_std=sqrt(mod_std);
%  obs_std=nanstd(fld_nsidc,[],3);
%  mod_std=nanstd(fld_nsidc+fld_dif,[],3);
elseif(flag_monthly==0);
  mis_rms=sqrt(nanmean(fld_dif1.^2,2));	%32GB memory
  obs_std=nanvar(fld_nsidc1,2);         obs_std=sqrt(obs_std);
  mod_std=nanvar(fld_nsidc1+fld_dif1,2);mod_std=sqrt(mod_std);
%converting back to gcmfaces:
  clear temp;temp=NaN(n1*n2,1);temp(iwet,:)=mis_rms;mis_rms=convert2gcmfaces(temp);
  clear temp;temp=NaN(n1*n2,1);temp(iwet,:)=obs_std;obs_std=convert2gcmfaces(temp);
  clear temp;temp=NaN(n1*n2,1);temp(iwet,:)=mod_std;mod_std=convert2gcmfaces(temp);
  climMod_cp=climMod;
  climObs_cp=climObs;
  clear temp;temp=NaN(n1*n2,12);temp(iwet,:)=climMod;temp=reshape(temp,n1,n2,12);climMod=convert2gcmfaces(temp);
  clear temp;temp=NaN(n1*n2,12);temp(iwet,:)=climObs;temp=reshape(temp,n1,n2,12);climObs=convert2gcmfaces(temp);
end;

clear climMod_cp climObs_cp
%keyboard
if ~isdir([dirMat 'cost/']); mkdir([dirMat 'cost/']); end;
eval(['save ' dirMat '/cost/cost_seaicearea.mat fld_err mis_rms obs_std mod_std IceArea* clim*;']);

else;%display previously computed results

if isdir([dirMat 'cost/']); dirMat=[dirMat 'cost/']; end;

eval(['load ' dirMat '/cost_seaicearea.mat;']);

%variance maps:
figure; m_map_gcmfaces(mis_rms,2.5,{'myCaxis',[0:0.1:1.]});
myCaption={'modeled-observed rms -- sea ice concentration'};
if addToTex; write2tex(fileTex,2,myCaption,gcf); end;

figure; m_map_gcmfaces(obs_std,2.5,{'myCaxis',[0:0.1:1.]});
myCaption={'observed std -- sea ice concentration'};
if addToTex; write2tex(fileTex,2,myCaption,gcf); end;

figure; m_map_gcmfaces(mod_std,2.5,{'myCaxis',[0:0.1:1.]});
myCaption={'modelled std -- sea ice concentration'};
if addToTex; write2tex(fileTex,2,myCaption,gcf); end;

%arctic maps
figureL; 
subplot(2,2,1); m_map_gcmfaces(climMod(:,:,3),2.5,{'myCaxis',[0:0.1:1.]});
subplot(2,2,2); m_map_gcmfaces(climObs(:,:,3),2.5,{'myCaxis',[0:0.1:1.]});
subplot(2,2,3); m_map_gcmfaces(climMod(:,:,9),2.5,{'myCaxis',[0:0.1:1.]});
subplot(2,2,4); m_map_gcmfaces(climObs(:,:,9),2.5,{'myCaxis',[0:0.1:1.]});
myCaption={'ECCO (left) and NSIDC (right, gsfc bootstrap) ice concentration in March (top) and September (bottom).'};
if addToTex; write2tex(fileTex,2,myCaption,gcf); end;

%southern ocean maps
plot_southern=0;
if(plot_southern==1);
figureL;
subplot(2,2,1); m_map_gcmfaces(climMod(:,:,3),3,{'myCaxis',[0:0.1:1.]});
subplot(2,2,2); m_map_gcmfaces(climObs(:,:,3),3,{'myCaxis',[0:0.1:1.]});
subplot(2,2,3); m_map_gcmfaces(climMod(:,:,9),3,{'myCaxis',[0:0.1:1.]});
subplot(2,2,4); m_map_gcmfaces(climObs(:,:,9),3,{'myCaxis',[0:0.1:1.]});
myCaption={'ECCO (left) and NSIDC (right, gsfc bootstrap) ice concentration in March (top) and September (bottom).'};
if addToTex; write2tex(fileTex,2,myCaption,gcf); end;
end;

%northern/southern integrals
ny=length(IceAreaNorthMod)/12;
yy=yearsta+[0:ny-1];

figureL;
if(plot_southern==1);
subplot(1,2,1);
end;
plot(yy,IceAreaNorthMod(1,3:12:end),'b','LineWidth',2); hold on;
plot(yy,IceAreaNorthObs(1,3:12:end),'r','LineWidth',2);
plot(yy,IceAreaNorthMod(1,9:12:end),'b','LineWidth',2);
plot(yy,IceAreaNorthObs(1,9:12:end),'r','LineWidth',2);
axis([yearsta yearsta+ny-1 0 20e12]);
ylabel('m^2'); title('Northern Hemisphere');
if(plot_southern==1);
subplot(1,2,2);
plot(yy,IceAreaSouthMod(1,3:12:end),'LineWidth',2); hold on;
plot(yy,IceAreaSouthObs(1,3:12:end),'r','LineWidth',2);
plot(yy,IceAreaSouthMod(1,9:12:end),'LineWidth',2);
plot(yy,IceAreaSouthObs(1,9:12:end),'r','LineWidth',2);
axis([yearsta yearsta+ny-1 0 20e12]);
ylabel('m^2'); title('Southern Hemisphere');
end;

if(plot_southern==1);
myCaption={'ECCO (blue) and NSIDC (red, gsfc bootstrap) ice concentration in March and September',...
           'in Northern Hemisphere (left) and Southern Hemisphere (right)'};
else;
myCaption={'ECCO (blue) and NSIDC (red, gsfc bootstrap) ice concentration in March and September',...
           'in Northern Hemisphere'};
end;
if addToTex; write2tex(fileTex,2,myCaption,gcf); end;

%southern basin integrals
if(plot_southern==1);
for mm=[3 9];
  figureL;
  for ii=1:6;
    subplot(3,2,ii);
    if ii==1; iiTxt='Entire Southern Ocean';
    else; iiTxt=sprintf('%dE to %dE',lonLims(ii-1),lonLims(ii));
    end;
    plot(yy,IceAreaSouthMod(ii,mm:12:end),'LineWidth',2); hold on;
    plot(yy,IceAreaSouthObs(ii,mm:12:end),'r','LineWidth',2);
    aa=axis; aa(1:2)=[yearsta yearsta+ny-1]; axis(aa);
    ylabel('m^2'); title(iiTxt);
  end;
  if mm==3; mmTxt='March'; elseif mm==9; mmTxt='September'; else; '???'; end;
  myCaption={'ECCO (blue) and NSIDC (red, gsfc bootstrap) ice concentration in ',mmTxt,' per Southern Ocean sector'};
  if addToTex; write2tex(fileTex,2,myCaption,gcf); end;
end;
end;

end;


