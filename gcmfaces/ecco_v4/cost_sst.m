function []=cost_sst(dirModel,dirMat,doComp,dirTex,nameTex);
%object:        compute cost function term for sst data
%inputs:        dimodel is the model directory
%               dirMat is the directory where diagnozed .mat files will be saved
%                     -> set it to '' to use the default [dirModel 'mat/']
%               doComp is a switch (1->compute; 0->display)
%optional:      dirTex is the directory where tex and figures files are created
%                 (if not specified then display all results to screen instead)
%               nameTex is the tex file name (default : 'myPlots')

if isempty(dirMat); dirMat=[dirModel 'mat' filesep]; else; dirMat=[dirMat filesep]; end;
if isempty(dir(dirMat));     mkdir([dirMat]); end;

%determine if and where to create tex and figures files
dirMat=[dirMat filesep];
if isempty(who('dirTex'));
  addToTex=0;
else;
  if ~ischar(dirTex); error('mis-specified dirTex'); end;
  addToTex=1; 
  if isempty(who('nameTex')); nameTex='myPlots'; end;
  fileTex=[dirTex filesep nameTex '.tex'];
end;

if doComp;	%loop 0, doComp

%grid, params and inputs

  gcmfaces_global; global myparms;
%if ~isfield(mygrid,'XC'); grid_load('./GRID/',5,'compact'); end;
  if(exist(mygrid.dirGrid)==0);
    fprintf('mygrid.dirGrid is wrong, fix to continue\n');
    keyboard
  end
  if ~isfield(mygrid,'XC'); grid_load('./GRID/',5,'compact',2); end;
  if ~isfield(mygrid,'hFacC'); tmp=rdmds([mygrid.dirGrid 'hFacC']);mygrid.hFacC=convert2gcmfaces(tmp);clear tmp;end;
  if ~isfield(mygrid,'LATS_MASKS'); gcmfaces_lines_zonal; end;
  if ~isfield(mygrid,'mskC');mskC=mygrid.hFacC;mskC(mskC==0)=NaN;mskC(mskC>0)=1;mygrid.mskC=mskC;end

%%%%%%%%%%%%
%define ASTE
%%%%%%%%%%%%
  if(sum(mygrid.ioSize-[270*1350 1])==0);
    store_all_rec=0;
    use_atn_wetpt=1;
    ioSize=mygrid.ioSize;
    dimSum=2;
    if isfield(myparms,'yearFirst'); yearFirst=myparms.yearFirst; yearLast=myparms.yearLast;
    else;
      error(...
       'inside cost_sst: missing myparms.[yearFirst,yearLast], need to run diags_pre_process');
      %yrStart=2002;yrEnd=2013; 
    end;
  else; 
    dimSum=3;
    store_all_rec=1;
    yearFirst=1992; yearLast=2011;
  end;

%search for nctiles files
  useNctiles=0;
  dirNctiles=[dirModel 'nctiles_remotesensing/sst/'];
  if ~isempty(dir(dirNctiles)); useNctiles=1; end;
  
  %nameReynolds='reynolds_oiv2_r1';
  %nameL2p='tmi_amsre_l2p_r1';
  %nameL2p='TMI_AMSRE_sst_daily_llc270aste';	%in input_sst
  nameReynolds='ReynoldsNoIce_monthly_llc270';	%in input_sst_reynolds
  nameRemss='tmi_amsre_oisst_r1';		%in input_sst_remss
  
  %dirData=dirModel; subdirReynolds=''; subdirRemss=''; subdirL2p='';
  %if ~isempty(dir([dirModel 'inputfiles/' nameReynolds '*']));
  %  dirData=[dirModel 'inputfiles/']; subdirReynolds=''; subdirRemss=''; subdirL2p='';
  %end;
  %dirData='/nobackupp2/atnguye4/llc270/aste_270x450x180/run_template/';
  %dirData='/home/atnguyen/llc270/aste_270x450x180/run_template/';
  dirData='/scratch/atnguyen/aste_270x450x180/run_template/';
  subdirReynolds='input_sst_reynolds/';subdirRemss='input_sst_remss/';subdirL2p='';
  
  %nameSigma='sigma_surf_0p5.bin';
  %dirSigma=dirModel;
  %if ~isempty(dir([dirModel 'inputfiles/' nameSigma]));
  %  dirSigma=[dirModel 'inputfiles/'];
  %end;
  nameSigma='sigma_half_llc270m9Eb.bin';
  dirSigma=[dirData 'input_sigma/'];

%atn: keep at gcmfaces b/c only read in to save
  fld_err=NaN*mygrid.RAC;							%gcmfaces,2d
  file0=[dirSigma nameSigma];

  if ~isempty(dir(file0));fld_err=read_bin(file0,1,0);end;
  if useNctiles; fld_err=read_nctiles([dirNctiles 'sst'],'sst_sigma'); end;
  fld_err(find(fld_err==0))=nan;
  %fld_w=fld_err.^-2;					%not used, comment out

  nameTbar='m_sst_mon';
  if ~isempty(dir([dirModel nameTbar '*']));
    dirTbar=dirModel;
  else;
    dirTbar=[dirModel 'barfiles' filesep];
  end;
  fileModel='unknown';
  file0=[dirTbar nameTbar '*data']; 
  if ~isempty(dir(file0)); 
    fileModel=dir(file0);
    fileModel=fileModel.name;
  end;

  if (dimSum==2)
    tmpmskC1=convert2gcmfaces(mygrid.mskC(:,:,1));			%ioSize [364500 1]
    tmpRAC  =convert2gcmfaces(mygrid.RAC);				%ioSize [364500 1]
    if use_atn_wetpt
      iwet=find(tmpmskC1==1);				%keep only wet point, 146620
    else
      iwet=1:ioSize(1);
    end
    Lwet=length(iwet);
  elseif(dimSum==3);
    tmpmskC1=mygrid.mskC(:,:,1);					%gcmfaces,2d
    tmpRAC  =mygrid.RAC;						%gcmfaces,2d
  end;

  nrec=12*(yearLast-yearFirst+1);

  if ~useNctiles;		%loop 1, ~useNctiles
%computational loop
    if(dimSum==3);
      if store_all_rec
        sst_mod  =repmat(NaN*tmpmskC1,[1 1 nrec]);			%gcmfaces,3d,nt,F1
        sst_rey  =repmat(NaN*tmpmskC1,[1 1 nrec]);			%gcmfaces,3d,nt,F2
        sst_remss=repmat(NaN*tmpmskC1,[1 1 nrec]);			%gcmfaces,3d,nt,F3
      else
        mod_m_rey  =repmat(NaN*tmpmskC1,[1 1 nrec]);			%gcmfaces,3d,nt,F4
        mod_m_remss=repmat(NaN*tmpmskC1,[1 1 nrec]);			%gcmfaces,3d,nt,F5
      end

%atn: 09Jul2017: move to raw array size to reduce memory
% even save further memory by keeping only wet points: 
    elseif(dimSum==2);
      if store_all_rec
        sst_mod  =nan(Lwet,nrec);					%[ioSize nt] or [Lwet nt],F1
        sst_rey  =nan(Lwet,nrec);					%[ioSize nt] or [Lwet nt],F2
        sst_remss=nan(Lwet,nrec);					%[ioSize nt] or [Lwet nt],F3
      else;
        mod_m_rey  =nan(Lwet,nrec);					%[ioSize nt] or [Lwet nt],F4
        mod_m_remss=nan(Lwet,nrec);					%[ioSize nt] or [Lwet nt],F5
      end;
    end;

%
    for ycur=yearFirst:yearLast;			%loop 2, ycur
      fprintf(['starting ' num2str(ycur) '\n']);
      tic;
      for mcur=1:12;					%loop 3, month, mcur

        mm=(ycur-yearFirst)*12+mcur;

  %load Reynolds SST
        file0=[dirData subdirReynolds nameReynolds '_' num2str(ycur)];
        fld_rey=NaN*tmpRAC;						%gcmfaces2d | ioSize
        if ~isempty(dir(file0)); 
%read_bin: convert to gcmfaces after read_slice
              if(dimSum==3);fld_rey=read_bin(file0,mcur,0);		%gcmfaces2d
          elseif(dimSum==2)
            fld_rey=squeeze(read_slice(file0,ioSize(1),ioSize(2),mcur));%ioSize [364500 1]
          end
        end;
        fld_rey(find(fld_rey==0))=NaN;
        fld_rey(find(fld_rey<-99))=NaN;
%mult also with mskC to remove effect from Pacific sector
        fld_rey=fld_rey.*tmpmskC1;					%gcmfaces2d or [ioSize]
%this is the array that requires memory, can we just compute without storing?
        if store_all_rec;
              if(dimSum==3);sst_rey(:,:,mm)=fld_rey;			%gcmfaced3d,F2
          elseif(dimSum==2);sst_rey(:,mm)=fld_rey(iwet);end		%[ioSize nt] or [Lwet nt],F2
        end

  %load Remss SST
        file0=[dirData subdirRemss nameRemss '_' num2str(ycur)];
        fld_remss=NaN*tmpRAC;						%gcmfaces2d or ioSize
        if ~isempty(dir(file0)); 
              if(dimSum==3);fld_remss=read_bin(file0,mcur,0);		%gcmfaces2d
          elseif(dimSum==2);
            fld_remss=squeeze(read_slice(file0,ioSize(1),ioSize(2),mcur));%ioSize [364500 1]
          end;
        end;
        fld_remss(find(fld_remss==0))=NaN;
        fld_remss(find(fld_remss<-99))=NaN;
        fld_remss=fld_remss.*tmpmskC1;					 %gcmfaces2d or ioSize
%this is the array that requires memory, can we just compute without storing?
        if store_all_rec
              if(dimSum==3);sst_remss(:,:,mm)=fld_remss;		%gcmfaces3d, F3
          elseif(dimSum==2);sst_remss(:,mm)=fld_remss(iwet);end;	%[ioSize nt] or [Lwet nt],F3
        end;

  %load model SST
        file0=[dirTbar fileModel];
        fld_mod=NaN*tmpRAC;					 	%gcmfaces2d or ioSize
%  if ~isempty(dir(file0)); fld_mod=read_bin(file0,mm,1); end;
        if ~isempty(dir(file0)); 
              if(dimSum==3);fld_mod=read_bin(file0,mm,0);	 	%gcmfaces2d
          elseif(dimSum==2);
            fld_mod=squeeze(read_slice(file0,ioSize(1),ioSize(2),mm)); 	%ioSize
          end;
        end;
        fld_mod=fld_mod.*tmpmskC1;				 	%gcmfaces2d or ioSize
%this is the array that requires memory, can we just compute without storing?
        if store_all_rec
              if(dimSum==3);sst_mod(:,:,mm)=fld_mod;			%gcmfaces3d,F1
          elseif(dimSum==2);sst_mod(:,mm)=fld_mod(iwet);end		%[ioSize nt] or [Lwet nt],F1
        end;

%this block Gael commented out but am putting back in, saves memory
% only mod_m_rey and mod_m_remss are needed in full 3D to compute RMS at the end
  %store misfit maps
        if ~store_all_rec
          if(dimSum==3);
            mod_m_rey(:,:,mm)  =fld_mod-fld_rey;			%gcmfaces3d,nt,F4
            mod_m_remss(:,:,mm)=fld_mod-fld_remss;			%gcmfaces3d,nt,F5
          elseif(dimSum==2);
%needs to make sure in double array format here:
            mod_m_rey(:,mm)  =fld_mod(iwet)-fld_rey(iwet);		%ioSize or [Lwet 1],F4
            mod_m_remss(:,mm)=fld_mod(iwet)-fld_remss(iwet);		%ioSize or [Lwet 1],F5
          end
  %comp zonal means
          zm_mod(:,mm)  =calc_zonmean_T(fld_mod);			%pass in gcmfaces2d or ioSize
          %msk=tmpmskC1; msk(isnan(fld_rey)==1)=NaN;			%identical w/ or w/o msk
          zm_rey(:,mm)  =calc_zonmean_T(fld_rey);
          %msk=tmpmskC1; msk(isnan(fld_remss)==1)=NaN;			%identical w/ or w/o msk
          zm_remss(:,mm)=calc_zonmean_T(fld_remss);
%atn: now adding 2 more in:
          zm_mod_m_rey(:,mm)  =calc_zonmean_T(fld_mod-fld_rey);
          zm_mod_m_remss(:,mm)=calc_zonmean_T(fld_mod-fld_remss);
        end

%  msk=mygrid.mskC(:,:,1); msk(isnan(fld_rey)==1)=NaN;
%  zm_rey(:,mm)=calc_zonmean_T(fld_rey.*msk);
%  zm_mod_m_rey(:,mm)=calc_zonmean_T(fld_mod-fld_rey.*msk);

%  msk=mygrid.mskC(:,:,1); msk(isnan(fld_remss)==1)=NaN;
%  zm_remss(:,mm)=calc_zonmean_T(fld_remss.*msk);
%  zm_mod_m_remss(:,mm)=calc_zonmean_T(fld_mod-fld_remss.*msk);

      end;			%loop 3, mcur
    toc;
    end;			%loop 2, yrcur
  end;%if ~useNctiles;	%loop1

  if useNctiles; 
   sst_rey=read_nctiles([dirNctiles 'sst'],'sst_REYNOLDS');
   sst_mod=read_nctiles([dirNctiles 'sst'],'sst_ECCOv4r2');
   sst_remss=repmat(NaN*mygrid.mskC(:,:,1),[1 1 12*(yearLast-yearFirst+1)]);
  end;

%%%store misfit maps
  if store_all_rec
%atn: need to keep mod_m_rey and mod_m_remss full 3D
    mod_m_rey=sst_mod-sst_rey;			%gcmfaces3d | [ioSize nt] | [Lwet nt], F4 (F1-F2)
    mod_m_remss=sst_mod-sst_remss;		%gcmfaces3d | [ioSize nt] | [Lwet nt], F5 (F1-F3)
    clear sst_mod sst_rey sst_remss		%clear F1 F2 F3

%comp zonal means
    tic;
    zm_mod=calc_zonmean_T(sst_mod);					%[179 nt]
    zm_rey=calc_zonmean_T(sst_rey);					%[179 nt]
    zm_remss=calc_zonmean_T(sst_remss);					%[179 nt]
    zm_mod_m_rey=calc_zonmean_T(mod_m_rey);				%[179 nt]
    zm_mod_m_remss=calc_zonmean_T(mod_m_remss);				%[179 nt]
    toc;
  end;

%compute rms maps
%atn: need to keep mod_m_rey and mod_m_remss full 3D
  rms_to_rey=sqrt(nanmean(mod_m_rey.^2,dimSum));    			%gcmfaces2d | ioSize | [Lwet 1]
  rms_to_remss=sqrt(nanmean(mod_m_remss.^2,dimSum));			%gcmfaces2d | ioSize | [Lwet 1]

%now clear memory, and convert back to gcmfaces for plotting:
  clear mod_m_rey mod_m_remss;						%clear F4 F5
  if(dimSum==2);
    if use_atn_wetpt
      tmp=zeros(ioSize);tmp(iwet)=rms_to_rey;  rms_to_rey=tmp;clear tmp	%ioSize
      tmp=zeros(ioSize);tmp(iwet)=rms_to_remss;rms_to_remss=tmp;clear tmp%ioSize
    end;
    rms_to_rey  =convert2gcmfaces(rms_to_rey);				%gcmfaces2d
    rms_to_remss=convert2gcmfaces(rms_to_remss);			%gcmfaces2d
  end;

  if ~isdir([dirMat 'cost/']); mkdir([dirMat 'cost/']); end;
  eval(['save ' dirMat '/cost/cost_sst.mat fld_err rms_* zm_*;']);

else;%display previously computed results , loop 0, doComp

  global mygrid;

  if isdir([dirMat 'cost/']); dirMat=[dirMat 'cost/']; end;

  eval(['load ' dirMat '/cost_sst.mat;']);

%atn: convert back to gcmfaces if needed: (from saving memory)

  if ~isempty(who('fld_rms')); 
    figure; m_map_gcmfaces(fld_rms,0,{'myCaxis',[0:0.2:1.2 1.5:0.5:3 4:1:6 8 10]/2});
    myCaption={'modeled-observed rms -- sea surface temperature (K)'};
    if addToTex; write2tex(fileTex,2,myCaption,gcf); end;
  else;

    testREMSS=sum(~isnan(rms_to_remss))>0;

    figure; m_map_gcmfaces(rms_to_rey,0,{'myCaxis',[0:0.2:1.2 1.5:0.5:3 4:1:6 8 10]/2});
    myCaption={'modeled-Reynolds rms -- sea surface temperature (K)'};
    if addToTex; write2tex(fileTex,2,myCaption,gcf); end;
  
    if testREMSS;
      figure; m_map_gcmfaces(rms_to_remss,0,{'myCaxis',[0:0.2:1.2 1.5:0.5:3 4:1:6 8 10]/2});
      myCaption={'modeled-REMSS rms -- sea surface temperature (K)'};
      if addToTex; write2tex(fileTex,2,myCaption,gcf); end;
    end;

    ny=size(zm_rey,2)/12;
%  [xx,yy]=meshgrid(1992+([1:ny*12]-0.5)/12,mygrid.LATS);
    [xx,yy]=meshgrid(2002+([1:ny*12]-0.5)/12,mygrid.LATS);

    figureL;
    obs=zm_rey; obsCycle=sst_cycle(obs); 
    mod=zm_rey+zm_mod_m_rey; modCycle=sst_cycle(mod);
    mis=zm_mod_m_rey; misCycle=sst_cycle(mis);
    subplot(3,1,1); pcolor(xx,yy,obs-obsCycle); shading flat; caxis([-1 1]*1); colorbar;
    set(gca,'FontSize',14); set(gca,'XTick',[]); ylabel('latitude'); 
    title('Reynolds sst anomaly');
    subplot(3,1,2); pcolor(xx,yy,mod-modCycle); shading flat; caxis([-1 1]*1); colorbar;
    set(gca,'FontSize',14); set(gca,'XTick',[]); ylabel('latitude');
    title('ECCO sst anomaly');
    subplot(3,1,3); pcolor(xx,yy,mis-misCycle); shading flat; caxis([-1 1]*1); colorbar;
    set(gca,'FontSize',14); ylabel('latitude'); title('ECCO-Reynolds sst misfit');
    myCaption={'ECCO and Reynolds zonal mean sst anomalies (K)'};
    if addToTex; write2tex(fileTex,2,myCaption,gcf); end;

    if testREMSS;
      figureL;
      obs=zm_remss; obsCycle=sst_cycle(obs);
      mod=zm_remss+zm_mod_m_remss; modCycle=sst_cycle(mod);
      mis=zm_mod_m_remss; misCycle=sst_cycle(mis);
      subplot(3,1,1); pcolor(xx,yy,obs-obsCycle); shading flat; caxis([-1 1]*1); colorbar;
      set(gca,'FontSize',14); set(gca,'XTick',[]); ylabel('latitude');
      title('REMSS sst anomaly');
      subplot(3,1,2); pcolor(xx,yy,mod-modCycle); shading flat; caxis([-1 1]*1); colorbar;
      set(gca,'FontSize',14); set(gca,'XTick',[]); ylabel('latitude');
      title('ECCO sst anomaly');
      subplot(3,1,3); pcolor(xx,yy,mis-misCycle); shading flat; caxis([-1 1]*1); colorbar;
      set(gca,'FontSize',14); ylabel('latitude'); title('ECCO-REMSS sst misfit');
      myCaption={'ECCO and REMSS zonal mean sst anomalies (K)'};
      if addToTex; write2tex(fileTex,2,myCaption,gcf); end;
    end;

  end;

end;	%loop 0

function [zmCycle]=sst_cycle(zmIn);

ny=size(zmIn,2)/12;
zmCycle=NaN*zeros(179,12);
for mm=1:12;
zmCycle(:,mm)=nanmean(zmIn(:,mm:12:ny*12),2);
end;
zmCycle=repmat(zmCycle,[1 ny]);

