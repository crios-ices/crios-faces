function []=cost_adxx(dirModel,dirMat,doComp,dirTex,nameTex);
%object:	compute cost function term for atmospheric controls
%inputs:	dirModel is the model directory
%               dirMat is the directory where diagnozed .mat files will be saved
%                     -> set it to '' to use the default [dirModel 'mat/']
%		doComp is a switch (1->compute; 0->display)
%optional:      dirTex is the directory where tex and figures files are created
%                 (if not specified then display all results to screen instead)
%               nameTex is the tex file name (default : 'myPlots')

if isempty(dirMat); dirMat=[dirModel 'mat' filesep]; else; dirMat=[dirMat filesep]; end;
if isempty(dir(dirMat));     mkdir([dirMat]); end;
if isempty(dirModel); dirModel=[dirMat '../']; end;
%keyboard
%determine if and where to create tex and figures files
dirMat=[dirMat filesep];
if isempty(who('dirTex'));
  addToTex=0;
else;
  if ~ischar(dirTex); error('mis-specified dirTex'); end;
  addToTex=1;
  if isempty(who('nameTex')); nameTex='myPlots'; end;
  fileTex=[dirTex filesep nameTex '.tex'];
end;

for iloop=1:2
  if iloop==1; jxx=7;else;jxx=5;end;
  for ii=1:jxx;
    if(iloop==1);
      switch ii;
        case 1; xxName='atemp'; nameWeight='wTair_Ev2linearcapsmm9Eb_MezenB.bin'; 
		cc=[2500 2500 250 40 300]; uni='1/K';
        case 2; xxName='aqh';   nameWeight='wQair_Ev2linearcapsmm9Eb_MezenB.bin'; 
		cc=[5e6 5e6 8e5 2e5 8e5]; uni='1/(g/kg)';
        case 3; xxName='uwind'; nameWeight='wUwind_Ev2linearcapsmm9Eb_MezenB.bin'; 
		cc=[3e4 3e4 16e2 6e2 4e3]; uni='1/(m/s)';
        case 4; xxName='vwind'; nameWeight='wVwind_Ev2linearcapsmm9Eb_MezenB.bin'; 
		cc=[5e4 5e4 5e3 5e2 5e3]; uni='1/(m/s)';
        case 5; xxName='lwdown'; nameWeight='wLwDn_Ev2linearcapsmm9Eb_MezenB.bin'; 
		cc=[1e2 1e2 1e1 5e0 2e1]; uni='1/(W/m2)';
        case 6; xxName='swdown'; nameWeight='wSwDn_Ev2linearcapsmm9Eb_MezenB.bin'; 
		cc=[1e2 1e2 1e1 5e0 2e1]; uni='1/(W/m2)';
        case 7; xxName='precip'; nameWeight='wRain_Ev2linearcapsmm9Eb_MezenB.bin';
		cc=[4e11 4e11 5e10 10e9 10e10]; uni='1/(m/s)';
      end;
    else;
      switch ii;
        case 1; xxName='theta';  nameWeight='weight_T_mad_feb2013_mod01DpClim_llc270m9Eb.bin';
		cc=[4e3 6e3 12e3 30e3 20e4 50e4 20e4 20e4]; uni='1/C';
        case 2; xxName='salt';   nameWeight='weight_S_mad_feb2013_mod01DpClim_llc270m9Eb.bin';
		cc=[10e4 10e4 20e4 40e4 80e4 20e5 20e5 20e5]; uni='1/psu';
        case 3; xxName='diffkr' ;nameWeight='weight_diffkr_basin_v1m9EfC.bin'; 
		cc=[2e9 2e9 2e9 1e9 5e8 5e8 5e8 5e8]; uni='1/(m2/s)';
        case 4; xxName='kapredi';nameWeight='weight_kapredi.bin';
		cc=[2e1 2e1 4e1 4e1 4e1 4e1 4e1 4e1];  uni='1/(m2/s)';
        case 5; xxName='kapgm'  ;nameWeight='weight_kapgm.bin';                
		cc=[2e2 2e2 2e2 2e2 2e2 2e2 2e2 2e2]; uni='1/(m2/s)';

      end;
    end;

    dirWei='/home/atnguyen/llc270/aste_270x450x180/run_template/input_weight/';

    if ~isempty(dir([dirModel 'adxx_' xxName '.*data']));
      dirXX=dirModel;
    else;
      dirXX=[dirModel 'ADXXfiles' filesep];
    end;

    gcmfaces_global;
    nz=50;
    ioSize=mygrid.ioSize;
    use_atn_readcompact=1;
    use_atn_wetpt=1;
    if use_atn_readcompact
      nx=mygrid.facesExpand(1);
      ny=prod(ioSize)/nx;
      nz=length(mygrid.RC);
      if use_atn_wetpt
        msk2d=convert2gcmfaces(mygrid.hFacC(:,:,1));iwet2d=find(msk2d(:)>0);Lwet2d=length(iwet2d);
      end
    end;

    if doComp;

%load grid
      if ~isfield(mygrid,'XC'); grid_load('./GRID/',5,'compact'); end;
      if ~isfield(mygrid,'LATS_MASKS'); gcmfaces_lines_zonal; end;

%read model cost output
      if iloop==1
        
        tmp1=dir([dirXX 'adxx_' xxName '.*data']);tmp1=tmp1(1);
        nt=tmp1.bytes/ioSize(1)/ioSize(2)/4;	%assume real4

%below is the default way to read everything at once into memory, then convert to global
        if ~use_atn_readcompact
          tmp2=size(convert2gcmfaces(mygrid.XC)); 
          fld_xx=read2memory([dirXX tmp1.name],[tmp2 tmp1.bytes/tmp2(1)/tmp2(2)/4]);
          fld_wei=sqrt(read_bin([dirWei nameWeight],1,0));
          fld_rms=sqrt(mean(fld_xx.^2,3));
          fld_mean=mean(fld_xx,3);
          fld_std=std(fld_xx,[],3);
        else;
%now use read_slice to reduce memory footprint as follows:
          if use_atn_wetpt
            fld_xx_mean=zeros(Lwet2d,1);
          else
            fld_xx_mean=zeros(ioSize);
          end;
          fld_xx_sq  =fld_xx_mean;
          fld_xt_mean=fld_xx_mean;
          fld_t_mean =fld_xx_mean;
          fld_t_sq   =fld_xx_mean;
          dt=2*7*24*3600;	%two weeks, from data.ctrl
          fprintf('read %s ',tmp1.name);fprintf(' of length: %i;',nt);
          for j=1:nt;
            tmp2=read_slice([dirXX tmp1.name],ioSize(1),ioSize(2),j);
            if j==1
              fld_wei=sqrt(readbin([dirWei nameWeight],[ioSize(1) ioSize(2) 1]));
            end;
            if use_atn_wetpt;
              %fld_xx(:,1,j)=tmp2(iwet2d);
              fld_xx_mean=fld_xx_mean+tmp2(iwet2d);
              fld_xx_sq  =fld_xx_sq  +tmp2(iwet2d).^2;
              if(j==1);fld_wei=fld_wei(iwet2d);end;
              fld_xt_mean=fld_xt_mean+(j-1)*dt*tmp2(iwet2d);		%cross term
              fld_t_mean =fld_t_mean+(j-1)*dt.*ones(Lwet2d,1);
              fld_t_sq   =fld_t_sq  +((j-1)*dt).^2.*ones(Lwet2d,1);
            else;
                %fld_xx(:,:,j)=tmp2;
              fld_xx_mean=fld_xx_mean+tmp2;
              fld_xx_sq  =fld_xx_sq  +tmp2.^2;
              fld_xt_mean=fld_xt_mean+ (j-1)*dt*tmp2;			%cross term
              fld_t_mean =fld_t_mean + (j-1)*dt.*ones(ioSize);
              fld_t_sq   =fld_t_sq   +((j-1)*dt).^2.*ones(ioSize);
            end;
            if(mod(j,50)==0);fprintf('%i ',j);end;
          end;%jloop
          fprintf('\n');

%compute xx stats
          fld_mean   =fld_xx_mean./nt;
          fld_std    =sqrt((fld_xx_sq - nt.*fld_mean.^2)./(nt-1));
          fld_rms    =sqrt(fld_xx_sq./nt);
          det        =fld_t_sq.*nt - fld_t_mean.^2;
          fld_slo    =(fld_xt_mean.*nt - fld_xx_mean.*fld_t_mean)./det;
          fld_int    =(fld_t_sq.*fld_xx_mean - fld_t_mean.*fld_xt_mean)./det;
%now convert back to gcmfaces:
          if use_atn_wetpt
            tmp=nan(nx,ny,1);tmp(iwet2d)=fld_mean;fld_mean=tmp; clear tmp
            tmp=nan(nx,ny,1);tmp(iwet2d)=fld_std; fld_std =tmp; clear tmp
            tmp=nan(nx,ny,1);tmp(iwet2d)=fld_rms; fld_rms =tmp; clear tmp
            tmp=nan(nx,ny,1);tmp(iwet2d)=fld_wei; fld_wei =tmp; clear tmp
            tmp=nan(nx,ny,1);tmp(iwet2d)=fld_slo; fld_slo =tmp; clear tmp
            tmp=nan(nx,ny,1);tmp(iwet2d)=fld_int; fld_int =tmp; clear tmp
          end;
          fld_mean=convert2gcmfaces(fld_mean);
          fld_std =convert2gcmfaces(fld_std);
          fld_rms =convert2gcmfaces(fld_rms);
          fld_wei =convert2gcmfaces(fld_wei);
          fld_slo =convert2gcmfaces(fld_slo);
          fld_int =convert2gcmfaces(fld_int);
        end; %use_atn_readcompact

%mask
        fld_wei=fld_wei.*mygrid.mskC(:,:,1);
        fld_rms=fld_rms.*mygrid.mskC(:,:,1).*fld_wei; 
        fld_mean=fld_mean.*mygrid.mskC(:,:,1).*fld_wei;
        fld_std=fld_std.*mygrid.mskC(:,:,1).*fld_wei;
        fld_slo=fld_slo.*mygrid.mskC(:,:,1).*fld_wei;
        fld_int=fld_int.*mygrid.mskC(:,:,1).*fld_wei;

        clear fld_xx;
        clear fld_xx* fld_t* fld_xt*;

        if ~isdir([dirMat 'cost/']); mkdir([dirMat 'cost/']); end;
        eval(['save ' dirMat '/cost/cost_adxx_' xxName '.mat fld_* cc uni;']);
      end;	%iloop=1

    else;%doComp=0, display previously computed results

      global mygrid;
      nfx=[nx 0 nx 180 450];nfy=[450 0 nx nx nx];
      inan=1:215;jnan=451:900;
      ix=91:2*nx;iy=16:865;
      rC=abs(round(squeeze(mygrid.RC)));
      msk=convert2gcmfaces(mygrid.mskC(:,:,1));msk(find(isnan(msk)==1))=0;
      msk=get_aste_tracer(reshape(msk,nx,ny),nfx,nfy);msk(inan,jnan)=0;
      convfac=24*3600*365.25;	%sec/yr
      plot_aste=1;

      if iloop==1
        if isdir([dirMat 'cost/']); dirMat=[dirMat 'cost/']; end;
        eval(['load ' dirMat '/cost_adxx_' xxName '.mat;']);

        if(plot_aste==1);

          fldstr={'rms','std','mean','slo','int'};
          for ifld=1:5
            eval(['fld=fld_' fldstr{ifld} ';']);
            fld=convert2gcmfaces(fld);fld=reshape(fld,nx,ny);fld=get_aste_tracer(fld,nfx,nfy);fld(inan,jnan)=nan;
            if(strcmp(fldstr{ifld},'slo'));fld=fld.*convfac;end;
            figure(ifld);clf;colormap(jet(21));
            imagescnan1(ix,iy,fld(ix,iy)');
            if(strcmp(fldstr{ifld},'rms')==1|strcmp(fldstr{ifld},'std')==1);caxis([0 cc(ifld)]);
            else;caxis([-cc(ifld) cc(ifld)]);end;colorbar;grid;
            hold on;[aa,bb]=contour(ix,iy,msk(ix,iy)',[1 1]);hold off;set(bb,'color',.1.*[1 1 1]);
            set(gca,'Xtick',-5e3:5e6:5e6,'Ytick',-5e3:5e6:5e6);
            myCaption={[fldstr{ifld} ' sensitivity -- adxx ' xxName ' (' uni ')']};
            if addToTex; write2tex(fileTex,2,myCaption,gcf); end;
          end
            
        else;

          figure(1);clf; 
          m_map_gcmfaces(fld_rms,0,{'myCaxis',[0:0.05:0.5 0.6:0.1:1 1.25]*cc});
          myCaption={['rms sensitivity -- adxx ' xxName ' (' uni ')']};
          if addToTex; write2tex(fileTex,2,myCaption,gcf); end;

          figure(1);clf;
          m_map_gcmfaces(fld_std,0,{'myCaxis',[0:0.05:0.5 0.6:0.1:1 1.25]*cc});
          myCaption={['std sensitivity -- adxx ' xxName ' (' uni ')']};
          if addToTex; write2tex(fileTex,2,myCaption,gcf); end;

          figure(1);clf;
          m_map_gcmfaces(fld_mean,0,{'myCaxis',[-0.5:0.05:0.5]*cc});
          myCaption={['mean sensitivity -- adxx ' xxName ' (' uni ')']};
          if addToTex; write2tex(fileTex,2,myCaption,gcf); end;

          figure(1);clf;
          m_map_gcmfaces(fld_slo.*convfac,0,{'myCaxis',[-0.05:0.005:0.05]*cc});
          myCaption={['slope sensitivity -- adxx ' xxName ' (' uni '/yr)']};
          if addToTex; write2tex(fileTex,2,myCaption,gcf); end;

          figure(1);clf;
          m_map_gcmfaces(fld_int,0,{'myCaxis',[-0.5:0.05:0.5]*cc});
          myCaption={['intercept sensitivity -- adxx ' xxName ' (' uni ')']};
          if addToTex; write2tex(fileTex,2,myCaption,gcf); end;
        end;

      else	%iloop==2, 3d fields, read in right here to plot
        klev=[1:5:40];
        tmp1=dir([dirXX 'adxx_' xxName '.*data']);tmp1=tmp1(1);
        fld_xx=read_slice([dirXX tmp1.name],ioSize(1),ioSize(2),klev);
        fld_xx=convert2gcmfaces(fld_xx).*mygrid.mskC(:,:,klev);

        fld_wei=sqrt(read_slice([dirWei nameWeight],ioSize(1),ioSize(2),klev));
        fld_wei=convert2gcmfaces(fld_wei).*mygrid.mskC(:,:,klev);

        %keyboard
        for k=1:length(klev);
          if(plot_aste==1);
            tmp=fld_xx(:,:,k).*fld_wei(:,:,k);tmp=reshape(convert2gcmfaces(tmp),nx,ny);
            tmp=get_aste_tracer(tmp,nfx,nfy);tmp(inan,jnan)=nan;
            figure(1);clf;colormap(jet(21));
            imagescnan1(ix,iy,tmp(ix,iy)');caxis([-cc(k),cc(k)]);colorbar;grid;
            hold on;[aa,bb]=contour(ix,iy,msk(ix,iy)',[1 1]);hold off;set(bb,'color',.1.*[1 1 1]);
            set(gca,'Xtick',-5e3:5e6:5e6,'Ytick',-5e3:5e6:5e6);
            myCaption={['sensitivity -- adxx ' xxName ' z(k=' num2str(klev(k)) ')=' num2str(rC(klev(k))) 'm (' uni ')']};
            if addToTex; write2tex(fileTex,2,myCaption,gcf); end;
          else;
 
            tmp=fld_xx(:,:,k).*fld_wei(:,:,k);
            figure(k);clf;
            m_map_gcmfaces(tmp,0,{'myCaxis',[-1.0:0.1:1.0]*cc(k)});
            myCaption={['sensitivity -- adxx ' xxName ' z(k=' num2str(klev(k)) ')=' num2str(rC(klev(k))) 'm  (' uni ')']};
            if addToTex; write2tex(fileTex,2,myCaption,gcf); end;
          end
        end;	%k
      end	%iloop==1|iloop==2
    end;	%doComp
  end;%for ii=1:6;
end;%iloop
