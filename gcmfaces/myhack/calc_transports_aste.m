%use gcmfaces package
clear all
dirRoot='/net/nares/raid8/ecco-shared/llc270/aste_270x450x180/';

% now model run specifics:
nf=[450  , 0   , 270 , 180 , 450 ];
nfx=[270 0 270 180 450];nfy=[450 0 270 270 270];
RunStr=input('RunStr ["run22"] ');dirRun=[dirRoot 'output/' RunStr '/'];dt=1200;yrStart=1992;
dirOut=[dirRun 'matlab/+ocean/'];if(exist(dirOut)==0);mkdir(dirOut);end;
ext   =input('ext ["mass" for uvelmass, empty for uvel] ');

flist=dir([dirRun 'Mean/' 'U' ext 'mean.*.data']);idot=find(flist(1).name=='.');idot=idot(1)+1:idot(2)-1;

%clear path:

gcmfaces_global

dirGrid=[dirRun 'GRID/'];if(exist(dirGrid)==0);dirGrid=[dirRoot 'GRID/'];end;
%if(exist([dirGrid 'grid.specs.mat'])==0);
%  ioSize=[364500 1];
%  facesSize=[[270 450];[0 0];[270 270];[180 270];[450 270]];
%  facesExpand=[270 450];
%  save([dirGrid 'grid.specs.mat'],'ioSize','facesSize','facesExpand');
%end;

nFaces=5;fileFormat='compact';nx=270;ny=1350;nz=50;
list23D={'XC','YC','RAC','DXC','DYC','DXG','DYG','hFacC','hFacW','hFacS','Depth'};
list1D={'RC','RF','DRC','DRF'};
grid_load_mod(dirGrid,nFaces,fileFormat,list23D,list1D);

%take care that nan in RAC are replaced with zeros for gcmfaces_calc/gcmfaces_edge_mask.m
temp=mygrid.RAC;temp(isnan(temp))=0;mygrid.RAC=temp;clear temp

temp=mygrid.hFacC;temp(temp==0)=NaN;temp(temp>0)=1;mygrid.mskC=temp;clear temp
temp=mygrid.hFacS;temp(temp==0)=NaN;temp(temp>0)=1;mygrid.mskS=temp;clear temp
temp=mygrid.hFacW;temp(temp==0)=NaN;temp(temp>0)=1;mygrid.mskW=temp;clear temp

%sample_analysis/line_greatC_TUV_MASKS_v4.m --> lonPairs, latPairs, names 
%gcmfaces_calc/gcmfaces_lines_transp.m -> add mygrid.LINES_MASKS
[lonPairs,latPairs,names]=line_greatC_TUV_MASKS_v4;
gcmfaces_lines_transp(lonPairs,latPairs,names);

% now, sample_analysis/basic_diags_compute_v3_or_v4.m
clear fldTRANSPORTS
for j=1:size(flist,1);

  clear ts msk fldU fldV fldBAR X Y FLD fldOV fsave
  ts=flist(j).name(idot);%'0000002232_0000263016';%ts='0000265248_0000501912';
  msk=mygrid.hFacW;
  fldU=readbin([dirRun 'Mean/' 'U' ext 'mean.' ts '.data'],[nx 1350 nz]);
%	[temp,fldU]=get_aste_tracer(fldU,nfx,nfy); old way, now need to put into global faces
	fldU=get_aste_tracer(fldU,nfx,nfy);fldU=aste_tracer2global(fldU,nfx,nfy);
     temp=nan(size(fldU{1}));fldU{2}=temp;
     %temp=nan(size(fldU{5}));temp(1:nfx(4),:,:)=fldU{4};fldU{4}=temp;
     fldU=gcmfaces(fldU);fldU(msk==0)=NaN;clear temp
  msk=mygrid.hFacS;
  fldV=readbin([dirRun 'Mean/' 'V' ext 'mean.' ts '.data'],[nx 1350 nz]);
	%[temp,fldV]=get_aste_tracer(fldV,nfx,nfy);
	fldV=get_aste_tracer(fldV,nfx,nfy);fldV=aste_tracer2global(fldV,nfx,nfy);
     temp=nan(size(fldV{1}));fldV{2}=temp;
     %temp=nan(size(fldV{5}));temp(1:nfx(4),:,:)=fldV{4};fldV{4}=temp;
     fldV=gcmfaces(fldV);fldV(msk==0)=NaN;clear temp

% to get back double: temp=squeeze(fldU.f5) for example

% Transports 
% if use uvelmass, skip hfac, else need hfac
  if(strcmp(ext,'mass')==1);
    list_factors={'dh','dz'};
  else;
    list_factors={'hfac','dh','dz'};
  end;
  [temp]=1e-6*calc_transports(fldU,fldV,mygrid.LINES_MASKS,list_factors);
  fldTRANSPORTS(:,:,j)=temp;

end;

%viewing

ix=1:225;iy=406:900;nxp=540;nyp=900;[yy,xx]=meshgrid(1:nyp,1:nxp);
xc=readbin([dirRoot 'GRID/XC.data'],[270 1350]);xc=get_aste_tracer(xc,nfx,nfy);xc(ix,iy)=-9999;
yc=readbin([dirRoot 'GRID/YC.data'],[270 1350]);yc=get_aste_tracer(yc,nfx,nfy);yc(ix,iy)=-9999;
hf=readbin([dirGrid 'hFacC.data'],[270 1350]);hf=get_aste_tracer(hf,nfx,nfy);hf(ix,iy)=0;
ba=readbin([dirGrid 'Depth.data'],[270 1350]);ba=get_aste_tracer(ba,nfx,nfy);ba(ix,iy)=nan;
%hf=readbin([dirRoot 'GRID/hFacC.data'],[270 1350]);hf=get_aste_tracer(hf,nfx,nfy);hf(ix,iy)=0;
%ba=readbin([dirRoot 'GRID/Depth.data'],[270 1350]);ba=get_aste_tracer(ba,nfx,nfy);ba(ix,iy)=nan;
load([dirRoot 'GRID_astemod/TRI_Stri.mat'],'TRI','Stri');
iprof=nan(size(lonPairs));
for k=1:2;ik=dsearch(xc,yc,TRI,lonPairs(:,k),latPairs(:,k),Stri);iprof(:,k)=ik;end;

L=length(latPairs);
figure(1);clf;cc=(1-winter(10));cc(end,:)=[1,1,1];colormap(cc);
subplot(121);i1=85:475;j1=1:405;
  pcolor(i1,j1,ba(i1,j1)');caxis([0,700]);shading flat;thincolorbar;
  shadeland(i1,j1,1-hf(i1,j1)',[.7,.7,.7]);hold on;
  [a,b]=contour(i1,j1,xc(i1,j1)',[-105:15:30],':');set(b,'color',.1.*[1,1,1]);
  [a,b]=contour(i1,j1,yc(i1,j1)',[-30:15:60],':');set(b,'color',.1.*[1,1,1]);
  for k=20:L;plot(xx(iprof(k,:)),yy(iprof(k,:)),'k.-','linewidth',2);end;hold off;
subplot(122);i1=225:540;j1=351:900;
  pcolor(i1,j1,ba(i1,j1)');caxis([0,700]);shading flat;thincolorbar;
  shadeland(i1,j1,1-hf(i1,j1)',[.7,.7,.7]);hold on;
  [a,b]=contour(i1,j1,xc(i1,j1)',[-150:30:150],':');set(b,'color',.1.*[1,1,1]);
  [a,b]=contour(i1,j1,yc(i1,j1)',[60:5:85],':');set(b,'color',.1.*[1,1,1]);
  for k=1:19;plot(xx(iprof(k,:)),yy(iprof(k,:)),'k.-','linewidth',2);end;hold off;
set(gcf,'paperunits','inches','paperposition',[0 0 11 5]);
fpr=[dirOut 'transports_gates.png'];fprintf('%s\n',fpr);print(fpr,'-dpng');

colorder='br';
figure(2);clf;
for j=1:size(flist,1);
  for k=1:L;
    jj=find(fldTRANSPORTS(k,:,j)~=0);if(length(jj)>0);jj=[jj(1):min(jj(end)+2,50)];end;
    a=subplot(5,7,k);
    if(j==size(flist,1));hold on;end;
    if(length(jj)>0);plot(fldTRANSPORTS(k,jj,j),-[jj],'.-','color',colorder(j));end;
    if(j==size(flist,1));plot(0.*jj,-jj,'k-','linewidth',2);hold off;grid;axis tight;
    if(size(flist,1)==1);
    aa={[num2str(k) '.' names{k}],[num2str(sum(fldTRANSPORTS(k,:,1)),3)]};
    elseif(size(flist,1)==2);
    aa={[num2str(k) '.' names{k}],...
        [num2str(sum(fldTRANSPORTS(k,:,1)),3) ' ' num2str(sum(fldTRANSPORTS(k,:,2)),3)]};
    end;
      title(aa);
    a1=get(a,'position');a1(2)=a1(2)-.01;a1(4)=0.90*a1(4);set(a,'position',a1);
    end;
  end;
end
set(gcf,'paperunits','inches','paperposition',[0 0 15.5 8.5]);
fpr=[dirOut RunStr '_transports.png'];fprintf('%s\n',fpr);print(fpr,'-dpng');

fsave=[dirOut RunStr '_transports.mat'];fprintf('%s\n',fsave);
save(fsave,'fldTRANSPORTS','latPairs','lonPairs','names');

