%use gcmfaces package
clear all
dirRoot='/net/nares/raid8/ecco-shared/llc270/global/';
%clear path:

gcmfaces_global

dirGrid=[dirRoot 'GRID/'];
nFaces=5;fileFormat='compact';nx=270;ny=nx*13;nz=50;
list23D={'XC','YC','RAC','DXC','DYC','DXG','DYG','hFacC','hFacW','hFacS','Depth'};
list1D={'RC','RF','DRC','DRF'};
grid_load_mod(dirGrid,nFaces,fileFormat,list23D,list1D);
temp=mygrid.hFacC;temp(temp==0)=NaN;temp(temp>0)=1;mygrid.mskC=temp;clear temp
temp=mygrid.hFacW;temp(temp==0)=NaN;temp(temp>0)=1;mygrid.mskW=temp;clear temp
temp=mygrid.hFacS;temp(temp==0)=NaN;temp(temp>0)=1;mygrid.mskS=temp;clear temp

%get Atlantic mask:
hf=convert2array(mygrid.hFacC);
msk=hf(:,:,1);;msk(find(msk~=1))=0;msk(:,1:376)=0;	%to southern tip of Africa
for j=376:681;
  kk=50:250;jj=find(msk(kk,j)==0);ibeg=kk(jj(1));
  kk=900:270*4;jj=find(msk(kk,j)==0);iend=max(900,kk(jj(1)));
  msk(ibeg:iend,j)=0;
end;
%East-Atlantic
msk(91:135,511:520)=hf(91:135,511:520,1);msk(98:120,620:622)=0;msk(91:110,652:653)=hf(91:110,652:653,1);
%Pacific:
msk(906:916,548:550)=0;msk(500:850,682:810)=0;
%misc:
msk(956,487)=0;msk(266:350,789:831)=0;
mskAtl=mk3D_mod(msk,hf);
%now convert back to gcmfaces
mskAtl=convert2vector(mskAtl);mskAtl=mskAtl.*mygrid.hFacC;

% separate Atlantic and World fields
RAC   =mygrid.RAC;
YC    =mygrid.YC;
RACAtl=mskAtl(:,:,1).*mygrid.RAC;
YCAtl =mskAtl(:,:,1).*mygrid.YC;ii=find(mskAtl(:,:,1)==0);YCAtl(ii)=nan;clear ii

%line to add in additional fields to mygrid:
%fieldn={'mskW','mskS'};field={'hFacW','hFacS'};
%for k=1:length(field)
%  eval(['mygrid.' fieldn{k} '=rdmds2gcmfaces([dirGrid ''' field{k} '*'']);']);
%  temp=eval(['mygrid.' fieldn{k}]);temp(temp==0)=NaN;temp(temp>0)=1;eval(['mygrid.' fieldn{k} '=temp;']);
%  clear temp
%end;

%gcmfaces_calc/gcmfaces_lines_zonal.m -> add mygrid.LATS_MASKS
%for Atlantic only
LATS_VAL_Atl=[-40:85]';
MSKS_NAM={'mskCint','mskCedge','mskWedge','mskSedge'};
mygrid.RAC=RACAtl;mygrid.YC=YCAtl;

mygrid.LATS_VAL=LATS_VAL_Atl;mygrid.LATS_MASKS=[];
gcmfaces_lines_zonal(LATS_VAL_Atl,MSKS_NAM);
LATS_MASKSAtl=mygrid.LATS_MASKS;

%for whole world:
LATS_VAL=[-89:89]';
MSKS_NAM={'mskCint','mskCedge','mskWedge','mskSedge'};
mygrid.RAC=RAC;mygrid.YC=YC;

mygrid.LATS_VAL=LATS_VAL;mygrid.LATS_MASKS=[];
gcmfaces_lines_zonal(LATS_VAL,MSKS_NAM);
LATS_MASKS=mygrid.LATS_MASKS;

% now model run specifics:
RunStr='BiHarmonic1';dirRun=[dirRoot 'output/' RunStr '/'];%dt=1800;yrStart=1992;
dirOut=[dirRun 'matlab/+ocean/'];fprintf('%s\n',dirOut);if(exist(dirOut)==0);mkdir(dirOut);end;

flist=dir([dirRun 'Umean.*.data']);idot=find(flist(1).name=='.');idot=idot(1)+1:idot(2)-1;

% now, sample_analysis/basic_diags_compute_v3_or_v4.m
for j=1:size(flist,1);
  clear ts msk fldU fldV fldBAR X Y FLD fldOV fsave
  ts=flist(j).name(idot);%'0000265248_0000501912';%'0000002232_0000263016';
  msk=mygrid.hFacC;
  fldU=readbin([dirRun 'Umean.' ts '.data'],[nx ny nz]);
	[temp,fldU]=patchface3D(nx,ny,nz,fldU,2);fldU=gcmfaces(fldU);fldU(msk==0)=NaN;
  fldV=readbin([dirRun 'Vmean.' ts '.data'],[nx ny nz]);
	[temp,fldV]=patchface3D(nx,ny,nz,fldV,2);fldV=gcmfaces(fldV);fldV(msk==0)=NaN;

% to get back double: temp=squeeze(fldU.f5) for example
% barotropic stream fxn:
  mygrid.RAC=RAC;mygrid.YC=YC;
  [fldBAR]=calc_barostream(fldU,fldV);
%viewing
  %temp=convert2array(fldBAR);temp=patchface3D(nx*4,nx*4,1,temp,0);temp=get_aste_tracer(temp,nfx,nfy);
  [X,Y,FLD]=convert2pcol(mygrid.XC,mygrid.YC,fldBAR);
  cc=[[-200:40:-40] [-30:10:80]];
  figure(1);clf;pcolor(X,Y,FLD); axis([-180 180 -90 90]); set(gcf,'Renderer','zbuffer'); shading interp;
  colormap(jet(16)); caxis([-50 50]); colorbar; title('Horizontal Stream Function');

% overturning:
  mygrid.RAC=RACAtl;mygrid.YC =YCAtl;mygrid.LATS_VAL=LATS_VAL_Atl;mygrid.LATS_MASKS=LATS_MASKSAtl;
  [fldOVAtl]=calc_overturn(fldU,fldV);
  LATS1=LATS_VAL_Atl;RF=squeeze(mygrid.RF)';
  X1a=LATS1*ones(1,length(RF)); Y1a=ones(length(LATS1),1)*RF;

  mygrid.RAC=RAC;mygrid.YC=YC;mygrid.LATS_VAL=LATS_VAL;mygrid.LATS_MASKS=LATS_MASKS;
  [fldOV]=calc_overturn(fldU,fldV);
  LATS=LATS_VAL;RF=squeeze(mygrid.RF)';
  X1=LATS*ones(1,length(RF)); Y1=ones(length(LATS),1)*RF;
  cc=[[-50:10:-30] [-24:3:24] [30:10:50]];
  figure(2);clf;
  subplot(211);pcolor(X1a,Y1a,fldOVAtl); axis([-90 90 -6000 0]); set(gcf,'Renderer','zbuffer'); shading interp;
  colormap(jet(16)); caxis([-25 25]); colorbar; title('Atlantic Meridional Stream Function');

  subplot(212);pcolor(X1,Y1,fldOV); axis([-90 90 -6000 0]); set(gcf,'Renderer','zbuffer'); shading interp;
  colormap(jet(16)); caxis([-25 25]); colorbar; title('Full Meridional Stream Function');

  %fsave=[dirOut 'streamfxnAtl.' ts '.mat'];fprintf('%s\n',fsave);
  fsave=[dirOut 'streamfxn.' ts '.mat'];fprintf('%s\n',fsave);
  save(fsave,'fldBAR','fldOV','fldOVAtl','X','Y','FLD','LATS','LATS1','RF');
end;
