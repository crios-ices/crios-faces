%use gcmfaces package
clear all
warning off

global mygrid

dirCur='/nobackupp2/atnguye4/matlab/atn_scripts/';cd(dirCur);
aa=which('grid_load');if(isempty(aa));cd('/nobackupp2/atnguye4/matlab/gcmfaces/');gcmfaces_global;cd(dirCur);end;

%dirRoot='/net/nares/raid8/ecco-shared/llc270/aste_270x450x180/';
dirRoot='/nobackupp2/atnguye4/MITgcm_c65q/mysetups/aste_270x450x180/';

RunStr1=input('RunStr1 [{"run22","run2"}]');Lrun=size(RunStr1,2);
ext   ='mass';%input('ext ["mass" for uvelmass, empty for uvel] ');

% now model run specifics:
nx=270;ny=1350;nz=50;
nf=[450  , 0   , 270 , 180 , 450 ];
nfx=[270 0 270 180 450];nfy=[450 0 270 270 270];

%get grid
fprintf('loading grid\n');tic;
[mygrid,mygrid1]=load_grid_aste();
fprintf('done grid\n');toc;

%clear path:
% now, sample_analysis/basic_diags_compute_v3_or_v4.m
for iRun=1:Lrun
  RunStr=RunStr1{iRun};dirRun=[dirRoot '/' RunStr '/'];dt=1200;yrStart=2002;
  if(exist(dirRun)>0)
    dirOut=[dirRun 'matlab/+ocean/'];if(exist(dirOut)==0);mkdir(dirOut);end;
    flist=dir([dirRun 'Mean/' 'U' ext 'mean.*.data']);
    flist1=dir([dirRun 'Mean/' 'ADVx_TH' 'mean.*.data']);
    if(length(flist)==0);fprintf('Mean files do not exist, skipping %s\n',RunStr);
    else;
    idot=find(flist(1).name=='.');idot=idot(1)+1:idot(2)-1;
    for j=1:1;%size(flist,1);

      clear ts msk fldU fldV fldBAR X Y FLD fldOV fsave fldMT_H fldMT_FW fldTzonmean fldSzonmean
      ts=flist(j).name(idot);%'0000002232_0000263016';%ts='0000265248_0000501912';
      fsave=[dirOut 'streamfxn.' ts '.mat'];
      if(exist(fsave)==0);
        if(length(ext)>0);
          fldU=readbin([dirRun 'Mean/' 'U' ext 'mean.' ts '.data'],[nx ny nz]);
          fldV=readbin([dirRun 'Mean/' 'V' ext 'mean.' ts '.data'],[nx ny nz]);
          fldT=readbin([dirRun 'Mean/' 'Tmean.' ts '.data'],[nx ny nz]);
          fldS=readbin([dirRun 'Mean/' 'Smean.' ts '.data'],[nx ny nz]);
        else;
          fldU=read_slice([dirRun 'diags/TRSP/trsp_3d_set1.' ts '.data'],nx,ny,1:nz);
          fldV=read_slice([dirRun 'diags/TRSP/trsp_3d_set1.' ts '.data'],nx,ny,[1:nz]+nz);
          fldT=read_slice([dirRun 'diags/STATE/state_3d_set1.' ts '.data'],nx,ny,1:nz);
          fldS=read_slice([dirRun 'diags/STATE/state_3d_set1.' ts '.data'],nx,ny,[1:nz]+nz);
        end;
        fldU=convert2gcmfaces(fldU);fldU(mygrid.hFacW==0)=NaN;
        fldV=convert2gcmfaces(fldV);fldV(mygrid.hFacS==0)=NaN;
        fldT=convert2gcmfaces(fldT);fldT(mygrid.hFacC==0)=NaN;
        fldS=convert2gcmfaces(fldS);fldS(mygrid.hFacC==0)=NaN;

        if(size(flist1,1)>0);
          fldADVTX=read_slice([dirRun 'diags/TRSP/trsp_3d_set2.' ts '.data'],nx,ny,3);
             fldADVTX=convert2gcmfaces(fldADVTX);fldADVTX(mygrid.hFacW(:,:,1)==0)=NaN;
          fldADVTY=read_slice([dirRun 'diags/TRSP/trsp_3d_set2.' ts '.data'],nx,ny,4);
             fldADVTY=convert2gcmfaces(fldADVTY);fldADVTY(mygrid.hFacS(:,:,1)==0)=NaN;
          fldADVSX=read_slice([dirRun 'diags/TRSP/trsp_3d_set2.' ts '.data'],nx,ny,7);
             fldADVSX=convert2gcmfaces(fldADVSX);fldADVSX(mygrid.hFacW(:,:,1)==0)=NaN;
          fldADVSY=read_slice([dirRun 'diags/TRSP/trsp_3d_set2.' ts '.data'],nx,ny,8);
             fldADVSY=convert2gcmfaces(fldADVSY);fldADVSY(mygrid.hFacS(:,:,1)==0)=NaN;
        else;
          fldADVTX=fldU.*fldT.*mygrid1.VolW; fldADVTX(mygrid.hFacW==0)=NaN;
          fldADVSX=fldU.*fldS.*mygrid1.VolW; fldADVSX(mygrid.hFacW==0)=NaN;
          fldADVTY=fldV.*fldT.*mygrid1.VolS; fldADVTY(mygrid.hFacS==0)=NaN;
          fldADVSY=fldV.*fldS.*mygrid1.VolS; fldADVSY(mygrid.hFacS==0)=NaN;
        end;

% keep original copy of grid:
        mskS=mygrid.mskS;mskW=mygrid.mskW;mskC=mygrid.mskC;

% to get back double: temp=squeeze(fldU.f5) for example
% barotropic stream fxn: (using diffsmooth2D_div_inv)
        mygrid.RAC = mygrid1.RACnoblank;
        mygrid.YC  = mygrid1.YCnoblank;
        [fldBAR] = calc_barostream(fldU,fldV);
%temp=convert2array(fldBAR);temp=patchface3D(nx*4,nx*4,1,temp,0);temp=get_aste_tracer(temp,nfx,nfy);
        [X,Y,FLD]=convert2pcol(mygrid.XC,mygrid.YC,fldBAR);

% barotropic stream fxn for Pacific South of 65degN
        mygrid.RAC  = mygrid1.RACpacific;
        mygrid.YC   = mygrid1.YCpacific;
        mygrid.XC   = mygrid1.XCpacific;
        mygrid.mskS = mygrid1.mskSpacific;
        mygrid.mskW = mygrid1.mskWpacific;
        [fldBARpacific]=calc_barostream(fldU,fldV);
        temp=mygrid.XC.f4;ii=find(temp<0);temp(ii)=temp(ii)+360;mygrid.XC.f4=temp;
        [Xp,Yp,FLDp]=convert2pcol(mygrid.XC,mygrid.YC,fldBARpacific);

%resetting grid;
        mygrid.XC=mygrid1.XC0;mygrid.mskS=mskS;mygrid.mskW=mskW;mygrid.YC=mygrid1.YCnoblank;mygrid.RAC=mygrid1.RACnoblank;

% overturning, for this, will zeroed out face4:
        mygrid.RAC = mygrid1.RACblank4;
        mygrid.YC  = mygrid1.YCblank4;
        if(length(ext)==0);list_factors={'hfac','dh','dz'};else;list_factors={'dh','dz'};end;
        [fldOV]=calc_overturn(fldU,fldV,1,list_factors);
% zonal fields:
        [fldTzonmean]=calc_zonmean_T(fldT);
        [fldSzonmean]=calc_zonmean_T(fldS);
% meridional transports
        [fldMT_H]=1e-15*4e6*calc_MeridionalTransport(fldADVTX,fldADVTY);
        [fldMT_FW]=1e-6/35*calc_MeridionalTransport(fldADVSX,fldADVSY);

        LATS=mygrid1.LATS_VAL;RF=squeeze(mygrid.RF)';

        save(fsave,'fldBAR','fldBARpacific','fldOV','fldMT_H','fldMT_FW','fldTzonmean','fldSzonmean',...
             'X','Y','FLD','Xp','Yp','FLDp','LATS','RF');
        fprintf('%s\n',fsave);
      else;%exist fsave
        load(fsave);fprintf('loading %s\n',fsave);
      end;

%viewing
%     hh(1)=subplot(211);pcolor(X,Y,FLD); axis([-180 180 -40 90]); set(gcf,'Renderer','zbuffer');
%     shading interp;colormap(jet(16)); caxis([-50 50]);title('Horizontal Stream Function');
%     set(hh(1),'position',[0.13 0.5 0.74 0.45]);ll(1)=thincolorbar; 

%     hh(2)=subplot(223);pcolor(Xp,Yp,FLDp); axis([135 235 45 68]); set(gcf,'Renderer','zbuffer');
%     shading interp;colormap(jet(16)); caxis([-50 50]);title('Pacific Horizontal Stream Function');
%     set(hh(2),'position',[.13 .11 .33,.28]);ll(2)=thincolorbar; 

      tmp=convert2gcmfaces(fldBAR);tmp=reshape(tmp,nx,ny);tmp=get_aste_tracer(tmp,nfx,nfy);
      msk=convert2gcmfaces(mygrid.hFacC(:,:,1));msk=reshape(msk,nx,ny);msk(find(msk<1))=0;
      msk=get_aste_tracer(msk,nfx,nfy);msk(1:270,700:900)=0;
      idash=find(ts=='_');dtp=24*3600/dt;
      ts1=ts2dte(str2num(ts(1:idash-1))-dtp,dt,yrStart,1,1);ts2=ts2dte(str2num(ts(idash+1:end))-dtp,dt,yrStart,1,1);
      ts1=ts1(end-3:end);ts2=ts2(end-3:end);

      figure(1);clf;ccmap=jet(21);ccmap(11,:)=[1 1 1];colormap(ccmap);%seismic(25));
      hh(1)=subplot(221);ix=90:430;iy=135:425;mypcolor(ix,iy,tmp(ix,iy)');caxis([-50 50]);
      cc=[-35 -25 15 25];hold on;[aa,bb]=contour(ix,iy,tmp(ix,iy)',cc);hold off;set(bb,'color',.3.*[1 1 1]);
      shadeland(ix,iy,1-msk(ix,iy)',[.7 .7 .7]);text(105,295,'contour:');text(105,270,num2str(cc'));
      text(105,325,'Barotropic Stream Function');text(105,310,['[' ts1 '--' ts2 ']']);
      set(hh(1),'position',[.08 .5 .45 .45]);ll(1)=thincolorbar;
      title(RunStr,'interpreter','none');

      hh(2)=subplot(222);ix=225:540;iy=425:865;ixp=310:500;iyp=500:750;
      mypcolor(ix,iy,tmp(ix,iy)');caxis([-30 30]);
      cc1=[-75 -50 -25];hold on;[aa,bb]=contour(ix,iy,tmp(ix,iy)',cc1);hold off;set(bb,'color',.9.*[1 1 1]);
      cc2=[-6 -3 2];hold on;[aa,bb]=contour(ixp,iyp,tmp(ixp,iyp)',cc2);hold off;set(bb,'color',.1.*[1 1 1]);
      shadeland(ix,iy,1-msk(ix,iy)',[.7 .7 .7]);
      text(450,760,'contour white:');text(450,745,num2str(cc1));
      text(240,760,'contour black (Arctic):');text(240,745,num2str(cc2));
      set(hh(2),'position',[.58 .5 .35 .45]);ll(2)=thincolorbar;

      X1=LATS*ones(1,length(RF)); Y1=ones(length(LATS),1)*RF;
      hh(3)=subplot(212);pcolor(X1,Y1,fldOV); axis([-40 90 -6000 0]); set(gcf,'Renderer','zbuffer');
%      hh(3)=subplot(212);depthStretchPlot('pcolor',{X1(:,1:end),Y1(:,1:end),fldOV});
%      axis([-40 90 -6000 0]); set(gcf,'Renderer','zbuffer');
      shading interp;caxis([-25 25]);%title('Atlantic Meridional Overturning Stream Function');
      text(-30,-5100,['Atlantic Meridional Overturning Stream Function ' ts1 '--' ts2]);
      set(hh(3),'position',[.08 .08 .8,.34]);ll(3)=thincolorbar; 
%      set(hh(3),'position',[.57 .11 .33,.28]);ll(3)=thincolorbar; 
      cc3(2)=15;hold on;[aa,bb]=contour(X1,Y1,fldOV,[cc3(2) cc3(2)]);hold off;set(bb,'color',.1.*[1,1,1]);
      cc3(1)=12;hold on;[aa,bb]=contour(X1,Y1,fldOV,[cc3(1) cc3(1)]);hold off;set(bb,'color',.3.*[1,1,1]);
      text(-30,-5500,['contour: [' num2str(cc3(1:2)) '] Sv']);

      fpr=[dirOut 'streamfxn_' ts '.png'];
      figure(1);set(gcf,'paperunits','inches','paperposition',[0 0 16 14]);print(fpr,'-dpng');fprintf('%s\n',fpr);

      %cc=[[-200:40:-40] [-30:10:80]];
      extstr='';if(size(flist1,1)==0);extstr='est. ';end;
      cc=[[-50:10:-30] [-24:3:24] [30:10:50]];
      L=length(LATS);ilat=4:L;
      figure(2);clf;cc=jet(30);colormap(cc);
      aa(1)=subplot(221);plot(LATS(ilat),fldMT_H(ilat),'.-');grid;set(gca,'Xlim',[-36,85]);%,'Xticklabel',' ');
      title([extstr 'Atlantic meridional\_H\_transport']);set(aa(1),'position',[.13,.63,.32,.30]);xlabel('lat');
      bb=axis;dby=bb(4)-bb(3);dbx=bb(2)-bb(1);text(bb(1)+.05*dbx,bb(3)+.1*dby,RunStr,'interpreter','none');
  
      aa(3)=subplot(223);depthStretchPlot('pcolor',{X1(:,1:end-1),Y1(:,1:end-1),fldTzonmean});
      shading interp;caxis([-2,28]);grid;title('Atlantic zonal\_T\_mean vs LAT');
      set(aa(3),'position',[.13,.08,.335,.45]);thincolorbar;xlabel('lat');ylabel('depth');
  
      aa(2)=subplot(222);plot(LATS(ilat),fldMT_FW(ilat),'.-');grid;set(gca,'Xlim',[-36,85]);%,'Xticklabel',' ');
      title([extstr 'Atlantic meridional\_FW\_transport']);set(aa(2),'position',[.57,.63,.32,.30]);xlabel('lat');

      aa(4)=subplot(224);depthStretchPlot('pcolor',{X1(:,1:end-1),Y1(:,1:end-1),fldSzonmean});
      shading interp;caxis([33,37]);grid;title('Atlantic zonal\_S\_mean vs LAT');
      set(aa(4),'position',[.57,.08,.335,.45]);thincolorbar;xlabel('lat');ylabel('depth');

      fpr=[dirOut 'MeridionalTransport_ZonalMean_' ts '.png'];
      figure(2);set(gcf,'paperunits','inches','paperposition',[0 0 14 8]);print(fpr,'-dpng');fprintf('%s\n',fpr);
    end;%jfile
    end;%length(flist)
  else;
    fprintf('%s ',RunStr);fprintf('does not exist, skip to next RunStr\n');
  end;%exist(dirRun)
end;%iRun
