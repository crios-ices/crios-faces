clear all;
dirRoot='/net/nares/raid8/ecco-shared/llc270/aste_270x450x180/output/';
RunStr=input('RunStr ["run00"] ');

dirIn=[dirRoot RunStr '/matlab/+ocean/'];

flist=dir([dirIn 'streamfxn.*.mat']);

figure(1);clf;

for k=1:size(flist,1);
  str=flist(k).name;
  idot=find(str=='.');ts1=str(idot(1)+1:idot(1)+10);ts2=str(idot(2)-10:idot(2)-1);
  load([dirIn flist(k).name]);

  fbar{k}=FLD;
  fov{k}=fldOV;    Xa =LATS*ones(1,length(RF)); Ya =ones(length(LATS),1)*RF;

  subplot(3,2,3*k-2);pcolor(X,Y,fbar{k});axis([-180 180 -90 90]); set(gcf,'Renderer','zbuffer');
	shading interp;colormap(jet(16)); caxis([-50 50]);thincolorbar;title([RunStr ';' ts1 '-' ts2]);

  if(exist('FLDp','var')>0);
    fbarp{k}=FLDp;
    subplot(3,2,3*k-1);pcolor(Xp,Yp,fbarp{k}); axis([135 235 45 70]); set(gcf,'Renderer','zbuffer');
       shading interp;colormap(jet(16)); caxis([-70 10]);thincolorbar;title('Pacific Horizontal Stream Function');
  end;

  subplot(3,2,3*k-0);pcolor(Xa,Ya,fov{k}); axis([-90 90 -6000 0]); set(gcf,'Renderer','zbuffer'); 
  shading interp;colormap(jet(16)); caxis([-25 25]); thincolorbar; 
  title('Atlantic Meridional Stream Function');

  clear fldBAR fldBARpacific FLD fldOV FLDp

end;

set(gcf,'paperunits','inches','paperposition',[0 0 15 11]);
fpr=[dirIn 'streamfxn.png'];fprintf('%s\n',fpr);print(fpr,'-dpng');
