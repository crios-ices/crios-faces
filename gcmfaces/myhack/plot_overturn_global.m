clear all;
dirRoot='/net/nares/raid8/ecco-shared/llc270/global/output/';
RunStr=input('RunStr ["BiHarmonic1"] ');

dirIn=[dirRoot RunStr '/matlab/+ocean/'];

flist=dir([dirIn 'streamfxn.*.mat']);

figure(1);clf;

for k=1:size(flist,1);
  str=flist(k).name;
  idot=find(str=='.');ts1=str(idot(1)+1:idot(1)+10);ts2=str(idot(2)-10:idot(2)-1);
  load([dirIn flist(k).name]);

  fbar{k}=FLD;
  fov{k}=fldOV;    Xg =LATS1*ones(1,length(RF)); Yg =ones(length(LATS1),1)*RF;
  fovA{k}=fldOVAtl;Xa =LATS*ones(1,length(RF));  Ya =ones(length(LATS),1)*RF;

  subplot(2,2,k);pcolor(X,Y,fbar{k});axis([-180 180 -90 90]); set(gcf,'Renderer','zbuffer');
	shading interp;colormap(jet(16)); caxis([-50 50]);thincolorbar;title([RunStr ';' ts1 '-' ts2]);

  subplot(4,2,k+4);pcolor(Xa,Ya,fovA{k}); axis([-90 90 -6000 0]); set(gcf,'Renderer','zbuffer'); 
  shading interp;colormap(jet(16)); caxis([-25 25]); thincolorbar; title('Atlantic Meridional Stream Function');

  subplot(4,2,k+6);pcolor(Xg,Yg,fov{k}); axis([-90 90 -6000 0]); set(gcf,'Renderer','zbuffer'); shading interp;
  colormap(jet(16)); caxis([-25 25]); thincolorbar; title('Full Meridional Stream Function');

  clear fldBAR FLD fldOV fldOVAtl

end;

set(gcf,'paperunits','inches','paperposition',[0 0 15 8]);
fpr=[dirIn 'streamfxn.png'];fprintf('%s\n',fpr);print(fpr,'-dpng');
