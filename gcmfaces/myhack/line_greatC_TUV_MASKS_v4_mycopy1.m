function [lonPairs,latPairs,names]=line_greatC_TUV_MASKS_v4();

%note for gates: always West-to-East, (North-to-South ?)

gcmfaces_global;

for iy=1:35;
    
    switch iy;
        case 1;  lonPair=[-65 -50];      latPair=[66 66];       name='DavisStrait';
        case 2;  lonPair=[-35 -20];      latPair=[67 65];       name='DenmarkStrait';
        case 3;  lonPair=[-15.20 -7.08]; latPair=[65.07 62.13]; name='IcelandFaroe';
        case 4;  lonPair=[-7.08  -4.51]; latPair=[62.13 58.22]; name='FaroeShetland';
        case 5;  lonPair=[-4.51 8];      latPair=[58.22 62];    name='ShetlandNorway';

        case 6;  lonPair=[-173 -165];    latPair=[65.84 65.84]; name='BeringStrait';
        case 7;  lonPair=[-18.5 1];      latPair=[80.37 80.14]; name='FramStraitEGC';
        case 8;  lonPair=[1 11.39];      latPair=[80.14 79.49]; name='FramStraitWSC';
        case 9;  lonPair=[-62.74 -56.89];latPair=[82.35 82.01]; name='NaresStrait';
        case 10; lonPair=[16.52 24.92];  latPair=[76.72 70.78]; name='BarentsSeaOpening';
        case 11; lonPair=[17.78 60.85];  latPair=[79.06 80.66]; name='SvalbardFSL';
        case 12; lonPair=[60.85 67.32];  latPair=[80.66 76.50]; name='FSLNovayaZemlya';
        case 13; lonPair=[60.85 95.10];  latPair=[80.66 80.72]; name='StAnaTrough';
        case 14; lonPair=[95.10 104.67]; latPair=[80.72 77.35]; name='SvernayaZemlya';
        case 15; lonPair=[-127.78 -123.77];latPair=[69.98 72.20]; name='CAA1';
        case 16; lonPair=[-122.25 -120.24];latPair=[73.83 76.48]; name='CAA2';
        case 17; lonPair=[-111.80 -103.74];latPair=[78.26 78.97]; name='CAA3';
        case 18; lonPair=[-81.89 -81.77]; latPair=[73.27 75.16]; name='CAAd';
        case 19; lonPair=[-80.41 -68.01]; latPair=[77.58 77.19]; name='NaresStraitd';

        case 20; lonPair=[-81 -77]; latPair=[28 26];       name='FloridaStrait';
        case 21; lonPair=[-81 -79]; latPair=[28 22];       name='FloridaStraitW1';
        case 22; lonPair=[-76 -76]; latPair=[21 8];        name='FloridaStraitS1';
        case 23; lonPair=[-77 -77]; latPair=[26 24];       name='FloridaStraitE1';
        case 24; lonPair=[-77 -77]; latPair=[24 22];       name='FloridaStraitE2';
        case 25; lonPair=[-76 -72]; latPair=[21 18.5];     name='FloridaStraitE3';
        case 26; lonPair=[-72 -72]; latPair=[18.5 10];     name='FloridaStraitE4';
%Talley, 
        case 27; lonPair=[-80 -73]; latPair=[33.5 33.5];   name='GS33p5N(90Sv)';%90Sv
        case 28; lonPair=[-69 -69]; latPair=[40.0 34.5];   name='GS69W';
        case 29; lonPair=[-65 -65]; latPair=[42.5 36.0];   name='GS65W(140Sv)';%140Sv
        case 30; lonPair=[-46 -46]; latPair=[47.5 44.0];   name='GS46NNAC';    %FlemishCap,NAC
        case 31; lonPair=[-46 -46]; latPair=[44.0 41.0];   name='GS46NSouth';  %Southernturn
        case 32; lonPair=[-45 -35]; latPair=[50 50];       name='NAC50N';	    %S of Ch.GibbsFZ
%Talley, p270, Brazil current
	   case 33; lonPair=[-40 -36.5];latPair=[-15 -15];    name='Brazil15S(4Sv)'; %4Sv
	   case 34; lonPair=[-50 -44];  latPair=[-27 -27];    name='Brazil27S(11Sv)';%11Sv
	   case 35; lonPair=[-52 -47.5];latPair=[-31 -31];    name='Brazil31S(17Sv)';%17Sv
    end;
    
    if iy==1; lonPairs=lonPair; latPairs=latPair; names={name};
    else; lonPairs(iy,:)=lonPair; latPairs(iy,:)=latPair; names{iy}=name;
    end;
    
end;

