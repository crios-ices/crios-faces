function [fldOut,X,Y,weightOut]=calc_zonmean_T(fldIn,method,fldType);
% CALC_ZONMEAN_T(budgIn,method,fldType)
%    computes zonal average of fldIn (or its fields recursively).
%
%    If method is 1 (default) then mskCedge (from mygrid.LATS_MASKS) 
%    and volume elements used; if method is 2 then mskCedge and surface 
%    elements are used; if method is -1 or -2 then mskC is used (from mygrid) 
%    instead of mskCedge to define the averaging footpring.
%    
%    If fldType is 'intensive' (default) then fldIn is mutliplied by 
%    RAC (method=2 or -2) or RAC.*hFacC*DRF (method=1 or -1).

global mygrid;

if isempty(who('fldType')); fldType='intensive'; end;
if isempty(who('method')); method=1; end;

if isa(fldIn,'struct');
  list0=fieldnames(fldIn);
  fldOut=[];
  for vv=1:length(list0);
    tmp1=getfield(fldIn,list0{vv});
    if isa(tmp1,'gcmfaces');
      [tmp2,X,Y,weightOut]=calc_zonmean_T(tmp1,method,fldType);
      fldOut=setfield(fldOut,list0{vv},tmp2);
    end;
  end;
  return;
end;

%initialize output:
%atn: 09Jul2017: add in hooks to allow pass in already array format:
if isa(fldIn,'gcmfaces')
  n3=max(size(fldIn.f1,3),1); n4=max(size(fldIn.f1,4),1);
elseif isa(fldIn,'double');
  n3=max(size(fldIn,3),1);    n4=max(size(fldIn,4),1);
end;
fldOut=NaN*squeeze(zeros(length(mygrid.LATS_MASKS),n3,n4));	%LATS_MASKS: -89:89, length=179
weightOut=NaN*squeeze(zeros(length(mygrid.LATS_MASKS),n3,n4));	%[179 nz] or [179 nt]

%use array format to speed up computation below:
%atn: 09Jul2017: add in hooks to allow pass in already array format:
if isa(fldIn,'gcmfaces');
  fldIn=convert2gcmfaces(fldIn);		%[364500 1 nz]
end;
n1=size(fldIn,1); n2=size(fldIn,2);		%[364500 1]
fldIn=reshape(fldIn,n1*n2,n3*n4);		%[364500 nz] or [364500 nt]

%set rac and hFacC according to method
if abs(method)==1;
  rac=reshape(convert2gcmfaces(mygrid.RAC),n1*n2,1)*ones(1,n3*n4);	%[364500 nz] or [364500 nt]
  if n3==length(mygrid.RC);
%old code:
     %hFacC=reshape(convert2array(mygrid.hFacC),n1*n2,n3*n4);
%done old code
      hFacC=reshape(convert2gcmfaces(mygrid.hFacC),n1*n2,n3);		%[364500 nz]
      hFacC=repmat(hFacC,[1 n4]);					%[364500 nz]
      DRF=repmat(mygrid.DRF',[n1*n2 n4]);				%[364500 nz]
  else;
      hFacC=reshape(convert2gcmfaces(mygrid.mskC(:,:,1)),n1*n2,1)*ones(1,n3*n4);	%[364500 nt]
      hFacC(isnan(hFacC))=0;
      DRF=repmat(mygrid.DRF(1),[n1*n2 n3*n4]);						%[364500 nt]
  end;
  weight=rac.*hFacC.*DRF;						%[364500 nz] or [364500 nt]
%atn: clear memory:
clear rac hFacC DRF
else;
  weight=mygrid.mskC(:,:,1).*mygrid.RAC;
  weight=reshape(convert2gcmfaces(weight),n1*n2,1)*ones(1,n3*n4);
end;

%masked area only:
weight(isnan(fldIn))=0;
weight(isnan(weight))=0;						%[364500 nz] or [364500 nt]
mask=weight; mask(weight~=0)=1;						%[364500 nz] or [364500 nt]
fldIn(isnan(fldIn))=0;							%[364500 nz] or [364500 nt]

ny=length(mygrid.LATS_MASKS);						%179
for iy=1:ny;

  if method>0;    
    %get list of points that form a zonal band:
    mm=convert2gcmfaces(mygrid.LATS_MASKS(iy).mskCedge);		%[364500 1]
    mm=find(~isnan(mm)&mm~=0);						%wet longitude along this lat
%10.Jun.2016: note: due to truncation of faces, mm can be empty
  else;
    if iy>1&iy<ny;
      tmpMin=0.5*(mygrid.LATS(iy-1)+mygrid.LATS(iy));
      tmpMax=0.5*(mygrid.LATS(iy)+mygrid.LATS(iy+1));
    elseif iy==1;
      tmpMin=-Inf;
      tmpMax=0.5*(mygrid.LATS(iy)+mygrid.LATS(iy+1));
    elseif iy==ny;
      tmpMin=0.5*(mygrid.LATS(iy-1)+mygrid.LATS(iy));
      tmpMax=+Inf;
    end;
    mm=convert2gcmfaces(mygrid.YC>=tmpMin&mygrid.YC<tmpMax);
    mm=find(~isnan(mm)&mm~=0);
  end;

  if(length(mm)>0);
    if strcmp(fldType,'intensive');
      tmp1=nansum(fldIn(mm,:).*weight(mm,:),1)./nansum(weight(mm,:),1);	%[1 nz] or [1 nt]
      tmp2=nansum(weight(mm,:),1);					%[1 nz] or [1 nt]
    else;
      tmp1=nansum(fldIn(mm,:).*mask(mm,:),1)./nansum(weight(mm,:),1);
      tmp2=nansum(weight(mm,:),1);
    end;
  else;
    tmp1=nan.*nansum(weight(1,:),1);tmp2=tmp1;				%[1 nz] or [1 nt]
  end;

  %store:
  fldOut(iy,:,:)=reshape(tmp1,n3,n4);					%[1 nz] or [1 nt]
  weightOut(iy,:,:)=reshape(tmp2,n3,n4);				%[1 nz] or [1 nt]
    
end;

X=[]; Y=[];								%if nt, returns empty X,Y
if size(fldOut,2)==length(mygrid.RC);
    X=mygrid.LATS*ones(1,length(mygrid.RC));
    Y=ones(length(mygrid.LATS),1)*(mygrid.RC');
%atn: fldOut is squeeze so its 2nd dimenion is missing, best to test fldIn
%elseif size(fldOut,2)==n3;	%n3 can not be nz here because already matches if above (atn)
elseif size(fldOut,2)==1;
    X=mygrid.LATS;
    Y=ones(length(mygrid.LATS),1);
end;

%output:
%fldOut 	[179 nz] or [179 nt] or [179 1]
%weightOut 	[179 nz] or [179 nt] or [179 1]
%X              [179 nz] or empty    or [179 1]
%Y              [179 nz] or empty    or [179 1]
