function [budgO,budgI,budgOI]=calc_budget_mass(kBudget);
% CALC_BUDGET_MASS(kBudget,doMoreBudgetOutput)
%
% note: within this routine 'ETAN','SIheff', and 'SIhsnow' denote
% the corresponding tendencies as computed by diags_diff_snapshots.m
% rather than the state variables themselves

gcmfaces_global;

%get variables from caller routine:
%----------------------------------
%fprintf('inside calc_budget_mass\n');keyboard;
global myparms;

list_variables={'ETAN','SIheff','SIhsnow','oceFWflx','SIatmFW','oceFWflx',...
                'UVELMASS','VVELMASS',...
                'ADVxHEFF','ADVxSNOW','DFxEHEFF','DFxESNOW',...
                'ADVyHEFF','ADVySNOW','DFyEHEFF','DFyESNOW'};

for vv=1:length(list_variables);
  v = evalin('caller',list_variables{vv});
  eval([list_variables{vv} '=v;']);
end;
clear v;

test3d=length(size(UVELMASS{1}))>2;

if test3d|kBudget>1;
  list_variables={'WVELMASS'};
  for vv=1:length(list_variables);
    v = evalin('caller',list_variables{vv});
    eval([list_variables{vv} '=v;']);
  end;
  clear v;
end;

%compute mapped budget:
%----------------------
%keyboard
%mass = myparms.rhoconst * sea level
budgO.tend=ETAN*myparms.rhoconst;			%[m/s * kg/m3] = kg/s * m2
budgI.tend=(SIheff*myparms.rhoi+SIhsnow*myparms.rhosn);	%[m/s * kg/m3] = kg/s * m2
%for deep ocean layer :
if kBudget>1&myparms.useNLFS<2;
% budgO.tend=0;			<-- error due to wrong size for mk3D
  budgO.tend=0.*mygrid.RAC;
  fprintf('inside loop 1\n');
elseif kBudget>1;%rstar case
  tmp1=mk3D(mygrid.DRF,mygrid.hFacC).*mygrid.hFacC;		%m
  tmp2=sum(tmp1(:,:,kBudget:length(mygrid.RC)),3)./mygrid.Depth;%unitless
  budgO.tend=tmp2.*ETAN*myparms.rhoconst;			%[m/s*kg/m3] = kg/s * m2
  fprintf('inside loop 2\n');
end;
%
if test3d;
  tmp1=mk3D(mygrid.DRF,mygrid.hFacC).*mygrid.hFacC;
  tmp2=tmp1./mk3D(mygrid.Depth,tmp1);
  budgO.tend=tmp2.*mk3D(ETAN,tmp2)*myparms.rhoconst;
  fprintf('inside test3d loop\n');
end;
%

%atn
%in the case load_grid_aste instead of grid_load
if(isfield(mygrid,'RACnoblank')==1);
  mygrid.RAC=mygrid.RACnoblank;
end;
%fprintf('inside calc_budget_mass\n');
%keyboard
tmptend=mk3D(mygrid.RAC,budgO.tend).*budgO.tend;%kg/s
budgO.fluxes.tend=tmptend;
budgO.tend=nansum(tmptend,3);
budgI.tend=mygrid.RAC.*budgI.tend;%kg/s
budgOI.tend=budgO.tend+budgI.tend;

%vertical divergence (air-sea fluxes or vertical advection)
budgO.zconv=oceFWflx;					%2d
budgI.zconv=SIatmFW-oceFWflx;				%2d
%in virtual salt flux we omit :
if ~myparms.useRFWF; budgO.zconv=0*budgO.zconv; end;	%2d
%for deep ocean layer :
if kBudget>1; budgO.zconv=-WVELMASS*myparms.rhoconst; end;	%2d (Gael output only layer 11)
%
if test3d;
  trWtop=-WVELMASS*myparms.rhoconst;				%3d
  %trWtop(:,:,1)=budgO.zconv;
  trWbot=trWtop(:,:,2:length(mygrid.RC));			%3d
  trWbot(:,:,length(mygrid.RC))=0;				%3d
  %
  budgO.fluxes.trWtop=mk3D(mygrid.RAC,trWtop).*trWtop;		%3d
  budgO.fluxes.trWbot=mk3D(mygrid.RAC,trWbot).*trWbot;%kg/s	%3d
%atn 25Sep2017: need to compute budgO.zonv again here?
  %budgO.zconv=nansum(budgO.fluxes.trWtop-budgO.fluxes,trWbot,3); %is this correct?
else;
  budgO.fluxes.trWtop=-mygrid.RAC.*budgO.zconv; 		%2d
  budgO.fluxes.trWbot=mygrid.RAC*0;%kg/s			%2d
end;
budgI.fluxes.trWtop=-mygrid.RAC.*(budgI.zconv+budgO.zconv); 	%2d
budgI.fluxes.trWbot=-mygrid.RAC.*budgO.zconv;%kg/s		%2d
%

budgO.zconv=mk3D(mygrid.RAC,budgO.zconv).*budgO.zconv;%kg/s	%conflicting
budgI.zconv=mygrid.RAC.*budgI.zconv;%kg/s
budgOI.zconv=budgO.zconv+budgI.zconv;

%horizontal divergence (advection and ice diffusion)
if test3d; 
  %3D UVELMASS,VVELMASS are multiplied by DRF
  %(2D diagnostics are expectedly vertically integrated by MITgcm)
  tmp1=mk3D(mygrid.DRF,UVELMASS);
  UVELMASS=tmp1.*UVELMASS;
  VVELMASS=tmp1.*VVELMASS;
end;
dxg=mk3D(mygrid.DXG,VVELMASS); dyg=mk3D(mygrid.DYG,UVELMASS);
tmpUo=myparms.rhoconst*dyg.*UVELMASS;
tmpVo=myparms.rhoconst*dxg.*VVELMASS;

%atn: add mask to avoid outside of domain:
if(sum(mygrid.ioSize-[270*1350 1])==0);
  sz=size(tmpUo);
  if(length(sz)==2);
    tmpUo(mygrid.mskC2Dblank==1)=NaN;
    tmpVo(mygrid.mskC2Dblank==1)=NaN;
    tmpUo(tmpUo==0)=NaN;
    tmpVo(tmpVo==0)=NaN;
    budgO.hconv=calc_UV_conv(tmpUo,tmpVo);
  elseif(length(sz)==3&sz(3)==length(mygrid.RC));
    tmpUo(mygrid.hFacW==1)=NaN;
    tmpVo(mygrid.hFacS==1)=NaN;
    tmpUo(tmpUo==0)=NaN;
    tmpVo(tmpVo==0)=NaN;
    budgO.hconv=calc_UV_conv(nansum(tmpUo,3),nansum(tmpVo,3));
  end;
end;

%budgO.hconv=calc_UV_conv(nansum(tmpUo,3),nansum(tmpVo,3));

tmpUi=(myparms.rhoi*DFxEHEFF+myparms.rhosn*DFxESNOW+...
       myparms.rhoi*ADVxHEFF+myparms.rhosn*ADVxSNOW);
tmpVi=(myparms.rhoi*DFyEHEFF+myparms.rhosn*DFyESNOW+...
       myparms.rhoi*ADVyHEFF+myparms.rhosn*ADVySNOW);

%atn: add mask to avoid outside of domain:
if(sum(mygrid.ioSize-[270*1350 1])==0);
  sz=size(tmpUi);
  if(length(sz)==2);
    tmpUi(mygrid.mskC2Dblank==1)=NaN;
    tmpVi(mygrid.mskC2Dblank==1)=NaN;
    tmpUi(tmpUi==0)=NaN;
    tmpVi(tmpVi==0)=NaN;
  elseif(length(sz)==3&sz(3)==length(mygrid.RC));
    tmpUi(mygrid.hFacW==1)=NaN;
    tmpVi(mygrid.hFacS==1)=NaN;
    tmpUi(tmpUi==0)=NaN;
    tmpVi(tmpVi==0)=NaN;
  end;
end;

budgI.hconv=calc_UV_conv(tmpUi,tmpVi); %dh needed is alerady in DFxEHEFF etc
%
budgOI.hconv=budgO.hconv;
budgOI.hconv(:,:,1)=budgOI.hconv(:,:,1)+budgI.hconv;
%
budgO.fluxes.trU=tmpUo; budgO.fluxes.trV=tmpVo;%kg/s
budgI.fluxes.trU=tmpUi; budgI.fluxes.trV=tmpVi;%kg/s

