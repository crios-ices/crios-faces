function [lonPairs,latPairs,names]=line_greatC_TUV_MASKS_v4();

%note this file works for llc270
gcmfaces_global;

for iy=1:45;
    
    switch iy;
%        case 1; lonPair=[-173 -164]; latPair=[65.5 65.5]; name='Bering Strait';
%        case 2; lonPair=[-5 -5]; latPair=[34 40]; name='Gibraltar';
%        case 3; lonPair=[-81 -77]; latPair=[28 26]; name='Florida Strait';
%        case 4; lonPair=[-81 -79]; latPair=[28 22]; name='Florida Strait W1';
%        case 5; lonPair=[-76 -76]; latPair=[21 8]; name='Florida Strait S1';
%        case 6; lonPair=[-77 -77]; latPair=[26 24]; name='Florida Strait E1';
%        case 7; lonPair=[-77 -77]; latPair=[24 22]; name='Florida Strait E2';

        case 8; lonPair=[-65 -50]; latPair=[66 66]; name='Davis Strait';
        case 9; lonPair=[-35 -20]; latPair=[67 65]; name='Denmark Strait';
%        case 10; lonPair=[-16 -7]; latPair=[65 62.5]; name='Iceland Faroe';
%        case 11; lonPair=[-6.5 -4]; latPair=[62.5 57]; name='Faroe Scotland';
%        case 12; lonPair=[-4 8]; latPair=[57 62];  name='Scotland Norway';

        case 13; lonPair=[-68 -63]; latPair=[-54 -66]; name='Drake Passage';
        case 14; lonPair=[103 103]; latPair=[4 -1]; name='Indonesia W1';
        case 15; lonPair=[104 109]; latPair=[-3 -8]; name='Indonesia W2';
        case 16; lonPair=[113 118]; latPair=[-8.5 -8.5]; name='Indonesia W3';
        case 17; lonPair=[118 127 ]; latPair=[-8.5 -15]; name='Indonesia W4';
        case 18; lonPair=[127 127]; latPair=[-25 -68]; name='Australia Antarctica';
        case 19; lonPair=[38 46]; latPair=[-10 -22]; name='Madagascar Channel';
        case 20; lonPair=[46 46]; latPair=[-22 -69]; name='Madagascar Antarctica';
        case 21; lonPair=[20 20]; latPair=[-30 -69.5]; name='South Africa Antarctica';
%        case 22; lonPair=[-76 -72]; latPair=[21 18.5]; name='Florida Strait E3';
%        case 23; lonPair=[-72 -72]; latPair=[18.5 10]; name='Florida Strait E4';

%atn 22-Jun-2013
%        case 1; lonPair=[-173 -165];     latPair=[65.84 65.84]; name='Bering Strait';
%        case 2; lonPair=[-6 -6]; 	 latPair=[35.6926 36.4386]; name='Gibraltar';
        case 1; lonPair=[-173 -165];     latPair=[65.8 65.8]; name='Bering Strait';
        case 2; lonPair=[-6.17 -6.17]; 	 latPair=[35.6926 36.4386]; name='Gibraltar';
        case 10; lonPair=[-15.20 -7.08]; latPair=[65.07 62.13]; name='Iceland Faroe';
        case 11; lonPair=[-7.08  -4.51]; latPair=[62.13 58.22]; name='Faroe Scotland';
        case 12; lonPair=[-4.51 8];      latPair=[58.22 62];  name='Scotland Norway';

        case 3; lonPair=[-81.17 -78.5];  latPair=[26.8 26.8]; name='Florida Strait';
        case 4; lonPair=[-81.17 -81.17]; latPair=[26.8 22.6]; name='Florida Strait W1';
        case 5; lonPair=[-75.83 -75.83]; latPair=[20.6 8];    name='Florida Strait S1';
%        case 6; lonPair=[-78.5 -78.5];   latPair=[26.8 24.3];  name='Florida Strait E1';
%        case 7; lonPair=[-78.5 -78.5];   latPair=[24.3 21.76]; name='Florida Strait E2';
        case 6; lonPair=[-78.75 -78.75];   latPair=[26.8 24.3];  name='Florida Strait E1';
        case 7; lonPair=[-78.75 -78.75];   latPair=[24.3 21.76]; name='Florida Strait E2';
        case 22; lonPair=[-75.83 -72.17]; latPair=[20.6 19];   name='Florida Strait E3';
        case 23; lonPair=[-72.17 -72.17]; latPair=[19 10.6];   name='Florida Strait E4';

%Arctic
        case 24; lonPair=[-18.5 1];      latPair=[80.37 80.14]; name='FramStraitEGC';
        case 25; lonPair=[1 11.39];      latPair=[80.14 79.49]; name='FramStraitWSC';
        case 26; lonPair=[-62.74 -56.89];latPair=[82.35 82.01]; name='NaresStrait';
        case 27; lonPair=[16.52 24.92];  latPair=[76.72 70.78]; name='BarentsSeaOpening';
        case 28; lonPair=[17.78 60.85];  latPair=[79.06 80.66]; name='SvalbardFSL';
        case 29; lonPair=[60.85 67.32];  latPair=[80.66 76.50]; name='FSLNovayaZemlya';
        case 30; lonPair=[60.85 95.10];  latPair=[80.66 80.72]; name='StAnaTrough';
        case 31; lonPair=[95.10 104.67]; latPair=[80.72 77.35]; name='SvernayaZemlya';
        case 32; lonPair=[-127.78 -123.77];latPair=[69.98 72.20]; name='CAA1';
        case 33; lonPair=[-122.25 -120.24];latPair=[73.83 76.48]; name='CAA2';
        case 34; lonPair=[-111.80 -103.74];latPair=[78.26 78.97]; name='CAA3';
        case 35; lonPair=[-81.89 -81.77]; latPair=[73.27 75.16]; name='CAAd';
        case 36; lonPair=[-80.41 -68.01]; latPair=[77.58 77.19]; name='NaresStraitd';
%Talley, 
        case 37; lonPair=[-80 -73]; latPair=[33.5 33.5];   name='GS33p5N(90Sv)';%90Sv
        case 38; lonPair=[-69 -69]; latPair=[40.0 34.5];   name='GS69W';
        case 39; lonPair=[-65 -65]; latPair=[42.5 36.0];   name='GS65W(140Sv)';%140Sv
        case 40; lonPair=[-46 -46]; latPair=[47.5 44.0];   name='GS46NNAC';    %FlemishCap,NAC
        case 41; lonPair=[-46 -46]; latPair=[44.0 41.0];   name='GS46NSouth';  %Southernturn
        case 42; lonPair=[-45 -35]; latPair=[50 50];       name='NAC50N';      %S of Ch.GibbsFZ
%Talley, p270, Brazil current
        case 43; lonPair=[-40 -36.5];latPair=[-15 -15];    name='Brazil15S(4Sv)'; %4Sv
        case 44; lonPair=[-50 -44];  latPair=[-27 -27];    name='Brazil27S(11Sv)';%11Sv
        case 45; lonPair=[-52 -47.5];latPair=[-31 -31];    name='Brazil31S(17Sv)';%17Sv
   end;
    
    if iy==1; lonPairs=lonPair; latPairs=latPair; names={name};
    else; lonPairs(iy,:)=lonPair; latPairs(iy,:)=latPair; names{iy}=name;
    end;
    
end;

