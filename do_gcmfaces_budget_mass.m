%assume already have mygrid
%attempt to do volume budget
clear all;

global mygrid
mygrid=[];
hhost='pfe';%hhost='sverdrup';
if strcmp(hhost,'sverdrup')
  dirCur='/home/atnguyen/nansen/'; %/*{{{*/
  cd(['~/matlab/gcmfaces/']);gcmfaces_global;cd(dirCur);
  dirGrid='/scratch/atnguyen/aste_270x450x180/GRID_real8/';
  dirRoot='/scratch/atnguyen/aste_270x450x180/';

  %dirIn='/net/barents/raid16/atnguyen/llc90/global/output_ad/r4r1i9/diags/BUDG/';
  %dirIn='/scratch/atnguyen/aste_270x450x180/run_c66h_jra55_rStar_v1q_noicoast_0xx3D_8ts_it0007_pk0000000002/diags/';
  %ext='A';ext1='rStar'%test SIsal0=0 case, can ignore now
  %ext='B';ext1='momhdiv'; %test RFWF=0,NLFS=0
  %ext='';ext1='rStar';
  %ext='';ext1='momhdiv';
  %ext='C';ext1='momhdiv';
  %ext='B';ext1='nocppNLFS';
  ext='_spvollead';ext1='momhdiv';
  dirIn=[dirRoot 'run_c66h_jra55_' ext1 '_sal0sp' ext '_v1q_noicoast_0xx3D_8ts_it0007_pk0000000002/diags/'];
  dirOut=[dirIn '../matlab/'];if(exist(dirOut)==0);mkdir(dirOut);end;
  dirRef=[dirRoot 'run_c66h_jra55_rStar_v1q_noicoast_0xx3D_it0007_pk0000000002/'];

  kBudget=1;	%kBudget=11;
  test3d=1;

  %load('/net/nares/raid11/ecco-shared/ecco-version-4/output/r4/iter9/mat/diags_grid_parms.mat','myparms');
  load([dirRef 'mat/diags_grid_parms.mat'],'myparms');
  myparms.rStar=4;
  myparms.SPHF=0;       %salt_plume_heat_flux resulting from #define SALT_PLUME_VOLUME
  if(strcmp(ext,'A'));myparms.SIsal0=0;end;
  if(~strcmp(ext1,'rStar'));myparms.useNLFS=0;myparms.rStar=0;end;
  if(strcmp(ext,'B'));myparms.useRFWF=0;end;
  if(strcmp(ext,'C'));myparms.useNLFS=2;myparms.rStar=0;end;
  if(strcmp(ext,'_spvollead'));myparms.useNLFS=4;myparms.rStar=0;myparms.SPHF=1;end; 
%/*}}}*/
elseif strcmp(hhost,'pfe')
  dirRoot='/nobackupp2/atnguye4/MITgcm_c65q/mysetups/aste_270x450x180/';
  dirCur='/nobackupp2/atnguye4/matlab/atn_scripts/';
  cd(['/nobackupp2/atnguye4/matlab/gcmfaces/']);;gcmfaces_global;cd(dirCur);
  dirGrid=[dirRoot 'GRID_real8/'];
  dirRef=[dirRoot 'run_c65q_jra55_it0007_pk0000000002_rerun/'];
  %RunStr='run_c65q_1yr_bp_varwei_diag_sal0sp_spvollead_it0008_pk0000000001';extb='';
  RunStr='run_c65q_jra55_it0009_pk0000000002';extb='';%'BUDG/';
  dirIn=[dirRoot RunStr '/diags/' extb];
  dirGrid1=[dirRoot RunStr '/'];
  dirOut=[dirRoot RunStr '/matlab/'];if(exist(dirOut)==0);mkdir(dirOut);end;

  kBudget=1;	%kBudget=11;
  test3d=1;

  load([dirRef 'mat/diags_grid_parms.mat'],'myparms');
  myparms.useNLFS=0;%4;
  myparms.rStar=0;
  myparms.SPHF=1;
  myparms.SIsal0=4;
end;

%nx=90;ny=nx*13;nz=50;/*{{{*/
%t1=168;t2=336;
%dt=(t2-t1)*3600;
%/*}}}*/
use_gcmfaces_hconv=0;
if ~use_gcmfaces_hconv; extfpr='A';else;extfpr='';end;
nx=270;ny=1350;nz=50;nfx=[nx 0 nx 180 450];nfy=[450 0 nx nx nx];
deltaTime=1200;

%get time-steps:
flist=dir([dirIn 'budg2d_snap_set2.*.data']);
idot=find(flist(1).name=='.');idot=idot(1)+1:idot(2)-1;
if1=8;    t1=str2num(flist(if1).name(idot));
if2=if1+1;t2=str2num(flist(if2).name(idot));%t1=2232;t2=4248;
dt=(t2-t1)*deltaTime;
t1str=sprintf('%10.10i',t1);t2str=sprintf('%10.10i',t2);

mygrid=grid_load_raw(dirGrid);
%update hFac:
tmp=rdmds([dirGrid1 'hFacC']);tmp=reshape(tmp,nx*ny,1,nz);mygrid.hFacC=convert2gcmfaces(tmp);
tmp=rdmds([dirGrid1 'hFacW']);tmp=reshape(tmp,nx*ny,1,nz);mygrid.hFacW=convert2gcmfaces(tmp);
tmp=rdmds([dirGrid1 'hFacS']);tmp=reshape(tmp,nx*ny,1,nz);mygrid.hFacS=convert2gcmfaces(tmp);

%hfC=rdmds([dirGrid1 'hFacC']);hfC=reshape(hfC,nx,ny,nz);hfC(find(hfC>0))=1;%identical to mygrid.mskC
%block out obcs:
RAC=reshape(convert2gcmfaces(mygrid.RAC),nx,ny);
if test3d;  RAC3=repmat(RAC,[1 1 nz]); end;
hfC=reshape(convert2gcmfaces(mygrid.hFacC),nx,ny,nz);
DD=reshape(convert2gcmfaces(mygrid.Depth),nx,ny);

hfC1=hfC(:,:,1);hfC1(find(hfC1==0))=nan;
RACg=RAC.*hfC1;
hfC1p=get_aste_tracer(hfC1,nfx,nfy);
  hfC1p(:,845)=nan;hfC1p(:,24)=nan;hfC1p(366,261:262)=nan;	%Last one: Gibraltar Strait
  hfC1p=aste_tracer2compact(hfC1p,nfx,nfy);
RACgp=RAC.*hfC1p;
msk=get_aste_tracer(hfC1p,nfx,nfy);msk(1:270,451:900)=nan;

%first, verifying:
if test3d
  flist=dir([dirIn 'budg2d_hflux_set2.*.data']);a=readbin([dirIn flist(if2).name],[nx ny 1],1,'real*8');%UVELMASS
  flist=dir([dirIn 'budg3d_hflux_set2.*.data']);a3=readbin([dirIn flist(if2).name],[nx ny nz],1,'real*8');%UVELMASS
  for k=1:nz;a3(:,:,k)=a3(:,:,k).*mygrid.DRF(k);end;tmp=a-sum(a3,3);sum(tmp(:))			%1.4434e-13
end;

%Tendency
%budg2d_snap_set1.*.data: 'ETAN    ' 'SIheff  ' 'SIhsnow ' 'SIarea  ' 'sIceLoad' 'PHIBOT  '
fileName='budg2d_snap_set1';varName={'ETAN','SIheff','SIhsnow'};
fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,1,[t1,t2]);
for ifld=1:length(varName);
  eval([strtrim(varName{ifld}) '=(fldOut.' varName{ifld} '(:,:,:,2)-fldOut.' varName{ifld} '(:,:,:,1))./dt;']);
end;
clear fldOut;	%clear memory

% Test Fold /*{{{*/
%flist=dir([dirIn 'budg2d_snap_set1.*.data']);/*{{{2*/
%a=readbin([dirIn flist(if1).name],[nx ny 6],1,'real*8');
%b=readbin([dirIn flist(if2).name],[nx ny 6],1,'real*8');
%c=(b-a)./dt;
%ETAN   =c(:,:,1);
%SIheff =c(:,:,2);
%SIhsnow=c(:,:,3);%/*}}}*/
% /*}}}*/
%mass = myparms.rhoconst * sea level 
budgO.tend=ETAN*myparms.rhoconst;                       %2d, [m/s * kg/m3] = kg/s * m2
budgI.tend=(SIheff*myparms.rhoi+SIhsnow*myparms.rhosn); %2d, [m/s * kg/m3] = kg/s * m2

%for deep ocean layer :
if kBudget>1&(myparms.useNLFS<2|(myparms.useNLFS>=2&myparms.rStar==0));	%if layer 11, and NLFS < 2 (not using rStar)
  budgO.tend=0.*RAC;					%2d
elseif kBudget>1;%rstar case
  tmp1=mk3D_mod(mygrid.DRF,hfC).*hfC;			%3d, m
  tmp2=sum(tmp1(:,:,kBudget:length(mygrid.RC)),3)./DD;	%2d, unitless
  %dETAN/dt*fraction dz/Depth*hFac(*RAC)*rhoconst [m/s(*m2) kg/m3]
  budgO.tend=tmp2.*ETAN*myparms.rhoconst;		%2d, [m/s*kg/m3] = kg/s * m2
end;

%3D, with rStar:
if test3d;
%copy from useNLFS<2 above for ocean interior where dr is not changing, so not expecting any tend?
%but should I take into account upper most layer which is ETAN?
  if(myparms.useNLFS<2|myparms.rStar==0);
    budgO.tend=zeros(nx,ny,nz);%3d
    budgO.tend(:,:,1)=ETAN*myparms.rhoconst; %??
  else;
    tmp1=mk3D_mod(mygrid.DRF,hfC).*hfC;			%3d
    tmp2=tmp1./mk3D_mod(DD,tmp1);			%3d
    %dETAN/dt*fraction dz/Depth*hFac*rhoconst 		%[m/s kg/m3]
    budgO.tend=tmp2.*mk3D_mod(ETAN,tmp2)*myparms.rhoconst;	%3d
  end;
end;

tmptend=mk3D_mod(RAC,budgO.tend).*budgO.tend;		%3d, %kg/s
budgO.fluxes.tend=tmptend;				%3d
budgO.tend=nansum(tmptend,3);				%2d, kg/s
budgI.tend=RAC.*budgI.tend;				%2d, kg/s
%ocean and ice
budgOI.tend=budgO.tend+budgI.tend;

%vertical convergence:
%'budg2d_zflux_set1.' t2str '.meta'/*{{{*/
% 'oceFWflx' 'SIatmFW ' 'TFLUX   ' 'SItflux ' 'SFLUX   ' 'oceQsw  ' 'oceSPflx'
%budg3d_zflux_set2.' t2str '.meta
% 'WVELMASS'
%/*}}}*/
fileName='budg2d_zflux_set1';varName={'oceFWflx','SIatmFW'};
fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,1,t2);
for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut;

%oceFWflx=read_slice([dirIn 'budg2d_zflux_set1.' t2str '.data'],nx,ny,1,'real*8');%kg/m2/s/ %*{{{*//
%SIatmFW =read_slice([dirIn 'budg2d_zflux_set1.' t2str '.data'],nx,ny,2,'real*8');%kg/m2/s
%integrate_for_w.F:
%uTrans(i,j)=uFld(i,j,k,bi,bj)*_dyG(i,j,bi,bj)*deepFacC(k)*rhoFacC(k)*drF(k)*_hFacW(i,j,k,bi,bj)
%vTrans(i,j)=vFld(i,j,k,bi,bj)*_dxG(i,j,bi,bj)*deepFacC(k)*rhoFacC(k)*drF(k)*_hFacS(i,j,k,bi,bj)
%conv2d(i,j)=-( uTrans(i+1,j)-uTrans(i,j) + vTrans(i,j+1)-vTrans(i,j) )
%if select_rStar.NE.0: 
%  wFld(i,j,k,bi,bj)=( conv2d(i,j)*recip_rA(i,j,bi,bj)-rStarDhDt(i,j)*drF(k)*h0FacC(i,j,bi,bj))
%                    *maskC(i,j,k,bi,bj)*recip_deepFac2F(k)*recip_rhoFacF(k)
%else
%  wFld(i,j,k,bi,bj)=( wFld(i,j,k+1,bi,bj)*deepFac2F(k+1)*rhoFacF(k+1)+conv2d(i,j)*recip_rA(i,j,bi,bj)
%                     -rStarDhDt(i,j)*drF(k)*h0FacC(i,j,k,bi,bj))*maskC(i,j,k,bi,bj)*recip_deepFac2F(k)*recip_rhoFacF(k)
%endif %/*}}}*/
%ifdef EXACT_CONSERV (yes): wVel(i,j,k,bi,bj)=wVel(i,j,k,bi,bj)+1/rhoConst*PmEpR(i,j,bi,bj).*maskC(i,j,k,bi,bj) 
if test3d
  fileName='budg3d_zflux_set2';varName={'WVELMASS'};fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,nz,t2);
  eval([strtrim(varName{1}) '=fldOut.' varName{1} '(:,:,:,1);']);clear fldOut;
  %WVELMASS=read_slice([dirIn 'budg3d_zflux_set2.' t2str '.data'],nx,ny,[1:nz]+0*nz,'real*8');%m/s
end;
budgO.zconv=oceFWflx;		%2d, net FW into ocean , +=down, >0 = S decreases , [kg/m2/s]
budgI.zconv=SIatmFW-oceFWflx;	%2d, net FW minus ocean = on ice

%in virtual salt flux we omit :
if ~myparms.useRFWF; budgO.zconv=0*budgO.zconv; end	%2d
%for deep ocean with ONLY ONE layer: (need WVELMASS to be 2d)
%note: base on this, because the bottom is zero, so only take top
if kBudget>1; budgO.zconv=-WVELMASS(:,:,kBudget)*myparms.rhoconst; end; %2d (Gael output only layer 11, not depth integr)
%for deep ocean layer , 3d :
if test3d
  trWtop=-WVELMASS.*myparms.rhoconst;		%3d, note this seems WVELMASS is applied to upper, not lower
 if(myparms.useNLFS<2|myparms.rStar==0);	%atn wrapper
%this is commented out by Gael, why?, atn: add in.  O(10^-3)
%essentially need to remove WVELMASS for the top layer (O(1))
  trWtop(:,:,1)=oceFWflx;			
 end;						%atn wrapper
  trWbot=trWtop(:,:,2:length(mygrid.RC));	%3d
  trWbot(:,:,length(mygrid.RC))=0;
  %
  budgO.fluxes.trWtop=trWtop.*RAC3;		%3d
  budgO.fluxes.trWbot=trWbot.*RAC3;		%3d,kg/s
else
  budgO.fluxes.trWtop=-RAC.*budgO.zconv;	%what does this mean for layer 1 in 3d case?
  budgO.fluxes.trWbot=RAC*0;			%kg/s
end;

%atn:
budgO.fluxes.zconv=budgO.fluxes.trWtop-budgO.fluxes.trWbot;	%kg/s , top minus bottom
%done atn

budgI.fluxes.trWtop=-RAC.*(budgI.zconv+budgO.zconv);	%2d, SIatmFW*RAC [kg/m2/s*m2=kg/s]
budgI.fluxes.trWbot=-RAC.*budgO.zconv;		%2d, kg/s

budgO.zconv=mk3D_mod(RAC,budgO.zconv).*budgO.zconv;	%2d, kg/s , shouldn't need mk3D
budgI.zconv=RAC.*budgI.zconv;%kg/s

budgOI.zconv=budgO.zconv+budgI.zconv;	%this is what GAel has for top to bottom

%horizontal convergence:
%ocean:
if test3d;
  fileName='budg3d_hflux_set2';varName={'UVELMASS','VVELMASS'};
  fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,nz,[t2]);
  for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut;
%/*{{{*/
  %UVELMASS=read_slice([dirIn 'budg3d_hflux_set2.' t2str '.data'],nx,ny,[1:nz]+0*nz,'real*8');
  %VVELMASS=read_slice([dirIn 'budg3d_hflux_set2.' t2str '.data'],nx,ny,[1:nz]+1*nz,'real*8');
  %3D UVELMASS,VVELMASS are multiplied by DRF
  %(2D diagnostics are expectedly vertically integrated by MITgcm) %/*}}}*/
  tmp1=mk3D_mod(mygrid.DRF,UVELMASS);
  UVELMASS=tmp1.*UVELMASS;			%3d, m2/s
  VVELMASS=tmp1.*VVELMASS;			%3d, m2/s
else
  fileName='budg2d_hflux_set2';varName={'UVELMASS','VVELMASS'};
  fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,1,[t2]);
  for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;
  clear fldOut;	%clear memory
  %UVELMASS=read_slice([dirIn 'budg2d_hflux_set2.' t2str '.data'],nx,ny,1,'real*8');%depth integrated   %/*{{{*/
  %VVELMASS=read_slice([dirIn 'budg2d_hflux_set2.' t2str '.data'],nx,ny,2,'real*8');%depth integrated	%/*}}}*/
end;
dxg=reshape(convert2gcmfaces(mygrid.DXG),nx,ny); dyg=reshape(convert2gcmfaces(mygrid.DYG),nx,ny);
dxg=mk3D_mod(dxg,VVELMASS); dyg=mk3D_mod(dyg,UVELMASS);	%3d, [m]
tmpUo=myparms.rhoconst*dyg.*UVELMASS;		%2d or 3d, kg/m3*m*m2/s = [kg/s]
tmpVo=myparms.rhoconst*dxg.*VVELMASS;		%2d or 3d

budgO.fluxes.trU=tmpUo; budgO.fluxes.trV=tmpVo;%2d or 3d

if use_gcmfaces_hconv;
  tmpUo=convert2gcmfaces(tmpUo);		tmpVo=convert2gcmfaces(tmpVo);
  if test3d
    tmp=0.*tmpUo;
    for k=1:nz;
      temp=calc_UV_conv(tmpUo(:,:,k),tmpVo(:,:,k));
      tmp(:,:,k)=temp;
      fprintf('%i ',k);
    end;
    fprintf('\n');
    budgO.fluxes.hconv=reshape(convert2gcmfaces(tmp),nx,ny,nz);%clear tmp	%3d , kg/s
  end
  tmp1=calc_UV_conv(nansum(tmpUo,3),nansum(tmpVo,3));			%2d , kg/s
  budgO.hconv=reshape(convert2gcmfaces(tmp1),nx,ny);clear tmp		%2d , kg/s
else
  if test3d
    tmp=calc_UV_conv_mod(tmpUo,tmpVo,nfx,nfy);
    budgO.fluxes.hconv=cat(2,tmp{1},tmp{3},reshape(tmp{4},nfy(4),nfx(4),nz),reshape(tmp{5},nfy(5),nfx(5),nz));
  end;
  tmp=calc_UV_conv_mod(nansum(tmpUo,3),nansum(tmpVo,3),nfx,nfy);
  budgO.hconv=cat(2,tmp{1},tmp{3},reshape(tmp{4},nfy(4),nfx(4),1),reshape(tmp{5},nfy(5),nfx(5),1));
end;

%budg2d_hflux_set1:%'ADVxHEFF','ADVyHEFF','DFxEHEFF','DFyEHEFF','ADVxSNOW','ADVySNOW','DFxESNOW','DFyESNOW',
fileName='budg2d_hflux_set1';
varName={'ADVxHEFF','ADVyHEFF','DFxEHEFF','DFyEHEFF','ADVxSNOW','ADVySNOW','DFxESNOW','DFyESNOW'};
fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,1,[t2]);
for ifld=1:length(varName);
  eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);
end;
clear fldOut; %clear memory
%/*{{{*/
%ADVxHEFF=read_slice([dirIn 'budg2d_hflux_set1.' t2str '.data'],nx,ny,1,'real*8');
%ADVyHEFF=read_slice([dirIn 'budg2d_hflux_set1.' t2str '.data'],nx,ny,2,'real*8');
%DFxEHEFF=read_slice([dirIn 'budg2d_hflux_set1.' t2str '.data'],nx,ny,3,'real*8');
%DFyEHEFF=read_slice([dirIn 'budg2d_hflux_set1.' t2str '.data'],nx,ny,4,'real*8');
%ADVxSNOW=read_slice([dirIn 'budg2d_hflux_set1.' t2str '.data'],nx,ny,5,'real*8');
%ADVySNOW=read_slice([dirIn 'budg2d_hflux_set1.' t2str '.data'],nx,ny,6,'real*8');
%DFxESNOW=read_slice([dirIn 'budg2d_hflux_set1.' t2str '.data'],nx,ny,7,'real*8');
%DFyESNOW=read_slice([dirIn 'budg2d_hflux_set1.' t2str '.data'],nx,ny,8,'real*8');/*}}}*/
tmpUi=(myparms.rhoi*DFxEHEFF+myparms.rhosn*DFxESNOW+...		%m.m2/s * kg/m3 = [kg/s]
       myparms.rhoi*ADVxHEFF+myparms.rhosn*ADVxSNOW);		%
tmpVi=(myparms.rhoi*DFyEHEFF+myparms.rhosn*DFyESNOW+...
       myparms.rhoi*ADVyHEFF+myparms.rhosn*ADVySNOW);

budgI.fluxes.trU=tmpUi; budgI.fluxes.trV=tmpVi;		%kg/s

if use_gcmfaces_hconv
  tmpUi=convert2gcmfaces(tmpUi);tmpVi=convert2gcmfaces(tmpVi);
  temp=calc_UV_conv(tmpUi,tmpVi); %no dh needed here 
  budgI.hconv=reshape(convert2gcmfaces(temp),nx,ny);		%dh needed is already in DFxEHEFF etc, 2d, kg/s
else
  temp=calc_UV_conv_mod(tmpUi,tmpVi,nfx,nfy);
  budgI.hconv=cat(2,temp{1},temp{3},reshape(temp{4},nfy(4),nfx(4),1),reshape(temp{5},nfy(5),nfx(5),1));
end;

budgOI.hconv=budgO.hconv+budgI.hconv;			%2d, [kg/s]

%now sum up globally:
fid=fopen([dirOut 'BudgetMassA.txt'],'w');
%/*{{{*/
a=budgOI.tend.*hfC1;
b=budgOI.hconv.*hfC1;
c=budgOI.zconv.*hfC1;
budgetOI(1,1)=nansum(a(:))./nansum(RACg(:));			%  0.118696167386604e-06
budgetOI(2,1)=nansum(b(:))./nansum(RACg(:));			% -0.000000000000423e-06 <-- is this real?
budgetOI(3,1)=nansum(c(:))./nansum(RACg(:))			%  0.218547720786622e-06
budgetOI(1)-budgetOI(2)-budgetOI(3)				% -9.985155339959532e-08 (? e-08 over e-06, not small)
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetOI',budgetOI(1)-budgetOI(2)-budgetOI(3)]);
fprintf(fid,'%%budgetOI tend hconv zconv tend-hconv-zconv\n');

a=budgOI.tend.*hfC1p;
b=budgOI.hconv.*hfC1p;
c=budgOI.zconv.*hfC1p;
budgetOIp(1,1)=nansum(a(:))./nansum(RACgp(:));			%  0.119181558519752e-06
budgetOIp(2,1)=nansum(b(:))./nansum(RACgp(:));			% -0.013599291294855e-06 (note diff to above!!!!)
budgetOIp(3,1)=nansum(c(:))./nansum(RACgp(:))			%  0.132780849814940e-06
budgetOIp(1)-budgetOIp(2)-budgetOIp(3)				% -3.319575059848806e-19 !!!!
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetOIp',budgetOIp(1)-budgetOIp(2)-budgetOIp(3)]);
fprintf(fid,'%%budgetOIp tend hconv zconv tend-hconv-zconv\n');

a=budgI.tend.*hfC1;
b=budgI.hconv.*hfC1;
c=budgI.zconv.*hfC1;
budgetI(1,1)=nansum(a(:))./nansum(RACg(:));			% 0.114733710644140e-04
budgetI(2,1)=nansum(b(:))./nansum(RACg(:));			% 0.000000000000000e-04 <-- real?
budgetI(3,1)=nansum(c(:))./nansum(RACg(:))			% 0.114869153191081e-04
budgetI(1)-budgetI(2)-budgetI(3)				% -1.354425469411785e-08
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetI',budgetI(1)-budgetI(2)-budgetI(3)]);
fprintf(fid,'%%budgetI tend hconv zconv tend-hconv-zconv\n');

a=budgI.tend.*hfC1p;
b=budgI.hconv.*hfC1p;
c=budgI.zconv.*hfC1p;
budgetIp(1,1)=nansum(a(:))./nansum(RACgp(:));			% 0.115202897872726e-04
budgetIp(2,1)=nansum(b(:))./nansum(RACgp(:));			%-0.000135996420017e-04 (note diff to above!!!)
budgetIp(3,1)=nansum(c(:))./nansum(RACgp(:))			% 0.115338894292743e-04
budgetIp(1)-budgetIp(2)-budgetIp(3)				% -3.726944967918921e-20 !!!
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetIp',budgetIp(1)-budgetIp(2)-budgetIp(3)]);
fprintf(fid,'%%budgetIp tend hconv zconv tend-hconv-zconv\n');

a=budgO.tend.*hfC1;
b=budgO.hconv.*hfC1;
c=budgO.zconv.*hfC1;
budgetO(1,1) =nansum(a(:))./nansum(RACg(:));			% -0.113546748970273e-04
budgetO(2,1)=nansum(b(:))./nansum(RACg(:));			% -0.000000000000001e-04 <-- real?
budgetO(3,1)=nansum(c(:))./nansum(RACg(:))			% -0.112683675983215e-04
budgetO(1)-budgetO(2)-budgetO(3)				% -8.630729870578462e-08 
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetO',budgetO(1)-budgetO(2)-budgetO(3)]);
fprintf(fid,'%%budgetO tend hconv zconv tend-hconv-zconv\n');

a=budgO.tend.*hfC1p;
b=budgO.hconv.*hfC1p;
c=budgO.zconv.*hfC1p;
budgetOp(1,1) =nansum(a(:))./nansum(RACgp(:));			% -0.114011082287528e-04
budgetOp(2,1)=nansum(b(:))./nansum(RACgp(:));			%  0.000000003507069e-04 !!!
budgetOp(3,1)=nansum(c(:))./nansum(RACgp(:))			% -0.114011085794593e-04
budgetOp(1)-budgetOp(2)-budgetOp(3)				% -3.472835083742631e-19 !!!!
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetOp',budgetOp(1)-budgetOp(2)-budgetOp(3)]);
fprintf(fid,'%%budgetOp tend hconv zconv tend-hconv-zconv\n');
%/*}}}*/
if test3d
%pick a point:
ix=17;iy=226;k=5;
[budgO.fluxes.tend(ix,iy,k) budgO.fluxes.zconv(ix,iy,k) budgO.fluxes.hconv(ix,iy,k)] %-0.0011e6 8.789e6 -8.790e6
[budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)] %-3.725290298461914e-09
k=1:nz;
aa=squeeze([budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)]);%10^-8!
%note: from the plots, hconv starts on iy=24 but zconv and tend are all zeros there! Does it mean the model/*{{{*/
% is not taking this grid into account?  check by mult with hfC1: (not hfC, but hfC1)
%note2: even after mult hfC1, still large residuals at iy=24 (and also on face4).  
% for volume budget, this does not matter because i BALANCED the obcs, so these added up to zeros.
% BUT for heat and salt this would not be true ?
% Go back up there and see if the budgets are different in the sum if the boundary along iy=24, ix=125(f4), 427(f5)
%for k=1:nz;
%  str=['; k=' num2str(k)];
%  a=get_aste_tracer(budgO.fluxes.tend(:,:,k).*hfC1p,nfx,nfy);
%  b=get_aste_tracer(budgO.fluxes.hconv(:,:,k).*hfC1p,nfx,nfy);
%  c=get_aste_tracer(budgO.fluxes.zconv(:,:,k).*hfC1p,nfx,nfy);
%  figure(4);clf;colormap(jet(21));
%  subplot(141);mypcolor(a');cc1=0.2*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;title(['tendency' str]);
%  subplot(142);mypcolor(b');cc1=0.2*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;title(['hconv']);
%  subplot(143);mypcolor(c');cc1=0.2*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;title(['zconv']);
%  subplot(144);mypcolor(a'-b'-c');%cc1=0.2*max(abs(caxis));caxis([-cc1 cc1]);
%               mythincolorbar;grid;title('tend-hconv-zconv');
%  pause;
%end; %/*}}}*/

figure(1);clf;%/*{{{*/
aa=zeros(50,4);
n=1;pt(n,:)=[17 226];ix=pt(n,1);iy=pt(n,2);k=5;
bb =[budgO.fluxes.tend(ix,iy,k) budgO.fluxes.zconv(ix,iy,k) budgO.fluxes.hconv(ix,iy,k)] %-0.366e+11 7.589e+11 -7.956e+11
bbp=[budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)] % 1.909!!!! (conserved!!)
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[bb bbp]);fprintf(fid,'%4i %4i %4i ',[ix iy k]);
fprintf(fid,'%%budgMo tend zconv hconv\n');
k=1:nz;aa(:,n)=squeeze([budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)]);%
subplot(2,2,n);plot(aa(:,n),-[1:50],'.-');grid;xlabel('net budgMo');
  title(['[ix,iy]=[' num2str(pt(n,1)) ',' num2str(pt(n,2)) ']; ' ...
          num2str(max(abs(aa(:,n)))/max(abs(budgO.fluxes.zconv(ix,iy,:))).*100) '%']);

n=2;pt(n,:)=[63 92+450];ix=pt(n,1);iy=pt(n,2);k=1;%a very bad point in the ARctic
bb =[budgO.fluxes.tend(ix,iy,k) budgO.fluxes.zconv(ix,iy,k) budgO.fluxes.hconv(ix,iy,k)] %-0.1977e+09 -8.0786e+09  7.8810e+09
bbp=[budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)] % -1.9347e+05 , 0.1% to 0.002%
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[bb bbp]);fprintf(fid,'%4i %4i %4i ',[ix iy k]);
fprintf(fid,'%%budgMo tend zconv hconv\n');
k=1:nz;aa(:,n)=squeeze([budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)]);%
subplot(2,2,n);plot(aa(:,n),-[1:50],'.-');grid;xlabel('net budgMo');
  title(['[ix,iy]=[' num2str(pt(n,1)) ',' num2str(pt(n,2)) ']; ' ...
          num2str(max(abs(aa(:,n)))/max(abs(budgO.fluxes.zconv(ix,iy,:))).*100) '%']);

n=3;pt(n,:)=[117 37+450];ix=pt(n,1);iy=pt(n,2);k=1
bb =[budgO.fluxes.tend(ix,iy,k) budgO.fluxes.zconv(ix,iy,k) budgO.fluxes.hconv(ix,iy,k)] %-0.2969e+09 -3.5506e+09  3.2561e+09
bbp=[budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)] %-2.3742e+06, 0.8% to 0.07% 
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[bb bbp]);fprintf(fid,'%4i %4i %4i ',[ix iy k]);
fprintf(fid,'%%budgMo tend zconv hconv\n');
k=1:nz;aa(:,n)=squeeze([budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)]);%
subplot(2,2,n);plot(aa(:,n),-[1:50],'.-');grid;xlabel('net budgMo');
  title(['[ix,iy]=[' num2str(pt(n,1)) ',' num2str(pt(n,2)) ']; ' ...
          num2str(max(abs(aa(:,n)))/max(abs(budgO.fluxes.zconv(ix,iy,:))).*100) '%']);

n=4;pt(n,:)=[88 107+450];ix=pt(n,1);iy=pt(n,2);k=1;
bb =[budgO.fluxes.tend(ix,iy,k) budgO.fluxes.zconv(ix,iy,k) budgO.fluxes.hconv(ix,iy,k)] %-0.0445e+09 -2.2443e+09  2.1998e+09
bbp=[budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)] %-0.0017 , conserved.
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[bb bbp]);fprintf(fid,'%4i %4i %4i ',[ix iy k]);
fprintf(fid,'%%budgMo tend zconv hconv\n');
k=1:nz;aa(:,n)=squeeze([budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)]);%
subplot(224);plot(aa(:,n),-[1:50],'.-');grid;xlabel('net budgMo');
  title(['[ix,iy]=[' num2str(pt(n,1)) ',' num2str(pt(n,2)) ']; ' ...
          num2str(max(abs(aa(:,n)))/max(abs(budgO.fluxes.zconv(ix,iy,:))).*100) '%']);

figure(1);set(gcf,'paperunit','inches','paperposition',[0 0 9.5 8.5]);
fpr=[dirOut 'Mass_budget1' extfpr '.png'];print(fpr,'-dpng');
%/*}}}*/
a=get_aste_tracer(budgO.fluxes.tend.*repmat(hfC1p,[1 1 nz]),nfx,nfy);%/*{{{*/
b=get_aste_tracer(budgO.fluxes.hconv.*repmat(hfC1p,[1 1 nz]),nfx,nfy);
c=get_aste_tracer(budgO.fluxes.zconv.*repmat(hfC1p,[1 1 nz]),nfx,nfy);
a(isnan(a))=0;b(isnan(b))=0;c(isnan(c))=0;
klev=[1,2,41];
figure(2);clf;colormap(seismic(21));
for k=1:length(klev);
  k1=klev(k);
  str=['; k=' num2str(k1)];
  subplot(3,4,(k-1)*4+1);mypcolor(a(:,:,k1)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(1:2*nx,1:900,isnan(msk'),[.7 .7 .7]);title(['Mass tend' str]);
  subplot(3,4,(k-1)*4+2);mypcolor(b(:,:,k1)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(1:2*nx,1:900,isnan(msk'),[.7 .7 .7]);title(['Mass hconv']);
  subplot(3,4,(k-1)*4+3);mypcolor(c(:,:,k1)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(1:2*nx,1:900,isnan(msk'),[.7 .7 .7]);title(['Mass zconv']);
  subplot(3,4,(k-1)*4+4);mypcolor(a(:,:,k1)'-b(:,:,k1)'-c(:,:,k1)');cc1=0.05*max(abs(caxis));caxis([-cc1 cc1]);
         mythincolorbar;grid;shadeland(1:2*nx,1:900,isnan(msk'),[.7 .7 .7]);title('Mass tend-hconv-zconv');
end;
figure(2);set(gcf,'paperunit','inches','paperposition',[0 0 14 9]);
fpr=[dirOut 'Mass_budget2' extfpr '.png'];print(fpr,'-dpng');
%/*}}}*/
a=budgO.fluxes.tend.*repmat(hfC1p,[1 1 nz]);%/*{{{*/
b=budgO.fluxes.hconv.*repmat(hfC1p,[1 1 nz]);
c=budgO.fluxes.zconv.*repmat(hfC1p,[1 1 nz]);
a(isnan(a))=0;b(isnan(b))=0;c(isnan(c))=0;
klev=[1,2,41];
ix=76:270;iy=451:450+200;
figure(3);clf;colormap(seismic(21));
for k=1:length(klev);
  k1=klev(k);
  str=['; k=' num2str(k1)];
  subplot(3,4,(k-1)*4+1);mypcolor(ix,iy,a(ix,iy,k1)');cc1=0.04*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         title(['Mass tend' str]);shadeland(ix,iy,isnan(hfC1p(ix,iy)'),[.7 .7 .7]);
  subplot(3,4,(k-1)*4+2);mypcolor(ix,iy,b(ix,iy,k1)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         title(['Mass hconv']);shadeland(ix,iy,isnan(hfC1p(ix,iy)'),[.7 .7 .7]);
  subplot(3,4,(k-1)*4+3);mypcolor(ix,iy,c(ix,iy,k1)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         title(['Mass zconv']);shadeland(ix,iy,isnan(hfC1p(ix,iy)'),[.7 .7 .7]);
  subplot(3,4,(k-1)*4+4);mypcolor(ix,iy,a(ix,iy,k1)'-b(ix,iy,k1)'-c(ix,iy,k1)');cc1=0.05*max(abs(caxis));caxis([-cc1 cc1]);
         mythincolorbar;grid;title('Mass tend-hconv-zconv');shadeland(ix,iy,isnan(hfC1p(ix,iy)'),[.7 .7 .7]);
end;
figure(3);set(gcf,'paperunit','inches','paperposition',[0 0 14 9]);
fpr=[dirOut 'Mass_budget3' extfpr '.png'];print(fpr,'-dpng');
%/*}}}*/
end;%test3d

a=get_aste_tracer(budgO.tend.*hfC1p ,nfx,nfy); d=get_aste_tracer(budgI.tend.*hfC1p,nfx,nfy);%/*{{{*/
b=get_aste_tracer(budgO.hconv.*hfC1p,nfx,nfy); f=get_aste_tracer(budgI.hconv.*hfC1p,nfx,nfy);
c=get_aste_tracer(budgO.zconv.*hfC1p,nfx,nfy); g=get_aste_tracer(budgI.zconv.*hfC1p,nfx,nfy);
a(isnan(a))=0;b(isnan(b))=0;c(isnan(c))=0;
d(isnan(d))=0;f(isnan(f))=0;g(isnan(g))=0;
ix=75:540;iy=1:870;
k=1;
  str=['; k=' num2str(k)];
  figure(4);clf;colormap(seismic(21));
  subplot(241);mypcolor(ix,iy,a(ix,iy,k)');cc1=0.04*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ocn Mass tend, all z']);
  subplot(242);mypcolor(ix,iy,b(ix,iy,k)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ocn Mass hconv, all z']);
  subplot(243);mypcolor(ix,iy,c(ix,iy,k)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ocn Mass zconv, all z']);
  subplot(244);mypcolor(ix,iy,a(ix,iy,k)'-b(ix,iy,k)'-c(ix,iy,k)');cc1=0.05*max(abs(caxis));caxis([-cc1 cc1]);
               mythincolorbar;grid;title('ocean tend-hconv-zconv, all z');
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);
  subplot(245);mypcolor(ix,iy,d(ix,iy,k)');cc1=0.04*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ice Mass tend']);
  subplot(246);mypcolor(ix,iy,f(ix,iy,k)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ice Mass hconv']);
  subplot(247);mypcolor(ix,iy,g(ix,iy,k)');cc1=0.04*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ice Mass zconv']);
  subplot(248);mypcolor(ix,iy,d(ix,iy,k)'-f(ix,iy,k)'-g(ix,iy,k)');cc1=0.05*max(abs(caxis));caxis([-cc1 cc1]);
               mythincolorbar;grid;title('ice tend-hconv-zconv');
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);
figure(4);set(gcf,'paperunit','inches','paperposition',[0 0 14 9]);
fpr=[dirOut 'Mass_budget4' extfpr '.png'];print(fpr,'-dpng');
%/*}}}*/
fclose(fid);
%MISSION ACCOMPLISHED!!! onto heat!
%===================
