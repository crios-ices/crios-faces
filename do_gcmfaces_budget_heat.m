%assume already have mygrid
clear all;

global mygrid
mygrid=[];
hhost='pfe';%hhost='sverdrup';
if strcmp(hhost,'sverdrup')
  dirCur='/home/atnguyen/nansen/';%/*{{{*/
  cd(['~/matlab/gcmfaces/']);gcmfaces_global;cd(dirCur);
  dirGrid='/scratch/atnguyen/aste_270x450x180/GRID_real8/';
  dirRoot='/scratch/atnguyen/aste_270x450x180/';

  %dirIn='/net/barents/raid16/atnguyen/llc90/global/output_ad/r4r1i9/diags/BUDG/';
  %dirIn='/scratch/atnguyen/aste_270x450x180/run_c66h_jra55_rStar_v1q_noicoast_0xx3D_8ts_it0007_pk0000000002/diags/';
  %ext='A';ext1='rStar'%test SIsal0=0 case, can ignore now
  %ext='B';ext1='momhdiv'; %test RFWF=0,NLFS=0
  %ext='';ext1='rStar';
  %ext='';ext1='momhdiv';
  %ext='C';ext1='momhdiv';
  %ext='B';ext1='nocppNLFS';
  ext='_spvollead';ext1='momhdiv';
  dirIn=[dirRoot 'run_c66h_jra55_' ext1 '_sal0sp' ext '_v1q_noicoast_0xx3D_8ts_it0007_pk0000000002/diags/'];
  dirOut=[dirIn '../matlab/'];if(exist(dirOut)==0);mkdir(dirOut);end;
  dirRef=[dirRoot 'run_c66h_jra55_rStar_v1q_noicoast_0xx3D_it0007_pk0000000002/'];

  kBudget=1;    %kBudget=11;
  test3d=1;

  %load('/net/nares/raid11/ecco-shared/ecco-version-4/output/r4/iter9/mat/diags_grid_parms.mat','myparms');
  load([dirRef 'mat/diags_grid_parms.mat'],'myparms');
  myparms.rStar=4;
  myparms.SPHF=0;	%salt_plume_heat_flux resulting from #define SALT_PLUME_VOLUME
  if(strcmp(ext,'A'));myparms.SIsal0=0;end;
  if(~strcmp(ext1,'rStar'));myparms.useNLFS=0;myparms.rStar=0;end;
  if(strcmp(ext,'B'));myparms.useRFWF=0;end;
  if(strcmp(ext,'C'));myparms.useNLFS=2;myparms.rStar=0;end;
  if(strcmp(ext,'_spvollead'));myparms.useNLFS=4;myparms.rStar=0;myparms.SPHF=1;end;
%/*}}}*/
elseif strcmp(hhost,'pfe')
  dirRoot='/nobackupp2/atnguye4/MITgcm_c65q/mysetups/aste_270x450x180/';
  dirCur='/nobackupp2/atnguye4/matlab/atn_scripts/';
  cd(['/nobackupp2/atnguye4/matlab/gcmfaces/']);;gcmfaces_global;cd(dirCur);
  dirGrid=[dirRoot 'GRID_real8/'];
  dirRef=[dirRoot 'run_c65q_jra55_it0007_pk0000000002_rerun/'];
  %RunStr='run_c65q_1yr_bp_varwei_diag_sal0sp_spvollead_it0008_pk0000000001';extb='';
  RunStr='run_c65q_jra55_it0009_pk0000000002';extb='';%'BUDG/';
  dirIn=[dirRoot RunStr '/diags/' extb];
  dirGrid1=[dirRoot RunStr '/'];
  dirOut=[dirRoot RunStr '/matlab/'];if(exist(dirOut)==0);mkdir(dirOut);end;

  kBudget=1;    %kBudget=11;
  test3d=1;

  load([dirRef 'mat/diags_grid_parms.mat'],'myparms');
  myparms.useNLFS=0;%4;
  myparms.rStar=0;
  myparms.SPHF=1;
  myparms.SIsal0=4;
end;

%nx=90;ny=nx*13;nz=50;%/*{{{*/
%t1=168;t2=336;
%dt=(t2-t1)*3600;
%/*}}}*/
use_gcmfaces_hconv=0;
if ~use_gcmfaces_hconv; extfpr='A';else;extfpr='';end;
nx=270;ny=1350;nz=50;nfx=[nx 0 nx 180 450];nfy=[450 0 nx nx nx];
deltaTime=1200;

%get time-steps:
flist=dir([dirIn 'budg2d_snap_set2.*.data']);
idot=find(flist(1).name=='.');idot=idot(1)+1:idot(2)-1;
if1=8;    t1=str2num(flist(if1).name(idot));
if2=if1+1;t2=str2num(flist(if2).name(idot));%t1=2232;t2=4248;
dt=(t2-t1)*deltaTime;
t1str=sprintf('%10.10i',t1);t2str=sprintf('%10.10i',t2);

mygrid=grid_load_raw(dirGrid);
%update hFac:
tmp=rdmds([dirGrid1 'hFacC']);tmp=reshape(tmp,nx*ny,1,nz);mygrid.hFacC=convert2gcmfaces(tmp);
tmp=rdmds([dirGrid1 'hFacW']);tmp=reshape(tmp,nx*ny,1,nz);mygrid.hFacW=convert2gcmfaces(tmp);
tmp=rdmds([dirGrid1 'hFacS']);tmp=reshape(tmp,nx*ny,1,nz);mygrid.hFacS=convert2gcmfaces(tmp);

RAC=convert2gcmfaces(mygrid.RAC);RAC=reshape(RAC,nx,ny);
if test3d;  RAC3=repmat(RAC,[1 1 nz]); end;
%hfC=rdmds(['/net/nares/raid11/ecco-shared/ecco-version-4/output/GRID/' 'hFacC']);
hfC=rdmds([dirGrid1 'hFacC']);hfC=reshape(hfC,nx,ny,nz);
hfC(find(hfC>0))=1;             %this is now identical to mygrid.mskC
DD=reshape(convert2gcmfaces(mygrid.Depth),nx,ny);

%block out obcs:
hfC1=hfC(:,:,1);hfC1(find(hfC1==0))=nan;
RACg=RAC.*hfC1;
hfC1p=get_aste_tracer(hfC1,nfx,nfy);
  hfC1p(:,845)=nan;hfC1p(:,24)=nan;hfC1p(366,261:262)=nan;      %Last one: Gibraltar Strait
  hfC1p=aste_tracer2compact(hfC1p,nfx,nfy);
RACgp=RAC.*hfC1p;
msk=get_aste_tracer(hfC1p,nfx,nfy);msk(1:270,451:900)=nan;

if test3d  %/*{{{*/
%first, verifying:
  flist=dir([dirIn 'budg2d_snap_set2.*.data']);%THETA,SALT
  a=readbin([dirIn flist(if2).name],[nx ny 1],1,'real*8');
  flist=dir([dirIn 'budg3d_snap_set2.*.data']);%THETA,SALT
  a3=readbin([dirIn flist(if2).name],[nx ny nz],1,'real*8');
  tmp=a-sum(a3,3);sum(tmp(:))			%-4.815533284630025e-10

  flist=dir([dirIn 'budg2d_hflux_set2.*.data']);%UVELMASS
  a=read_slice([dirIn flist(if2).name],nx,ny,1,'real*8');
  flist=dir([dirIn 'budg3d_hflux_set2.*.data']);%UVELMASS
  a3=read_slice([dirIn flist(if2).name],nx,ny,[1:nz]+0*nz,'real*8');
  for k=1:nz;a3(:,:,k)=a3(:,:,k).*mygrid.DRF(k);end;
  tmp=a-sum(a3,3);sum(tmp(:))			%-9.439054355839249e-13
 
  flist=dir([dirIn 'budg2d_hflux_set2.*.data']);
  a=read_slice([dirIn flist(if2).name],nx,ny,3,'real*8');%ADVx_TH
  flist=dir([dirIn 'budg3d_hflux_set2.*.data']);
  a3=read_slice([dirIn flist(if2).name],nx,ny,[1:nz]+2*nz,'real*8');%ADVx_TH,3d
  tmp=a-sum(a3,3);sum(tmp(:))			%0
end;
%/*}}}*/
%===================== BLOCK 1: Tendency ======================================================
%ocean
%THETA SALT/*{{{*/
%'UVELMASS' 'VVELMASS' 'ADVx_TH ' 'ADVy_TH ' 'DFxE_TH ' 'DFyE_TH '
%'ADVx_SLT' 'ADVy_SLT' 'DFxE_SLT' 'DFyE_SLT' 'oceSPtnd' 'AB_gT   ' 'AB_gS   '/*}}}*/
%flist=dir([dirIn 'budg3d_snap_set2.*.data']);%THETADR, SALTDR/*{{{*/
%a=read_slice([dirIn flist(if1).name],nx,ny,1:nz,'real*8');
%b=read_slice([dirIn flist(if2).name],nx,ny,1:nz,'real*8');
%THETA=(b-a)/dt;						%3d, m.degC/s
%flist=dir([dirIn 'budg3d_kpptend_set1.*.data']);%THETADR, SALTDR/*}}}*/
AB_gT=0;%read_slice([dirIn 'budg3d_kpptend_set1.' t2str '.data'],nx,ny,[1:nz]+5*nz,'real*8');

if test3d; fileName='budg3d_snap_set2';varName={'THETADR'};n3=nz;
else;      fileName='budg2d_snap_set2';varName={'THETA'};  n3=1;    end;
fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,[t1,t2]);
for ifld=1:length(varName);
  eval([strtrim(varName{ifld}) '=(fldOut.' varName{ifld} '(:,:,:,2)-fldOut.' varName{ifld} '(:,:,:,1))./dt;']);
end;
clear fldOut;   %clear memory
if exist('THETADR','var');  THETA = THETADR; clear THETADR; end;

tmptend=myparms.rcp.*(THETA-AB_gT).*mk3D_mod(RAC,THETA);%kg/m3 J/kg/degC degC/s m3 = [J/s]=Watts
if test3d; budgO.fluxes.tend=tmptend; end;		%3d Watts
budgO.tend=nansum(tmptend,3);				%2d Watts

%ice
% 'ETAN    ' 'SIheff  ' 'SIhsnow ' 'SIarea  ' 'sIceLoad' 'PHIBOT  '
%flist=dir([dirIn 'budg2d_snap_set1.*.data']);
%a=readbin([dirIn flist(if1).name],[nx ny 6],1,'real*8');
%b=readbin([dirIn flist(if2).name],[nx ny 6],1,'real*8');
%c=(b-a)./dt;
%SIheff=c(:,:,2);
%SIhsnow=c(:,:,3);
fileName='budg2d_snap_set1';varName={'SIheff','SIhsnow'};n3=1;
fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,[t1,t2]);
for ifld=1:length(varName);
  eval([strtrim(varName{ifld}) '=(fldOut.' varName{ifld} '(:,:,:,2)-fldOut.' varName{ifld} '(:,:,:,1))./dt;']);
end;
clear fldOut;   %clear memory

budgI.tend=-myparms.flami*(SIheff*myparms.rhoi+SIhsnow*myparms.rhosn).*RAC;%J/kg.kg/m3.m/s m2 = J/s = W

%ocean and ice
budgOI1.tend=budgO.tend+budgI.tend;			%2d, %Watts

%vertical convergence:
%'budg2d_zflux_set1.' t2str '.meta' %/*{{{*/
% 'oceFWflx' 'SIatmFW ' 'TFLUX   ' 'SItflux ' 'SFLUX   ' 'oceQsw  ' 'oceSPflx'
%budg2d_zflux_set2.' t2str '.meta
% 'SRELAX  ' 'TRELAX  ' 'WTHMASS ' 'WSLTMASS' 'oceSflux' 'oceQnet '
% 'SIatmQnt' 'SIaaflux' 'SIsnPrcp' 'SIacSubl'
%/*}}}*/
%%TFLUX: -Qsw(i,j,bi,bj)+(surfaceForcingT(i,j,bi,bj)+surfaceForcingTice(i,j,bi,bj))*Cp*rhoConst %/*{{{*/
%% if (nonlinFreeSurf.GT.0 .AND. useRealFreshWaterFlux ): TFLUX=TFLUX+PmEpR*theta(i,j,1,bi,bj)*Cp
%TFLUX   =read_slice([dirIn 'budg2d_zflux_set1.' t2str '.data'],nx,ny,3,'real*8');%2d, W/m2, total heat flux
%%oceQnet=-1.*Qnet , net heat flux into the ocean, +=down, [W/m2]
%oceQnet =read_slice([dirIn 'budg2d_zflux_set2.' t2str '.data'],nx,ny,6,'real*8');%2d, W/m2, + is down
%oceQsw  =read_slice([dirIn 'budg2d_zflux_set1.' t2str '.data'],nx,ny,6,'real*8');%2d, W/m2
%%% surForcT = surfaceForcingT*Cp*rhoConst, model surface forcing for Temperature [W/m2], >0 increases T
%%surForcT=read_slice([dirIn 'budg2d_zflux_set1.' t2str '.data'],nx,ny,8,'real*8');%2d, W/m2
%%% oceFreez = surfaceForcingTice*Cp*rhoConst (= heating from freezing of sea-water, if allowFreezing=T)
%%oceFreez=read_slice([dirIn 'budg2d_zflux_set1.' t2str '.data'],nx,ny,9,'real*8');%2d, W/m2
%%oceFWflx=read_slice([dirIn 'budg2d_zflux_set1.' t2str '.data'],nx,ny,1,'real*8');%2d, kg/m2/s, -EMPMR
%%SItflux = Same as TFLUX but incl seaice (>0 incr T decr H)
%SItflux =read_slice([dirIn 'budg2d_zflux_set1.' t2str '.data'],nx,ny,4,'real*8');%2d
%TRELAX  =read_slice([dirIn 'budg2d_zflux_set2.' t2str '.data'],nx,ny,2,'real*8');%2d
%/*}}}*/
fileName='budg2d_zflux_set1';varName={'TFLUX','oceQsw','SItflux','SPforcT1'};n3=1;
fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,t2);
for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut;

%SIaaflux= , W/m2, conservative ocn<->seaice adv. heat flux adjust, lots of things added to Qnet, in seaice_growth.F/*{{{*/
%%if ~myparms.useRFWF|~myparms.useNLFS;
  %SIaaflux=read_slice([dirIn 'budg2d_zflux_set2.' t2str '.data'],nx,ny,8,'real*8');	%read up there already
%%else;
%%  SIaaflux=0;
%%end;%/*}}}*/
fileName='budg2d_zflux_set2';varName={'oceQnet','TRELAX','WTHMASS','SIaaflux'};n3=1;
fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,t2);
for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut;

%WTHMASS is output as a single layer without k-index, so assume it's the top layer (not depth integr)
%WTHMASS=wVel(i,j,k,bi,bj)*0.5*(theta(i,j,k,bi,bj)+theta(k,j,km1,bi,bj)) <-- between km1 and k, so TOP surface
if ~myparms.useNLFS;%|myparms.rStar==0;
  %WTHMASS2d=read_slice([dirIn 'budg2d_zflux_set2.' t2str '.data'],nx,ny,3,'real*8');%%C.m/s , read way above
else;
  WTHMASS=0.*WTHMASS;
end;

geothFlux=0;

if myparms.SPHF
%SPforcT1: heat flux at surface due to SALT_PLUME_VOL (note SPforcS1 = oceSPflx)  %/*{{{*/
%%  SPforcT1=read_slice([dirIn 'budg2d_sp_set1.' t2str '.data'],nx,ny,4,'real*8');
%  SPforcT1=read_slice([dirIn 'budg2d_zflux_set1.' t2str '.data'],nx,ny,8,'real*8');%/*}}}*/
  if test3d
%%    oceEPtnd=read_slice([dirIn 'budg3d_hflux_set2.' t2str '.data'],nx,ny,[1:nz]+11*nz,'real*8');%/*{{{*/
%    oceEPtnd=read_slice([dirIn 'budg3d_kpptend_set1.' t2str '.data'],nx,ny,[1:nz]+7*nz,'real*8');%/*}}}*/
    fileName='budg3d_kpptend_set1';varName={'oceEPtnd'};n3=nz;
  else;
    fileName='budg2d_hflux_set2';varName={'oceEPtnd'};n3=1;
  end;
  fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,t2);oceEPtnd=fldOut.oceEPtnd;clear fldOut;
  %SPforcT1: read way above
else;
  SPforcT1=0.*ones(nx,ny);
  oceEPtnd=0.*ones(nx,ny,nz);
end;

if test3d
  %WTHMASS=read_slice([dirIn 'budg3d_zflux_set2.' t2str '.data'],nx,ny,[1:nz]+1*nz,'real*8');%3d, degC.m/s, 1st layer=WTHMASS2d/*{{{*/
  %ADVr_TH=read_slice([dirIn 'budg3d_zflux_set2.' t2str '.data'],nx,ny,[1:nz]+3*nz,'real*8');%3d, degC.m3/s
  %DFrE_TH=read_slice([dirIn 'budg3d_zflux_set2.' t2str '.data'],nx,ny,[1:nz]+4*nz,'real*8');%3d, degC.m3/s
  %DFrI_TH=read_slice([dirIn 'budg3d_zflux_set2.' t2str '.data'],nx,ny,[1:nz]+5*nz,'real*8');%3d, degC.m3/s/*}}}*/
  fileName='budg3d_zflux_set2';varName={'ADVr_TH','DFrE_TH','DFrI_TH'};n3=nz;%did not print out WTHMASS, only need 1st
  fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,t2);
  for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut

%KPP non-local Flux of Pot.Temp
  %%KPPg_TH=read_slice([dirIn 'budg3d_kpp_set1.'   t2str '.data'],nx,ny,[1:nz]+0*nz,'real*8');%3d, degC.m3/s/*{{{*
  %KPPg_TH=read_slice([dirIn 'budg3d_kpptend_set1.'   t2str '.data'],nx,ny,[1:nz]+2*nz,'real*8');%3d, degC.m3/s/*}}}*/
  fileName='budg3d_kpptend_set1';varName={'KPPg_TH'};n3=nz;fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,[t2]);
  for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut
else
  %fileName='budg2d_kpptend_set1';varName={'KPPg_TH'};n3=nz;fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,[t2]);
  %for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut
  KPPg_TH=zeros(nx,ny,nz);		%not yet output for 2d,should output appropriate diag
end;

budgO.zconv=TFLUX+geothFlux+SPforcT1;				%2d
budgI.zconv=-(SItflux+TFLUX-TRELAX+SPforcT1);			%2d
%in linear surface we omit :
if ~myparms.useNLFS;%|myparms.rStar==0; 
  budgO.zconv=budgO.zconv-myparms.rcp*WTHMASS(:,:,kBudget); 	%2d
end;
%in virtual salt flux we omit :
%if ~myparms.useRFWF|~myparms.useNLFS;  %if useRFWF=0 or useNLFS=0
  budgI.zconv=budgI.zconv+SIaaflux; 
%end;

budgO.zdia=budgO.zconv;				%2d
%for deep ocean layer :
if kBudget>1;
  budgO.zconv=-(ADVr_TH+DFrE_TH+DFrI_TH)./RAC*myparms.rcp;%2d (only for layer 11)
  if(myparms.SPHF);budgO.zconv=budgO.zconv+oceEPtnd(:,:,kBudget);end;
  budgO.zdia=-(DFrE_TH+DFrI_TH)./RAC*myparms.rcp;	%2d
  dd=mygrid.RF(kBudget); 
  msk=reshape(convert2gcmfaces(mygrid.mskC(:,:,kBudget)),nx,ny);
  swfrac=0.62*exp(dd/0.6)+(1-0.62)*exp(dd/20);
  if dd<-200; swfrac=0; end;
  budgO.zconv=budgO.zconv+swfrac*oceQsw+geothFlux;%.*msk;%2d
  budgO.zdia=budgO.zdia+swfrac*oceQsw+geothFlux;%.*msk;	%2d, w/o advective
end;
%
%notes: - geothFlux remains to be accounted for in 3D case
%       - diaWtop, diaWbot remain to be implemented in 3D case
if test3d;
  nr=size(ADVr_TH,3);
  trWtop=-(ADVr_TH+DFrE_TH+DFrI_TH+KPPg_TH)*myparms.rcp;	%3d, Watts (degC.m3/s kg/m3 J/kg/degC)
  %
  dd=mygrid.RF(1:end-1);				%50, m
  swfrac=0.62*exp(dd/0.6)+(1-0.62)*exp(dd/20);		%unitless
  swfrac(dd<-200)=0;
  swtop=mk3D_mod(swfrac,trWtop).*mk3D_mod(RAC.*oceQsw,trWtop);	%frac sw penetr top, 3d, Watts
  mskC=reshape(convert2gcmfaces(mygrid.mskC),nx,ny,nz);	%3d
  swtop(isnan(mskC))=0;
  trWtop=trWtop+swtop;					%3d,adv+diff+penetr flx, Watts
  %
  if(myparms.SPHF);
    tmp1=(mk3D_mod(SPforcT1,oceEPtnd)-cumsum(oceEPtnd,3)).*RAC3;
    trWtop(:,:,2:nr)=trWtop(:,:,2:nr)+tmp1(:,:,1:nr-1);
  end;
  trWtop(:,:,1)=budgO.zconv.*RAC;			%no minus, toplayer = surf flx or layer11
  trWbot(:,:,1:length(mygrid.RC)-1)=trWtop(:,:,2:length(mygrid.RC));		%3d
  trWbot(:,:,length(mygrid.RC))=0;			%need to take care of geothFlux somewhere
  %
  budgO.fluxes.trWtop=trWtop;%Watt			%3d
  budgO.fluxes.trWbot=trWbot;%Watt			%3d
else;
  budgO.fluxes.trWtop=-RAC.*(budgO.zconv-geothFlux);	%note [-] sign, not sure why geoth is to the top
  budgO.fluxes.trWbot=RAC.*geothFlux;%Watt		%2d
  budgO.fluxes.diaWtop=-RAC.*(budgO.zdia-geothFlux);	%2d
  budgO.fluxes.diaWbot=RAC.*geothFlux;%Watt		%2d
end;
budgI.fluxes.trWtop=-RAC.*(budgI.zconv+budgO.zconv);	%2d
budgI.fluxes.trWbot=-RAC.*budgO.zconv;%Watt		%2d
%
budgO.fluxes.zconv=budgO.fluxes.trWtop-budgO.fluxes.trWbot;%3d, Watts
budgO.zconv=mk3D_mod(RAC,budgO.zconv).*budgO.zconv;	%Watt,%2d, why mk3D?
budgI.zconv=RAC.*budgI.zconv;%Watt			%2d
budgOI1.zconv=budgO.zconv+budgI.zconv;		%2d

if test3d  %/*{{{*/
%tmp=zeros(nx,ny,nz);
trWtop=zeros(nx,ny,nz);
trWbot=zeros(nx,ny,nz);
for k=nz:-1:1;
  trWtop(:,:,k)=-myparms.rcp.*(ADVr_TH(:,:,k)  +DFrE_TH(:,:,k)  +DFrI_TH(:,:,k))+swtop(:,:,k);
  if(k==nz);
    trWbot(:,:,k)=0;
  elseif(k>1&k<nz);
    trWbot(:,:,k)=trWtop(:,:,k+1);
  end;
  if(k==1);
    trWtop(:,:,k)=TFLUX.*RAC - myparms.rcp*WTHMASS(:,:,k).*RAC;
  end;
end;
budgO.fluxes.zconv1=trWtop-trWbot;clear tmp;%W/m2 m2 = W (kg/m3 J/kg/C C.m/s m2 = J/s)
end;
%/*}}}*/
%horizontal convergence:
% budg2d_hflux_set1.0000000336.meta/*{{{*/
% 'ADVxHEFF' 'ADVyHEFF' 'DFxEHEFF' 'DFyEHEFF' 'ADVxSNOW' 'ADVySNOW' 'DFxESNOW' 'DFyESNOW'
% budg2d_hflux_set2.0000000336.meta
% 'UVELMASS' 'VVELMASS' 'ADVx_TH ' 'ADVy_TH ' 'DFxE_TH ' 'DFyE_TH '
% 'ADVx_SLT' 'ADVy_SLT' 'DFxE_SLT' 'DFyE_SLT' 'oceSPtnd' 'AB_gT   ' 'AB_gS   '/*}}}*/
%/*{{{*/
%ADVx_TH=read_slice([dirIn 'budg3d_hflux_set2.' t2str '.data'],nx,ny,[1:nz]+2*nz,'real*8');
%ADVy_TH=read_slice([dirIn 'budg3d_hflux_set2.' t2str '.data'],nx,ny,[1:nz]+3*nz,'real*8');
%DFxE_TH=read_slice([dirIn 'budg3d_hflux_set2.' t2str '.data'],nx,ny,[1:nz]+4*nz,'real*8');
%DFyE_TH=read_slice([dirIn 'budg3d_hflux_set2.' t2str '.data'],nx,ny,[1:nz]+5*nz,'real*8');/*}}}*/
if test3d; fileName='budg3d_hflux_set2';varName={'ADVx_TH','ADVy_TH','DFxE_TH','DFyE_TH'};n3=nz;
else;      fileName='budg2d_hflux_set2';varName={'ADVx_TH','ADVy_TH','DFxE_TH','DFyE_TH'};n3=1; end;
fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,t2);
for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut

%a=readbin([dirIn 'budg2d_hflux_set1.' t2str '.data'],[nx ny 8],1,'real*8');/*{{{*/
%DFxEHEFF=a(:,:,3);	DFxESNOW=a(:,:,7);	ADVxHEFF=a(:,:,1);	ADVxSNOW=a(:,:,5);
%DFyEHEFF=a(:,:,4);	DFyESNOW=a(:,:,8);	ADVyHEFF=a(:,:,2);	ADVySNOW=a(:,:,6);/*}}}*/
fileName='budg2d_hflux_set1';
varName={'DFxEHEFF','DFyEHEFF','ADVxHEFF','ADVyHEFF','DFxESNOW','DFyESNOW','ADVxSNOW','ADVySNOW'};n3=1;
fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,t2);
for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut

tmpUo=myparms.rcp*(ADVx_TH+DFxE_TH);		%3d, kg/m3.J/kg/degC.degC.m3/s = [W]
tmpVo=myparms.rcp*(ADVy_TH+DFyE_TH);		%3d, kg/m3.J/kg/degC.degC.m3/s = [W]

if test3d
  if use_gcmfaces_hconv;
    tmpUo=convert2gcmfaces(tmpUo);
    tmpVo=convert2gcmfaces(tmpVo);
    tmp=0.*tmpUo;
    for k=1:nz;
%tmpUo(1:end-1,:)+tmpUo(2:end,:)+tmpVo(:,1:end-1)+tmpVo(:,2:end),[W]
      temp=calc_UV_conv(tmpUo(:,:,k),tmpVo(:,:,k));
      tmp(:,:,k)=temp;
      fprintf('%i ',k);
    end;
    fprintf('\n');
    budgO.fluxes.hconv=reshape(convert2gcmfaces(tmp),nx,ny,nz);clear tmp
  else;
    tmp=calc_UV_conv_mod(tmpUo,tmpVo,nfx,nfy);
    budgO.fluxes.hconv=cat(2,tmp{1},tmp{3},reshape(tmp{4},nfy(4),nfx(4),nz),reshape(tmp{5},nfy(5),nfx(5),nz));
  end;
end;

if use_gcmfaces_hconv;
  if ~test3d;tmpUo=convert2gcmfaces(nansum(tmpUo,3));tmpVo=convert2gcmfaces(nansum(tmpVo,3));end;
  tmp1=calc_UV_conv(nansum(tmpUo,3),nansum(tmpVo,3));			%2d , W
  budgO.hconv=reshape(convert2gcmfaces(tmp1),nx,ny);clear tmp1		%2d , W
else;
  tmp=calc_UV_conv_mod(nansum(tmpUo,3),nansum(tmpVo,3),nfx,nfy);
  budgO.hconv=cat(2,tmp{1},tmp{3},reshape(tmp{4},nfy(4),nfx(4),1),reshape(tmp{5},nfy(5),nfx(5),1));
end;

%               J/kg        kg/m^3 m.m^2/s         kg/m^3   m.m^2/s = [W]
tmpUi=-myparms.flami*(myparms.rhoi*DFxEHEFF+myparms.rhosn*DFxESNOW...
                     +myparms.rhoi*ADVxHEFF+myparms.rhosn*ADVxSNOW);
tmpVi=-myparms.flami*(myparms.rhoi*DFyEHEFF+myparms.rhosn*DFyESNOW...
                     +myparms.rhoi*ADVyHEFF+myparms.rhosn*ADVySNOW);
if use_gcmfaces_hconv;
  tmpUi=convert2gcmfaces(tmpUi);tmpVi=convert2gcmfaces(tmpVi);
  temp=calc_UV_conv(tmpUi,tmpVi); %no dh needed here, already built in DFx etc %[W]
  budgI.hconv=reshape(convert2gcmfaces(temp),nx,ny);
else;
  tmp=calc_UV_conv_mod(tmpUi,tmpVi,nfx,nfy);
  budgI.hconv=cat(2,tmp{1},tmp{3},reshape(tmp{4},nfy(4),nfx(4),1),reshape(tmp{5},nfy(5),nfx(5),1));
end;

budgOI1.hconv=budgO.hconv+budgI.hconv;                                  %[W]

%%pick a point to test:/*{{{*/
%ix=211;iy=546;k=2;
%a=[ADVx_TH(ix+1,iy,k)-ADVx_TH(ix,iy,k) ADVy_TH(ix,iy+1,k)-ADVy_TH(ix,iy,k) ADVr_TH(ix,iy,k-1)-ADVr_TH(ix,iy,k)]...
%   .*myparms.rcp./1e8	%[5.3362  -62.8944   28.8366]
%b=[DFxE_TH(ix+1,iy,k)-DFxE_TH(ix,iy,k) DFyE_TH(ix,iy+1,k)-DFyE_TH(ix,iy,k) ...
%   DFrE_TH(ix,iy,k-1)-DFrE_TH(ix,iy,k) DFrI_TH(ix,iy,k-1)-DFrI_TH(ix,iy,k)].*myparms.rcp./1e8 
%  %[-0.0003   -0.0161   -0.4428   -3.8189]
%c=[WTHMASS(ix,iy,k-1)-WTHMASS(ix,iy,k)].*RAC(ix,iy).*myparms.rcp./1e8	%this term shouldn't be needed..., 27.5731
%budgO.tend(ix,iy,k)./1e8	%-2.9638
%budgO.tend(ix,iy,k)./1e8-(sum(a)+sum(b)+c)	%2.4627 , not balanced
%/*}}}*/

%now sum up globally:
fid=fopen([dirOut 'BudgetHeat.txt'],'w');

if test3d
a=budgOI1.tend.*hfC1;%/*{{{*/
b=budgOI1.hconv.*hfC1;
c=budgOI1.zconv.*hfC1;				%wrong SIsal0 run	SIsal0sp
budgetOI(1,1)=nansum(a(:))./nansum(RACg(:));	%-1.671e+02		-1.671e+02
budgetOI(2,1)=nansum(b(:))./nansum(RACg(:));	%-0.000e+02 <-- wrong	 0.000e+02
budgetOI(3,1)=nansum(c(:))./nansum(RACg(:)),	%-1.710e+02		-1.710e+02
budgetOI(1)-budgetOI(2)-budgetOI(3)		% 3.888			 3.893
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetOI',budgetOI(1)-budgetOI(2)-budgetOI(3)]);
fprintf(fid,'%%budgetOI tend hconv zconv tend-hconv-zconv\n');

a=budgOI1.tend.*hfC1p;
b=budgOI1.hconv.*hfC1p;
c=budgOI1.zconv.*hfC1p;
budgetOIp(1,1)=nansum(a(:))./nansum(RACgp(:));	%-1.678e+02		-1.678e+02
budgetOIp(2,1)=nansum(b(:))./nansum(RACgp(:));	% 0.035e+02 <- NOTE diff 0.035e+02
budgetOIp(3,1)=nansum(c(:))./nansum(RACgp(:)),	%-1.714e+02		-1.714e+02
budgetOIp(1)-budgetOIp(2)-budgetOIp(3)		% 0.072 <--better! 0.03% 0.077
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetOIp',budgetOIp(1)-budgetOIp(2)-budgetOIp(3)]);
fprintf(fid,'%%budgetOIp tend hconv zconv tend-hconv-zconv\n');

a=budgI.tend.*hfC1;
b=budgI.hconv.*hfC1;
c=budgI.zconv.*hfC1;
budgetI(1,1)=nansum(a(:))./nansum(RACg(:));	%-3.832			-4.068
budgetI(2,1)=nansum(b(:))./nansum(RACg(:));	% 0.000 <-- wrong	-0.000
budgetI(3,1)=nansum(c(:))./nansum(RACg(:)),	%-3.953			-4.149
budgetI(1)-budgetI(2)-budgetI(3)		% 0.121			 0.081
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetI',budgetI(1)-budgetI(2)-budgetI(3)]);
fprintf(fid,'%%budgetI tend hconv zconv tend-hconv-zconv\n');

a=budgI.tend.*hfC1p;
b=budgI.hconv.*hfC1p;
c=budgI.zconv.*hfC1p;
budgetIp(1,1)=nansum(a(:))./nansum(RACgp(:));	%-3.848			-4.085
budgetIp(2,1)=nansum(b(:))./nansum(RACgp(:));	% 0.005 <-- from where?	 0.0045
budgetIp(3,1)=nansum(c(:))./nansum(RACgp(:))	%-3.969			-4.166
budgetIp(1)-budgetIp(2)-budgetIp(3)		% 0.116 <-- still bad 3% 0.0768, 1.8%
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetIp',budgetIp(1)-budgetIp(2)-budgetIp(3)]);
fprintf(fid,'%%budgetIp tend hconv zconv tend-hconv-zconv\n');

a=budgO.tend.*hfC1;
b=budgO.hconv.*hfC1;
c=budgO.zconv.*hfC1;
budgetO(1,1) =nansum(a(:))./nansum(RACg(:));	%-1.633e+02		-1.631e+02
budgetO(2,1)=nansum(b(:))./nansum(RACg(:));	%-0.000e+02 <-- wrong	-0.000e+02
budgetO(3,1)=nansum(c(:))./nansum(RACg(:))	%-1.671e+02		-1.669e+02
budgetO(1)-budgetO(2)-budgetO(3)		% 3.768	%2.3%		 3.812
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetO',budgetO(1)-budgetO(2)-budgetO(3)]);
fprintf(fid,'%%budgetO tend hconv zconv tend-hconv-zconv\n');

a=budgO.tend.*hfC1p;
b=budgO.hconv.*hfC1p;
c=budgO.zconv.*hfC1p;
budgetOp(1,1) =nansum(a(:))./nansum(RACgp(:));	%-1.640e+02		-1.637e+02
budgetOp(2,1)=nansum(b(:))./nansum(RACgp(:));	% 0.035e+02		 0.035e+02
budgetOp(3,1)=nansum(c(:))./nansum(RACgp(:))	%-1.674e+02		-1.672e+02
budgetOp(1)-budgetOp(2)-budgetOp(3)		%-0.045 <- bad? 0.03%	-4.09e-12 yay!!!
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetOp',budgetOp(1)-budgetOp(2)-budgetOp(3)]);
fprintf(fid,'%%budgetOp tend hconv zconv tend-hconv-zconv\n');
%/*}}}*/
figure(1);clf;%/*{{{*/
aa=zeros(50,4);
n=1;pt(n,:)=[17 226];ix=pt(n,1);iy=pt(n,2);k=5;
bb =[budgO.fluxes.tend(ix,iy,k) budgO.fluxes.zconv(ix,iy,k) budgO.fluxes.hconv(ix,iy,k)] %-0.366e+11 7.589e+11 -7.956e+11
bbp=[budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)] % 1.909!!!! (conserved!!)
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[bb bbp]);fprintf(fid,'%4i %4i %4i ',[ix iy k]);
fprintf(fid,'%%budgHo tend zconv hconv\n');
k=1:nz;aa(:,n)=squeeze([budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)]);%
subplot(2,2,n);plot(aa(:,n),-[1:50],'.-');grid;xlabel('net budgHo');
  title(['[ix,iy]=[' num2str(pt(n,1)) ',' num2str(pt(n,2)) ']; ' ...
          num2str(max(abs(aa(:,n)))/max(abs(budgO.fluxes.zconv(ix,iy,:))).*100) '%']);

n=2;pt(n,:)=[63 92+450];ix=pt(n,1);iy=pt(n,2);k=1;%a very bad point in the ARctic
bb =[budgO.fluxes.tend(ix,iy,k) budgO.fluxes.zconv(ix,iy,k) budgO.fluxes.hconv(ix,iy,k)] %-0.1977e+09 -8.0786e+09  7.8810e+09
bbp=[budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)] % -1.9347e+05 , 0.1% to 0.002%
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[bb bbp]);fprintf(fid,'%4i %4i %4i ',[ix iy k]);
fprintf(fid,'%%budgHo tend zconv hconv\n');
k=1:nz;aa(:,n)=squeeze([budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)]);%
subplot(2,2,n);plot(aa(:,n),-[1:50],'.-');grid;xlabel('net budgHo');
  title(['[ix,iy]=[' num2str(pt(n,1)) ',' num2str(pt(n,2)) ']; ' ...
          num2str(max(abs(aa(:,n)))/max(abs(budgO.fluxes.zconv(ix,iy,:))).*100) '%']);

n=3;pt(n,:)=[117 37+450];ix=pt(n,1);iy=pt(n,2);k=1
bb =[budgO.fluxes.tend(ix,iy,k) budgO.fluxes.zconv(ix,iy,k) budgO.fluxes.hconv(ix,iy,k)] %-0.2969e+09 -3.5506e+09  3.2561e+09
bbp=[budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)] %-2.3742e+06, 0.8% to 0.07% 
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[bb bbp]);fprintf(fid,'%4i %4i %4i ',[ix iy k]);
fprintf(fid,'%%budgHo tend zconv hconv\n');
k=1:nz;aa(:,n)=squeeze([budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)]);%
subplot(2,2,n);plot(aa(:,n),-[1:50],'.-');grid;xlabel('net budgHo');
  title(['[ix,iy]=[' num2str(pt(n,1)) ',' num2str(pt(n,2)) ']; ' ...
          num2str(max(abs(aa(:,n)))/max(abs(budgO.fluxes.zconv(ix,iy,:))).*100) '%']);

n=4;pt(n,:)=[88 107+450];ix=pt(n,1);iy=pt(n,2);k=1;
bb =[budgO.fluxes.tend(ix,iy,k) budgO.fluxes.zconv(ix,iy,k) budgO.fluxes.hconv(ix,iy,k)] %-0.0445e+09 -2.2443e+09  2.1998e+09
bbp=[budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)] %-0.0017 , conserved.
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[bb bbp]);fprintf(fid,'%4i %4i %4i ',[ix iy k]);
fprintf(fid,'%%budgHo tend zconv hconv\n');
k=1:nz;aa(:,n)=squeeze([budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)]);%
subplot(224);plot(aa(:,n),-[1:50],'.-');grid;xlabel('net budgHo');
  title(['[ix,iy]=[' num2str(pt(n,1)) ',' num2str(pt(n,2)) ']; ' ...
          num2str(max(abs(aa(:,n)))/max(abs(budgO.fluxes.zconv(ix,iy,:))).*100) '%']);

figure(1);set(gcf,'paperunit','inches','paperposition',[0 0 9.5 8.5]);
fpr=[dirOut 'Heat_budget1' extfpr '.png'];print(fpr,'-dpng');
%/*}}}*/
a=get_aste_tracer(budgO.fluxes.tend.*repmat(hfC1p,[1 1 nz]),nfx,nfy);%/*{{{*/
b=get_aste_tracer(budgO.fluxes.hconv.*repmat(hfC1p,[1 1 nz]),nfx,nfy);
c=get_aste_tracer(budgO.fluxes.zconv.*repmat(hfC1p,[1 1 nz]),nfx,nfy);
d=get_aste_tracer(swtop.*repmat(hfC1p,[1 1 nz]),nfx,nfy);
a(isnan(a))=0;b(isnan(b))=0;c(isnan(c))=0;d(isnan(d))=0;
msk=get_aste_tracer(hfC1p,nfx,nfy);msk(1:270,451:900)=nan;
klev=[1,2,41];
figure(2);clf;colormap(seismic(21));
for k=1:length(klev);
  k1=klev(k);
  str=['; k=' num2str(k1)];
  subplot(3,4,(k-1)*4+1);mypcolor(a(:,:,k1)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(1:2*nx,1:900,isnan(msk'),[.7 .7 .7]);title(['Heat tend' str]);
  subplot(3,4,(k-1)*4+2);mypcolor(b(:,:,k1)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(1:2*nx,1:900,isnan(msk'),[.7 .7 .7]);title(['Heat hconv']);
  subplot(3,4,(k-1)*4+3);mypcolor(c(:,:,k1)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(1:2*nx,1:900,isnan(msk'),[.7 .7 .7]);title(['Heat zconv']);
  subplot(3,4,(k-1)*4+4);mypcolor(a(:,:,k1)'-b(:,:,k1)'-c(:,:,k1)');cc1=0.05*max(abs(caxis));caxis([-cc1 cc1]);
         mythincolorbar;grid;shadeland(1:2*nx,1:900,isnan(msk'),[.7 .7 .7]);title('Heat tend-hconv-zconv');
end;
figure(2);set(gcf,'paperunit','inches','paperposition',[0 0 14 9]);
fpr=[dirOut 'Heat_budget2' extfpr '.png'];print(fpr,'-dpng');
%/*}}}*/
a=budgO.fluxes.tend.*repmat(hfC1p,[1 1 nz]);%/*{{{*/
b=budgO.fluxes.hconv.*repmat(hfC1p,[1 1 nz]);
c=budgO.fluxes.zconv.*repmat(hfC1p,[1 1 nz]);
a(isnan(a))=0;b(isnan(b))=0;c(isnan(c))=0;
klev=[1,2,41];
ix=76:270;iy=451:450+200;
figure(3);clf;colormap(seismic(21));
for k=1:length(klev);
  k1=klev(k);
  str=['; k=' num2str(k1)];
  subplot(3,4,(k-1)*4+1);mypcolor(ix,iy,a(ix,iy,k1)');cc1=0.04*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         title(['Heat tend' str]);shadeland(ix,iy,isnan(hfC1p(ix,iy)'),[.7 .7 .7]);
  subplot(3,4,(k-1)*4+2);mypcolor(ix,iy,b(ix,iy,k1)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         title(['Heat hconv']);shadeland(ix,iy,isnan(hfC1p(ix,iy)'),[.7 .7 .7]);
  subplot(3,4,(k-1)*4+3);mypcolor(ix,iy,c(ix,iy,k1)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         title(['Heat zconv']);shadeland(ix,iy,isnan(hfC1p(ix,iy)'),[.7 .7 .7]);
  subplot(3,4,(k-1)*4+4);mypcolor(ix,iy,a(ix,iy,k1)'-b(ix,iy,k1)'-c(ix,iy,k1)');cc1=0.05*max(abs(caxis));caxis([-cc1 cc1]);
         mythincolorbar;grid;title('Heat tend-hconv-zconv');shadeland(ix,iy,isnan(hfC1p(ix,iy)'),[.7 .7 .7]);
end;
figure(3);set(gcf,'paperunit','inches','paperposition',[0 0 14 9]);
fpr=[dirOut 'Heat_budget3' extfpr '.png'];print(fpr,'-dpng');  %/*}}}*/
end;	%test3d
a=get_aste_tracer(budgO.tend.*hfC1p ,nfx,nfy);	d=get_aste_tracer(budgI.tend.*hfC1p,nfx,nfy);%/*{{{*/
b=get_aste_tracer(budgO.hconv.*hfC1p,nfx,nfy); f=get_aste_tracer(budgI.hconv.*hfC1p,nfx,nfy);
c=get_aste_tracer(budgO.zconv.*hfC1p,nfx,nfy); g=get_aste_tracer(budgI.zconv.*hfC1p,nfx,nfy);
a(isnan(a))=0;b(isnan(b))=0;c(isnan(c))=0;
d(isnan(d))=0;f(isnan(f))=0;g(isnan(g))=0;
ix=75:540;iy=1:870;
k=1;
  str=['; k=' num2str(k)];
  figure(4);clf;colormap(seismic(21));
  subplot(241);mypcolor(ix,iy,a(ix,iy,k)');cc1=0.04*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ocn Heat tend, all z']);
  subplot(242);mypcolor(ix,iy,b(ix,iy,k)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ocn Heat hconv, all z']);
  subplot(243);mypcolor(ix,iy,c(ix,iy,k)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ocn Heat zconv, all z']);
  subplot(244);mypcolor(ix,iy,a(ix,iy,k)'-b(ix,iy,k)'-c(ix,iy,k)');cc1=0.05*max(abs(caxis));caxis([-cc1 cc1]);
               mythincolorbar;grid;title('ocean tend-hconv-zconv, all z');
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);
  subplot(245);mypcolor(ix,iy,d(ix,iy,k)');cc1=0.04*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ice Heat tend']);
  subplot(246);mypcolor(ix,iy,f(ix,iy,k)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ice Heat hconv']);
  subplot(247);mypcolor(ix,iy,g(ix,iy,k)');cc1=0.04*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ice Heat zconv']);
  subplot(248);mypcolor(ix,iy,d(ix,iy,k)'-f(ix,iy,k)'-g(ix,iy,k)');cc1=0.05*max(abs(caxis));caxis([-cc1 cc1]);
               mythincolorbar;grid;title('ice tend-hconv-zconv');
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);
figure(4);set(gcf,'paperunit','inches','paperposition',[0 0 14 9]);
fpr=[dirOut 'Heat_budget4' extfpr '.png'];print(fpr,'-dpng');
%/*}}}*/
fclose(fid);

%conserve outside sea ice area!!!!
%fix: add SIaaflux for when NLFS=2 and RFWF=1

%%load(['/net/nares/raid11/ecco-shared/ecco-version-4/output/r4/iter9/mat/diags_set_D_' num2str(t2) '.mat'],...
%load([dirIn '../../mat/diags_set_D/diags_set_D_' num2str(t2) '.mat'],...
%      'glo_heat_ice','glo_heat_ocn','glo_heat_tot');
%glo_heat_tot-budgetOI			%[0.288657986402541  -0.124384150538271   0.612843109593086] x 10^-13
%

