%assume already have mygrid
clear all;

global mygrid
mygrid=[];
hhost='pfe';%hhost='sverdrup';
if strcmp(hhost,'sverdrup')
  dirCur='/home/atnguyen/nansen/'; %/*{{{*/
  cd(['~/matlab/gcmfaces/']);gcmfaces_global;cd(dirCur);
  dirGrid='/scratch/atnguyen/aste_270x450x180/GRID_real8/';
  dirRoot='/scratch/atnguyen/aste_270x450x180/';

  %dirIn='/net/barents/raid16/atnguyen/llc90/global/output_ad/r4r1i9/diags/BUDG/';
  %dirIn='/scratch/atnguyen/aste_270x450x180/run_c66h_jra55_rStar_v1q_noicoast_0xx3D_8ts_it0007_pk0000000002/diags/';
  %ext='A';ext1='rStar'%test SIsal0=0 case, can ignore now
  %ext='B';ext1='momhdiv'; %test RFWF=0,NLFS=0
  %ext='';ext1='rStar';
  %ext='';ext1='momhdiv';
  %ext='C';ext1='momhdiv';
  %ext='B';ext1='nocppNLFS';
  ext='_spvollead';ext1='momhdiv';
  dirIn=[dirRoot 'run_c66h_jra55_' ext1 '_sal0sp' ext '_v1q_noicoast_0xx3D_8ts_it0007_pk0000000002/diags/'];
  dirOut=[dirIn '../matlab/'];if(exist(dirOut)==0);mkdir(dirOut);end;
  dirRef=[dirRoot 'run_c66h_jra55_rStar_v1q_noicoast_0xx3D_it0007_pk0000000002/'];

  kBudget=1;    %kBudget=11;
  test3d=1;
  seaice_variable_salinity_flag=0;%1;	%so that we can overwrite myparms.SIsal0

  %load('/net/nares/raid11/ecco-shared/ecco-version-4/output/r4/iter9/mat/diags_grid_parms.mat','myparms');
  load([dirRef 'mat/diags_grid_parms.mat'],'myparms');
  myparms.rStar=4;
  myparms.SPHF=0;       %salt_plume_heat_flux resulting from #define SALT_PLUME_VOLUME
  if(strcmp(ext,'A'));myparms.SIsal0=0;end;
  if(~strcmp(ext1,'rStar'));myparms.useNLFS=0;myparms.rStar=0;end;
  if(strcmp(ext,'B'));myparms.useRFWF=0;end;
  if(strcmp(ext,'C'));myparms.useNLFS=2;myparms.rStar=0;end;
  if(strcmp(ext,'_spvollead'));myparms.useNLFS=4;myparms.rStar=0;myparms.SPHF=1;end;
%/*}}}*/
elseif strcmp(hhost,'pfe')
  dirRoot='/nobackupp2/atnguye4/MITgcm_c65q/mysetups/aste_270x450x180/';
  dirCur='/nobackupp2/atnguye4/matlab/atn_scripts/';
  cd(['/nobackupp2/atnguye4/matlab/gcmfaces/']);;gcmfaces_global;cd(dirCur);
  dirGrid=[dirRoot 'GRID_real8/'];
  dirRef=[dirRoot 'run_c65q_jra55_it0007_pk0000000002_rerun/'];
  %RunStr='run_c65q_1yr_bp_varwei_diag_sal0sp_spvollead_it0008_pk0000000001';extb='';
  RunStr='run_c65q_jra55_it0009_pk0000000002';extb='';%'BUDG/';
  dirIn=[dirRoot RunStr '/diags/' extb];
  dirGrid1=[dirRoot RunStr '/'];
  dirOut=[dirRoot RunStr '/matlab/'];if(exist(dirOut)==0);mkdir(dirOut);end;

  kBudget=1;    %kBudget=11;
  test3d=1;
  seaice_variable_salinity_flag=0;%1;	%so that we can overwrite myparms.SIsal0

  load([dirRef 'mat/diags_grid_parms.mat'],'myparms');
  myparms.useNLFS=0;%4;
  myparms.rStar=0;
  myparms.SPHF=1;
  myparms.SIsal0=4;
end;

use_gcmfaces_hconv=0;
if ~use_gcmfaces_hconv; extfpr='A';else;extfpr='';end;
nx=270;ny=1350;nz=50;nfx=[nx 0 nx 180 450];nfy=[450 0 nx nx nx];
deltaTime=1200;

%get time-steps:
flist=dir([dirIn 'budg2d_snap_set2.*.data']);
idot=find(flist(1).name=='.');idot=idot(1)+1:idot(2)-1;
if1=8;    t1=str2num(flist(if1).name(idot));
if2=if1+1;t2=str2num(flist(if2).name(idot));%t1=2232;t2=4248;
dt=(t2-t1)*deltaTime;
t1str=sprintf('%10.10i',t1);t2str=sprintf('%10.10i',t2);

mygrid=grid_load_raw(dirGrid);
%update hFac:
tmp=rdmds([dirGrid1 'hFacC']);tmp=reshape(tmp,nx*ny,1,nz);mygrid.hFacC=convert2gcmfaces(tmp);
tmp=rdmds([dirGrid1 'hFacW']);tmp=reshape(tmp,nx*ny,1,nz);mygrid.hFacW=convert2gcmfaces(tmp);
tmp=rdmds([dirGrid1 'hFacS']);tmp=reshape(tmp,nx*ny,1,nz);mygrid.hFacS=convert2gcmfaces(tmp);

RAC=convert2gcmfaces(mygrid.RAC);RAC=reshape(RAC,nx,ny);
if test3d;  RAC3=repmat(RAC,[1 1 nz]); end;
%hfC=rdmds(['/net/nares/raid11/ecco-shared/ecco-version-4/output/GRID/' 'hFacC']);
hfC=rdmds([dirGrid1 'hFacC']);hfC=reshape(hfC,nx,ny,nz);
hfC(find(hfC>0))=1;		%this is now identical to mygrid.mskC
DD=reshape(convert2gcmfaces(mygrid.Depth),nx,ny);

%block out obcs:
hfC1=hfC(:,:,1);hfC1(find(hfC1==0))=nan;
RACg=RAC.*hfC1;
hfC1p=get_aste_tracer(hfC1,nfx,nfy);
  hfC1p(:,845)=nan;hfC1p(:,24)=nan;hfC1p(366,261:262)=nan;      %Last one: Gibraltar Strait
  hfC1p=aste_tracer2compact(hfC1p,nfx,nfy);
RACgp=RAC.*hfC1p;
msk=get_aste_tracer(hfC1p,nfx,nfy);msk(1:270,451:900)=nan;

%list_variables={'SALT','AB_gS','SRELAX','SIheff',.../*{{{*/
%'SFLUX','oceSPflx','oceSflux','WSLTMASS',...
%'ADVx_SLT','DFxE_SLT','ADVy_SLT','DFyE_SLT',...
%'ADVxHEFF','ADVxSNOW','DFxEHEFF','DFxESNOW',...
%'ADVyHEFF','ADVySNOW','DFyEHEFF','DFyESNOW'};
%
%if test3d|kBudget>1;
%  list_variables={'oceSPtnd','ADVr_SLT','DFrE_SLT',...
%  'DFrI_SLT','ADVr_SLT','DFrE_SLT','DFrI_SLT'};
%  for vv=1:length(list_variables);
%    v = evalin('caller',list_variables{vv});
%    eval([list_variables{vv} '=v;']);
%  end;
%  clear v;
%end;
%/*}}}*/
%first, verifying:/*{{{*/
if test3d
  flist=dir([dirIn 'budg2d_snap_set2.*.data']);%THETA,SALT
  a=read_slice([dirIn flist(if2).name],nx,ny,2,'real*8');
  flist=dir([dirIn 'budg3d_snap_set2.*.data']);%THETADR,SALTDR
  a3=read_slice([dirIn flist(if2).name],nx,ny,[1:nz]+nz,'real*8');
  tmp=a-sum(a3,3);sum(tmp(:))			%-2.7656e-09

  flist=dir([dirIn 'budg2d_hflux_set2.*.data']);%VVELMASS
  a=read_slice([dirIn flist(if2).name],nx,ny,2,'real*8');
  flist=dir([dirIn 'budg3d_hflux_set2.*.data']);%VVELMASS
  a3=read_slice([dirIn flist(if2).name],nx,ny,[1:nz]+1*nz,'real*8');
  for k=1:nz;a3(:,:,k)=a3(:,:,k).*mygrid.DRF(k);end;
  tmp=a-sum(a3,3);sum(tmp(:))			%2.2115e-12

  flist=dir([dirIn 'budg2d_hflux_set2.*.data']);
  a=read_slice([dirIn flist(if2).name],nx,ny,7,'real*8');%ADVx_SLT
  flist=dir([dirIn 'budg3d_hflux_set2.*.data']);
  a3=read_slice([dirIn flist(if2).name],nx,ny,[1:nz]+6*nz,'real*8');%ADVx_SLT,3d
  tmp=a-sum(a3,3);sum(tmp(:))			%0
end  
%/*}}}*/
%Tendency
%ocean
%THETA SALT/*{{{*/
%'UVELMASS' 'VVELMASS' 'ADVx_TH ' 'ADVy_TH ' 'DFxE_TH ' 'DFyE_TH '
%'ADVx_SLT' 'ADVy_SLT' 'DFxE_SLT' 'DFyE_SLT' 'oceSPtnd' 'AB_gT   ' 'AB_gS   '
%note: if full ocean depth, SALT=sum_k(dSALT(:,:,k)/dt*drF(k).*hFacC(:,:,k)) [psu m/s]
%      if test3d: SALT(k) = SALTDR(k) = dSALT(:,:,k)/dt*drF(k).*hFacC(:,:,k) <-- new diag atn put in
%/*}}}*/
%/*{{{*/
%flist=dir([dirIn 'budg3d_snap_set2.*.data']);%THETADR,SALTDR
%a=read_slice([dirIn flist(if1).name],nx,ny,[1:nz]+1*nz,'real*8');
%b=read_slice([dirIn flist(if2).name],nx,ny,[1:nz]+1*nz,'real*8');
%SALT=(b-a)/dt;						%3d , m.psu/s/*}}}*/
AB_gS=0;%read_slice([dirIn 'budg3d_kpptend_set1.' t2str '.data'],nx,ny,[1:nz]+6*nz,'real*8');

if test3d; fileName='budg3d_snap_set2';varName={'SALTDR'};n3=nz;
else;      fileName='budg2d_snap_set2';varName={'SALT'};  n3=1;    end;
fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,[t1,t2]);
for ifld=1:length(varName);
  eval([strtrim(varName{ifld}) '=(fldOut.' varName{ifld} '(:,:,:,2)-fldOut.' varName{ifld} '(:,:,:,1))./dt;']);
end;
clear fldOut;   %clear memory
if exist('SALTDR','var');  SALT = SALTDR; clear SALTDR; end;

tmptend=myparms.rhoconst.*(SALT-AB_gS).*mk3D_mod(RAC,SALT);%kg/m3 g/kg.m/s m2 = [g/s]
if test3d; budgO.fluxes.tend=tmptend; end;		%3d g/s
budgO.tend=nansum(tmptend,3);				%2d g/s

%ice
% 'ETAN    ' 'SIheff  ' 'SIhsnow ' 'SIarea  ' 'sIceLoad' 'PHIBOT  '
fileName='budg2d_snap_set1';varName={'ETAN','SIheff'};n3=1;
fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,[t1,t2]);
for ifld=1:length(varName);
  eval([strtrim(varName{ifld}) '=(fldOut.' varName{ifld} '(:,:,:,2)-fldOut.' varName{ifld} '(:,:,:,1))./dt;']);
end;
clear fldOut;   %clear memory
%/*{{{*/
%flist=dir([dirIn 'budg2d_snap_set1.*.data']);
%a=readbin([dirIn flist(if1).name],[nx ny 6],1,'real*8');
%b=readbin([dirIn flist(if2).name],[nx ny 6],1,'real*8');
%c=(b-a)./dt;
%SIheff=c(:,:,2); %/*}}}*/
if(seaice_variable_salinity_flag==0);%constant salinity case
  budgI.tend=myparms.SIsal0*SIheff*myparms.rhoi.*RAC;%g/kg m/s kg/m3 m2 = g/s
else;
%this is not correct, will need to figure out what to do with SEAICE_saltFrac and SSS
%but for now, because by accident I've (un)set SEAICE_saltFrac so it takes default = 0.
  budgI.tend=0.*SIheff*myparms.rhoi.*RAC;%g/kg m/s kg/m3 m2 = g/s
end;

%ocean and ice
budgOI3.tend=budgO.tend+budgI.tend;			%2d, %Watts

%vertical convergence:
%'budg2d_zflux_set1.' t2str '.meta'/* %{{{*/
% 'oceFWflx' 'SIatmFW ' 'TFLUX   ' 'SItflux ' 'SFLUX   ' 'oceQsw  ' 'oceSPflx'
%budg2d_zflux_set2.' t2str '.meta
% 'SRELAX  ' 'TRELAX  ' 'WTHMASS ' 'WSLTMASS' 'oceSflux' 'oceQnet '
% 'SIatmQnt' 'SIaaflux' 'SIsnPrcp' 'SIacSubl'

%2d fluxes:

%total salt flux (match salt-content variations), >0 increases salt, g/m^2/s
%SFLUX = surfaceForcingS(i,j,bi,bj)*rhoConst; (rUnit2mass=rhoConst), surfaceForcingS = surForcS
% if nonlinFreeSurf & useRealFreshWaterFlux: SFLUX=SFLUX+PmEpR.*salt(i,j,1,bi,bj)
%SFLUX   =read_slice([dirIn 'budg2d_zflux_set1.' t2str '.data'],nx,ny,5,'real*8');%2d, g/m2/s
%net surface Salt flux rejected into the ocean during freezing, (+=down), g/m^2/s
%oceSPflx = saltPlumeFlux
%oceSPflx=read_slice([dirIn 'budg2d_zflux_set1.' t2str '.data'],nx,ny,7,'real*8');%2d, g/m2/s
%oceSPflx=0;	%looks like oceSPflx is identically -SFLUX, this makes vertical FW flux zero, wrong!
%SRELAX  =read_slice([dirIn 'budg2d_zflux_set2.' t2str '.data'],nx,ny,1,'real*8');%2d, all zeros
%salt tendency due to salt plume flux >0 increases salinity, g/m^2/s
%oceSPtnd=read_slice([dirIn 'budg3d_hflux_set2.' t2str '.data'],nx,ny,[1:nz]+10*nz,'real*8');%2d,g/m2/s
%/*}}}*/
fileName='budg2d_zflux_set1';varName={'SFLUX','oceSPflx'}; fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,1,t2);
for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut;

fileName='budg2d_zflux_set2';varName={'SRELAX','oceSflux','WSLTMASS'};
fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,1,t2);
for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut;

if test3d
  fileName='budg3d_kpptend_set1';varName={'oceSPtnd'};n3=nz;
  %oceSPtnd=read_slice([dirIn 'budg3d_kpptend_set1.' t2str '.data'],nx,ny,[1:nz]+4*nz,'real*8');%3d,g/m2/s
else
  fileName='budg2d_hflux_set2';varName={'oceSPtnd'};n3=1;
  %oceSPtnd=read_slice([dirIn 'budg2d_hflux_set2.' t2str '.data'],nx,ny,[1:1]+10*1,'real*8');%2d,g/m2/s
end
fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,t2);
for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut;
%/*{{{*/
%#ifndef SALT_PLUME_VOLUME
%          plumefrac(i,j) = ( plumekb2D(i,j)-plumefrac(i,j) )
%     &                     *maskC(i,j,k,bi,bj)
%          plumetend(I,J) = saltPlumeFlux(i,j,bi,bj)*plumefrac(I,J)
%#else /* SALT_PLUME_VOLUME */
%          plumetend(I,J) = SPforcingS(i,j,k,bi,bj)
%#endif /* SALT_PLUME_VOLUME */
%/*}}}*/
%working approach for real fresh water (?) and virtual salt flux
if ~myparms.useRFWF|~myparms.useNLFS;	%enters if useRFWF=0 or useNLFS=0
% due to mistake of (un)set SEAICE_saltFrac, this shoudl all be zeros, YES, confirmed
% net surface Salt flux into the ocean (+=down), >0 increases salinity, g/m^2/s
  %oceSflux=read_slice([dirIn 'budg2d_zflux_set2.' t2str '.data'],nx,ny,5,'real*8');%2d, g/m2/s
  %oceSflux: already read above
else;
  oceSflux=0.*oceSflux;
end;

%Vertical Advective and Diffusive (ex and im) Flux of Salinity, psu.m^3/s
if test3d
  fileName='budg3d_zflux_set2';varName={'ADVr_SLT','DFrE_SLT','DFrI_SLT'};n3=nz;%skip reading WSLTMASS, only need 1st
  fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,[t2]);
  for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut
  %ADVr_SLT=read_slice([dirIn 'budg3d_zflux_set2.' t2str '.data'],nx,ny,[1:nz]+6*nz,'real*8');%3d, psu.m3/s/*{{{*/
  %DFrE_SLT=read_slice([dirIn 'budg3d_zflux_set2.' t2str '.data'],nx,ny,[1:nz]+7*nz,'real*8');%3d, psu.m3/s
  %DFrI_SLT=read_slice([dirIn 'budg3d_zflux_set2.' t2str '.data'],nx,ny,[1:nz]+8*nz,'real*8');%3d, psu.m3/s
%/*}}}*/
%KPP non-local Flux of Salt
  %KPPg_SLT=read_slice([dirIn 'budg3d_kpptend_set1.'   t2str '.data'],nx,ny,[1:nz]+3*nz,'real*8');%3d, psu.m3/s
  fileName='budg3d_kpptend_set1';varName={'KPPg_SLT'};n3=nz;fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,[t2]);
  for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut
else;
  %WSLTMASS2d=read_slice([dirIn 'budg2d_zflux_set2.' t2str '.data'],nx,ny,4,'real*8');	%2d
  for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut
  %fileName='budg2d_kpptend_set1';varName={'KPPg_SLT'};n3=1;fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,[t2]);
  %for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut
  KPPg_SLT=zeros(nx,ny,nz);	%not yet output for 2d,should output appropriate diag
end;

%in linear surface we omit :
if ~myparms.useNLFS;
%Vertical Mass-Weight Transp of Salinity, psu.m/s/*{{{*/
%Note: there is NO depth integr for WSLTMASS, and it's read in as 2d field, so must have been layer 1
%      If it's layer 11, Gael added a Depth layer = 11 field, so this is a single layer quantity
  %WSLTMASS2d=read_slice([dirIn 'budg2d_zflux_set2.' t2str '.data'],nx,ny,4,'real*8');	%2d
  %WSLTMASS=read_slice([dirIn 'budg3d_zflux_set2.' t2str '.data'],nx,ny,[1:nz]+2*nz,'real*8');%3d, psu.m/s
  %tmp=WSLTMASS2d-WSLTMASS(:,:,1);
  %fprintf('sum(WSLTMASS2d-WSLTMASS(:,:,1)): %10.8e\n',sum(tmp(:)));
  %already read in above/*}}}*/
else;
  WSLTMASS=0.*WSLTMASS;
end;
%/*{{{*/
%plot to show issues perhaps related to exchange?
figure(5);clf;mypcolor(1:270,451:720,oceSPtnd(:,451:720,1)');colorbar;title('oceSPtnd.f3,k=1');grid;
              shadeland(1:270,451:720,isnan(hfC1(:,451:720)'),[.7 .7 .7]);set(gca,'dataaspectratio',[1 1 1])
fpr=[dirOut 'oceSPtnd_f3_klev1A.png'];print(fpr,'-dpng');

%vertical divergence (air-sea fluxes or vertical adv/dif)
%these two quantities are identical, so budgO.zconv is 10^-17 = zeros, this cant' be right?
figure(6);clf;mypcolor(1:270,451:720,oceSPflx(:,451:720,1)'+SFLUX(:,451:720)');colorbar;
		title('oceSPflx.f3+SFLUX.f3');grid;
a=oceSPflx+SFLUX;fprintf('sum(oceSPflx+SFLUX): %10.8e\n',sum(a(:)))		%4.643508593082820e-15
%check ecco4r3 to see if this is also true./*}}}*/

budgO.zconv=SFLUX+oceSPflx;				%2d
budgI.zconv=-budgO.zconv+SRELAX;			%2d
%in linear surface we omit :
if ~myparms.useNLFS; budgO.zconv=budgO.zconv-myparms.rhoconst*WSLTMASS(:,:,kBudget); end;	%2d
%in virtual salt flux we omit :
if ~myparms.useRFWF|~myparms.useNLFS; budgI.zconv=-oceSflux; end;%problematic: what if useRFWF and ~useNLFS?
%atn: attempt1: remove useNLFS filter: no, this is wrong
%if ~myparms.useRFWF; budgI.zconv=-oceSflux; end;
%
budgO.zdia=budgO.zconv;				%2d
%for deep ocean layer :
if kBudget>1;
  budgO.zconv=-(ADVr_SLT+DFrE_SLT+DFrI_SLT)./RAC*myparms.rhoconst;%2d (only for layer 11)
  budgO.zconv=budgO.zconv+oceSPtnd(:,:,kBudget);	%2d, why plus here and minus in test3d block?
  budgO.zdia=-(DFrE_SLT+DFrI_SLT)./RAC*myparms.rhoconst;
  budgO.zdia=budgO.zdia+oceSPtnd(:,:,kBudget);%.*msk;
end;
%
if test3d;
  nr=length(mygrid.RC);
  trWtop=-(ADVr_SLT+DFrE_SLT+DFrI_SLT+KPPg_SLT)*myparms.rhoconst;	%3d, g/s
  tmp1=mk3D_mod(oceSPflx,oceSPtnd)-cumsum(oceSPtnd,3);
  tmp1=tmp1.*RAC3;
  trWtop(:,:,2:nr)=trWtop(:,:,2:nr)+tmp1(:,:,1:nr-1);
% if(~myparms.useNLFS);
%  trWtop(:,:,1)=(oceSPflx-myparms.rhoconst.*WSLTMASS(:,:,1)).*RAC;	%no minus, toplayer = surf flx or layer11
% else;
  trWtop(:,:,1)=budgO.zconv.*RAC;			%no minus, toplayer = surf flx or layer11
% end;
  trWbot(:,:,1:nr-1)=trWtop(:,:,2:length(mygrid.RC));	%3d
  trWbot(:,:,length(mygrid.RC))=0;			%need to take care of geothFlux somewhere
  %
  budgO.fluxes.trWtop=trWtop;%g/s			%3d
  budgO.fluxes.trWbot=trWbot;%g/s			%3d
else;
  budgO.fluxes.trWtop=-RAC.*budgO.zconv;		%note [-] sign , g/s
  budgO.fluxes.trWbot=RAC.*0;%				%2d, g/s
  budgO.fluxes.diaWtop=-RAC.*budgO.zdia;		%2d
  budgO.fluxes.diaWbot=RAC.*0;%g/s			%2d
end;
budgI.fluxes.trWtop=-RAC.*0;				%2d
budgI.fluxes.trWbot=budgO.fluxes.trWtop(:,:,1);	%2d, g/s
%
budgO.fluxes.zconv=budgO.fluxes.trWtop-budgO.fluxes.trWbot;%3d, g/s
budgO.zconv=mk3D_mod(RAC,budgO.zconv).*budgO.zconv;	%g/s,%2d, why mk3D?
budgI.zconv=RAC.*budgI.zconv;%g/s			%2d
budgOI3.zconv=budgO.zconv+budgI.zconv;		%2d

%tmp=zeros(nx,ny,nz);/*{{{*/
%for k=nz:-1:1;
%  if(k==nz);
%    tmp(:,:,k)=0;
%  elseif(k>1&k<nz);
%    tmp(:,:,k)=-myparms.rhoconst.*((ADVr_SLT(:,:,k)  +DFrE_SLT(:,:,k)  +DFrI_SLT(:,:,k))... %minus
%                                  -(ADVr_SLT(:,:,k+1)+DFrE_SLT(:,:,k+1)+DFrI_SLT(:,:,k+1)))...
%               		         + oceSPtnd(:,:,k);
%  elseif(k==1);
%    tmp(:,:,k)=SFLUX.*RAC+myparms.rhoconst.*(ADVr_SLT(:,:,k)+DFrI_SLT(:,:,k)...
%                                                      +DFrE_SLT(:,:,k));%lower surface minus surf
%  end;
%end;
%budgO.fluxes.zconv1=tmp;clear tmp;
%/*}}}*/
%horizontal convergence:
% budg2d_hflux_set1.0000000336.meta/*{{{*/
% 'ADVxHEFF' 'ADVyHEFF' 'DFxEHEFF' 'DFyEHEFF' 'ADVxSNOW' 'ADVySNOW' 'DFxESNOW' 'DFyESNOW'
% budg2d_hflux_set2.0000000336.meta
% 'UVELMASS' 'VVELMASS' 'ADVx_TH ' 'ADVy_TH ' 'DFxE_TH ' 'DFyE_TH '
% 'ADVx_SLT' 'ADVy_SLT' 'DFxE_SLT' 'DFyE_SLT' 'oceSPtnd' 'AB_gT   ' 'AB_gS   '

%ADVx_SLT=read_slice([dirIn 'budg3d_hflux_set2.' t2str '.data'],nx,ny,[1:nz]+6*nz,'real*8');
%ADVy_SLT=read_slice([dirIn 'budg3d_hflux_set2.' t2str '.data'],nx,ny,[1:nz]+7*nz,'real*8');
%DFxE_SLT=read_slice([dirIn 'budg3d_hflux_set2.' t2str '.data'],nx,ny,[1:nz]+8*nz,'real*8');
%DFyE_SLT=read_slice([dirIn 'budg3d_hflux_set2.' t2str '.data'],nx,ny,[1:nz]+9*nz,'real*8');
%/*}}}*/
if test3d; fileName='budg3d_hflux_set2';varName={'ADVx_SLT','ADVy_SLT','DFxE_SLT','DFyE_SLT'};n3=nz;
else;      fileName='budg2d_hflux_set2';varName={'ADVx_SLT','ADVy_SLT','DFxE_SLT','DFyE_SLT'};n3=1; end;
fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,[t2]);
for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut

%a=readbin([dirIn 'budg2d_hflux_set1.' t2str '.data'],[nx ny 8],1,'real*8');/*{{{*/
%DFxEHEFF=a(:,:,3);	ADVxHEFF=a(:,:,1);
%DFyEHEFF=a(:,:,4);	ADVyHEFF=a(:,:,2); %/*}}}*/
fileName='budg2d_hflux_set1';varName={'DFxEHEFF','DFyEHEFF','ADVxHEFF','ADVyHEFF'};n3=1;
fldOut=rdmds_list_fld(fileName,varName,dirIn,nx,ny,n3,[t2]);
for ifld=1:length(varName);eval([strtrim(varName{ifld}) '=fldOut.' varName{ifld} '(:,:,:,1);']);end;clear fldOut

tmpUo=myparms.rhoconst*(ADVx_SLT+DFxE_SLT);		%2d or 3d, kg/m3.g/kg.m3/s = [g/s]
tmpVo=myparms.rhoconst*(ADVy_SLT+DFyE_SLT);		%2d or 3d, kg/m3.g/kg.m3/s = [g/s]

if test3d
  if use_gcmfaces_hconv;
    tmpUo=convert2gcmfaces(tmpUo);tmpVo=convert2gcmfaces(tmpVo);%/*{{{*/
    tmp=0.*tmpUo;
    for k=1:nz;
%%tmpUo(1:end-1,:)+tmpUo(2:end,:)+tmpVo(:,1:end-1)+tmpVo(:,2:end),[g/s]
    temp=calc_UV_conv(tmpUo(:,:,k),tmpVo(:,:,k));
    tmp(:,:,k)=temp;
    fprintf('%i ',k);
    end;
    fprintf('\n');
    budgO.fluxes.hconv=reshape(convert2gcmfaces(tmp),nx,ny,nz);clear tmp %/*}}}*/
  else;
    tmp=calc_UV_conv_mod(tmpUo,tmpVo,nfx,nfy);
    budgO.fluxes.hconv=cat(2,tmp{1},tmp{3},reshape(tmp{4},nfy(4),nfx(4),nz),reshape(tmp{5},nfy(5),nfx(5),nz));
  end;
end;

if use_gcmfaces_hconv;
  tmpUo=convert2gcmfaces(nansum(tmpUo,3));tmpVo=convert2gcmfaces(nansum(tmpVo,3));
  tmp1=calc_UV_conv(nansum(tmpUo,3),nansum(tmpVo,3));			%2d , g/s
  budgO.hconv=reshape(convert2gcmfaces(tmp1),nx,ny);clear tmp		%2d , g/s
else;
  tmp=calc_UV_conv_mod(nansum(tmpUo,3),nansum(tmpVo,3),nfx,nfy);
  budgO.hconv=cat(2,tmp{1},tmp{3},reshape(tmp{4},nfy(4),nfx(4),1),reshape(tmp{5},nfy(5),nfx(5),1));
end;

%               J/kg        kg/m^3 m.m^2/s         kg/m^3   m.m^2/s = [W]
if(seaice_variable_salinity_flag==0);
  SIsal0=myparms.SIsal0;
else;
  SIsal0=0;	%not true, would need to calc from SEAICEsaltFrac*SSS, but by accident my prev runs SIsal0=0;
end;
tmpUi=SIsal0*myparms.rhoi*(DFxEHEFF+ADVxHEFF);
tmpVi=SIsal0*myparms.rhoi*(DFyEHEFF+ADVyHEFF);
                   
if use_gcmfaces_hconv
  tmpUi=convert2gcmfaces(tmpUi);tmpVi=convert2gcmfaces(tmpVi);
  temp=calc_UV_conv(tmpUi,tmpVi); %no dh needed here, already built in DFx etc %[g/s]
  budgI.hconv=reshape(convert2gcmfaces(temp),nx,ny);
else
  tmp=calc_UV_conv_mod(tmpUi,tmpVi,nfx,nfy);
  budgI.hconv=cat(2,tmp{1},tmp{3},reshape(tmp{4},nfy(4),nfx(4),1),reshape(tmp{5},nfy(5),nfx(5),1));
end;

budgOI3.hconv=budgO.hconv+budgI.hconv;                                  %[g/s]

%%pick a point to test:%/*{{{*/
%ix=211;iy=546;k=2;
%a=[ADVx_TH(ix+1,iy,k)-ADVx_TH(ix,iy,k) ADVy_TH(ix,iy+1,k)-ADVy_TH(ix,iy,k) ADVr_TH(ix,iy,k-1)-ADVr_TH(ix,iy,k)]...
%   .*myparms.rcp./1e8	%[5.3362  -62.8944   28.8366]
%b=[DFxE_TH(ix+1,iy,k)-DFxE_TH(ix,iy,k) DFyE_TH(ix,iy+1,k)-DFyE_TH(ix,iy,k) ...
%   DFrE_TH(ix,iy,k-1)-DFrE_TH(ix,iy,k) DFrI_TH(ix,iy,k-1)-DFrI_TH(ix,iy,k)].*myparms.rcp./1e8 
%  %[-0.0003   -0.0161   -0.4428   -3.8189]
%c=[WTHMASS(ix,iy,k-1)-WTHMASS(ix,iy,k)].*RAC(ix,iy).*myparms.rcp./1e8	%this term shouldn't be needed..., 27.5731
%budgO1.tend(ix,iy,k)./1e8	%-2.9638
%budgO1.tend(ix,iy,k)./1e8-(sum(a)+sum(b)+c)	%2.4627 , not balanced 

%/*}}}*/
%now sum up globally:
fid=fopen([dirOut 'BudgetSalt.txt'],'w');
if test3d
%/*{{{*/
a=budgOI3.tend.*hfC1;
b=budgOI3.hconv.*hfC1;
c=budgOI3.zconv.*hfC1;				% wrong use SIsal0	Correct SIsal0:	new run
budgetOI(1,1)=nansum(a(:))./nansum(RACg(:));	% 0.303e-04		 0.303e-04	0.138e-04
budgetOI(2,1)=nansum(b(:))./nansum(RACg(:));	% 0.000e-04 <- wrong     0.000e-04	0
budgetOI(3,1)=nansum(c(:))./nansum(RACg(:)),	% 0 <-- wrong?, SFLUX=-oceSPflx	 0	0
budgetOI(1)-budgetOI(2)-budgetOI(3)		% 3.029e-05		 3.029e-05     -1.383e-05
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetOI',budgetOI(1)-budgetOI(2)-budgetOI(3)]);
fprintf(fid,'%%budgetOI tend hconv zconv tend-hconv-zconv\n');

a=budgOI3.tend.*hfC1p;
b=budgOI3.hconv.*hfC1p;
c=budgOI3.zconv.*hfC1p;
budgetOIp(1,1)=nansum(a(:))./nansum(RACgp(:));	% 0.294e-04		 0.294e-04	-0.1486e-04
budgetOIp(2,1)=nansum(b(:))./nansum(RACgp(:));	%-0.149e-04		-0.149e-04	-0.1480e-04
budgetOIp(3,1)=nansum(c(:))./nansum(RACgp(:)),	% 0				 0	 0
budgetOIp(1)-budgetOIp(2)-budgetOIp(3)		% 4.430e-05		 4.430e-05	-5.426e-08, 0.4%
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetOIp',budgetOIp(1)-budgetOIp(2)-budgetOIp(3)]);
fprintf(fid,'%%budgetOIp tend hconv zconv tend-hconv-zconv\n');

a=budgI.tend.*hfC1;
b=budgI.hconv.*hfC1;
c=budgI.zconv.*hfC1;
budgetI(1,1)=nansum(a(:))./nansum(RACg(:));	%-3.832		 	 0.441e-04	 0.468e-04
budgetI(2,1)=nansum(b(:))./nansum(RACg(:));	% 0.000 <-- wrong	 0.000e-04	 0
budgetI(3,1)=nansum(c(:))./nansum(RACg(:)),	%-3.953			-0.000e-04	 0.469e-04
budgetI(1)-budgetI(2)-budgetI(3)		% 0.121		 	 4.407e-05	-5.404e-08, 0.1%
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetI',budgetI(1)-budgetI(2)-budgetI(3)]);
fprintf(fid,'%%budgetI tend hconv zconv tend-hconv-zconv\n');

a=budgI.tend.*hfC1p;
b=budgI.hconv.*hfC1p;
c=budgI.zconv.*hfC1p;
budgetIp(1,1)=nansum(a(:))./nansum(RACgp(:));	%-3.848		 	 0.442e-04	 0.470e-04
budgetIp(2,1)=nansum(b(:))./nansum(RACgp(:));	% 0.005 <-- from?	-0.001e-04	 0
budgetIp(3,1)=nansum(c(:))./nansum(RACgp(:))	%-3.969			-0.000e-04	 0.471e-04
budgetIp(1)-budgetIp(2)-budgetIp(3)		% 0.116 <-- bad, 3%	 4.430e-05 ??	-5.426e-08, 0.1%
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetIp',budgetIp(1)-budgetIp(2)-budgetIp(3)]);
fprintf(fid,'%%budgetIp tend hconv zconv tend-hconv-zconv\n');

a=budgO.tend.*hfC1;
b=budgO.hconv.*hfC1;
c=budgO.zconv.*hfC1;
budgetO(1,1) =nansum(a(:))./nansum(RACg(:));	%-1.633e+02				-0.607e-04
budgetO(2,1)=nansum(b(:))./nansum(RACg(:));	%-0.000e+02 <-- wrong			 0
budgetO(3,1)=nansum(c(:))./nansum(RACg(:))	%-1.671e+02				-0.469e-04
budgetO(1)-budgetO(2)-budgetO(3)		% 3.768	%2.3%				-1.377e-05
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetO',budgetO(1)-budgetO(2)-budgetO(3)]);
fprintf(fid,'%%budgetO tend hconv zconv tend-hconv-zconv\n');

a=budgO.tend.*hfC1p;
b=budgO.hconv.*hfC1p;
c=budgO.zconv.*hfC1p;
budgetOp(1,1) =nansum(a(:))./nansum(RACgp(:));	%-1.640e+02				-0.619e-04
budgetOp(2,1)=nansum(b(:))./nansum(RACgp(:));	% 0.035e+02				-0.148e-04
budgetOp(3,1)=nansum(c(:))./nansum(RACgp(:))	%-1.674e+02				-0.471e-04
budgetOp(1)-budgetOp(2)-budgetOp(3)		%-0.045 <-- still bad, 0.03%		 2.002e-14 :-)
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[budgetOp',budgetOp(1)-budgetOp(2)-budgetOp(3)]);
fprintf(fid,'%%budgetOp tend hconv zconv tend-hconv-zconv\n');
%/*}}}*/
figure(1);clf;%/*{{{*/
aa=zeros(50,4);
n=1;pt(n,:)=[17 226];ix=pt(n,1);iy=pt(n,2);k=5;
bb =[budgO.fluxes.tend(ix,iy,k) budgO.fluxes.zconv(ix,iy,k) budgO.fluxes.hconv(ix,iy,k)] %0.0019e+08 3.294e+08 -3.292e+08
bbp=[budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)] % 0.0068!!!! (conserved!!)
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[bb bbp]);fprintf(fid,'%4i %4i %4i ',[ix iy k]);
fprintf(fid,'%%budgSo tend zconv hconv\n');
k=1:nz;aa(:,n)=squeeze([budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)]);%
subplot(2,2,n);plot(aa(:,n),-[1:50],'.-');grid;xlabel('net budgSo');
  title(['[ix,iy]=[' num2str(pt(n,1)) ',' num2str(pt(n,2)) ']; ' ...
          num2str(max(abs(aa(:,n)))/max(abs(budgO.fluxes.zconv(ix,iy,:))).*100) '%']);

n=2;pt(n,:)=[63 92+450];ix=pt(n,1);iy=pt(n,2);k=1;%a very bad point (for Heat) in the ARctic
bb =[budgO.fluxes.tend(ix,iy,k) budgO.fluxes.zconv(ix,iy,k) budgO.fluxes.hconv(ix,iy,k)] %0.0258e+07 3.798e+07 -3.772e+07
bbp=[budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)] % -3.994e-06 , conserved!
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[bb bbp]);fprintf(fid,'%4i %4i %4i ',[ix iy k]);
fprintf(fid,'%%budgSo tend zconv hconv\n');
k=1:nz;aa(:,n)=squeeze([budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)]);%
subplot(2,2,n);plot(aa(:,n),-[1:50],'.-');grid;xlabel('net budgSo');
  title(['[ix,iy]=[' num2str(pt(n,1)) ',' num2str(pt(n,2)) ']; ' ...
          num2str(max(abs(aa(:,n)))/max(abs(budgO.fluxes.zconv(ix,iy,:))).*100) '%']);

n=3;pt(n,:)=[117 37+450];ix=pt(n,1);iy=pt(n,2);k=1;
bb =[budgO.fluxes.tend(ix,iy,k) budgO.fluxes.zconv(ix,iy,k) budgO.fluxes.hconv(ix,iy,k)] %0.125e+07 1.627e+07  -1.502+07
bbp=[budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)] %3.079e-06, conserved!
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[bb bbp]);fprintf(fid,'%4i %4i %4i ',[ix iy k]);
fprintf(fid,'%%budgSo tend zconv hconv\n');
k=1:nz;aa(:,n)=squeeze([budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)]);%
subplot(2,2,n);plot(aa(:,n),-[1:50],'.-');grid;xlabel('net budgSo');
  title(['[ix,iy]=[' num2str(pt(n,1)) ',' num2str(pt(n,2)) ']; ' ...
          num2str(max(abs(aa(:,n)))/max(abs(budgO.fluxes.zconv(ix,iy,:))).*100) '%']);

n=4;pt(n,:)=[88 107+450];ix=pt(n,1);iy=pt(n,2);k=1;
bb =[budgO.fluxes.tend(ix,iy,k) budgO.fluxes.zconv(ix,iy,k) budgO.fluxes.hconv(ix,iy,k)] %-0.0130e+07 0.993e+07  -1.006e+07
bbp=[budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)] % 6.419e-06 , conserved.
fprintf(fid,'%10.8e %10.8e %10.8e %10.8e ',[bb bbp]);fprintf(fid,'%4i %4i %4i ',[ix iy k]);
fprintf(fid,'%%budgSo tend zconv hconv\n');
k=1:nz;aa(:,n)=squeeze([budgO.fluxes.tend(ix,iy,k)-budgO.fluxes.zconv(ix,iy,k)-budgO.fluxes.hconv(ix,iy,k)]);%
subplot(224);plot(aa(:,n),-[1:50],'.-');grid;xlabel('net budgSo');
  title(['[ix,iy]=[' num2str(pt(n,1)) ',' num2str(pt(n,2)) ']; ' ...
          num2str(max(abs(aa(:,n)))/max(abs(budgO.fluxes.zconv(ix,iy,:))).*100) '%']);

figure(1);set(gcf,'paperunit','inches','paperposition',[0 0 9.5 8.5]);
fpr=[dirOut 'Salt_budget1' extfpr '.png'];print(fpr,'-dpng');
%/*}}}*/
a=get_aste_tracer(budgO.fluxes.tend.*repmat(hfC1p,[1 1 nz]),nfx,nfy);%/*{{{*/
b=get_aste_tracer(budgO.fluxes.hconv.*repmat(hfC1p,[1 1 nz]),nfx,nfy);
c=get_aste_tracer(budgO.fluxes.zconv.*repmat(hfC1p,[1 1 nz]),nfx,nfy);
a(isnan(a))=0;b(isnan(b))=0;c(isnan(c))=0;
msk=get_aste_tracer(hfC1p,nfx,nfy);msk(1:270,451:900)=nan;
klev=[1,2,41];
figure(2);clf;colormap(seismic(21));
for k=1:length(klev);
  k1=klev(k);
  str=['; k=' num2str(k1)];
  subplot(3,4,(k-1)*4+1);mypcolor(a(:,:,k1)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(1:2*nx,1:900,isnan(msk'),[.7 .7 .7]);title(['Salt tend' str]);
  subplot(3,4,(k-1)*4+2);mypcolor(b(:,:,k1)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(1:2*nx,1:900,isnan(msk'),[.7 .7 .7]);title(['Salt hconv']);
  subplot(3,4,(k-1)*4+3);mypcolor(c(:,:,k1)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(1:2*nx,1:900,isnan(msk'),[.7 .7 .7]);title(['Salt zconv']);
  subplot(3,4,(k-1)*4+4);mypcolor(a(:,:,k1)'-b(:,:,k1)'-c(:,:,k1)');cc1=0.05*max(abs(caxis));caxis([-cc1 cc1]);
         mythincolorbar;grid;shadeland(1:2*nx,1:900,isnan(msk'),[.7 .7 .7]);title('Salt tend-hconv-zconv');
end;
figure(2);set(gcf,'paperunit','inches','paperposition',[0 0 14 9]);
fpr=[dirOut 'Salt_budget2' extfpr '.png'];print(fpr,'-dpng');
%/*}}}*/
a=budgO.fluxes.tend.*repmat(hfC1p,[1 1 nz]);%/*{{{*/
b=budgO.fluxes.hconv.*repmat(hfC1p,[1 1 nz]);
c=budgO.fluxes.zconv.*repmat(hfC1p,[1 1 nz]);
a(isnan(a))=0;b(isnan(b))=0;c(isnan(c))=0;
klev=[1,2,41];
ix=76:270;iy=451:450+200;
figure(3);clf;colormap(seismic(21));
for k=1:length(klev);
  k1=klev(k);
  str=['; k=' num2str(k1)];
  subplot(3,4,(k-1)*4+1);mypcolor(ix,iy,a(ix,iy,k1)');cc1=0.04*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         title(['Salt tend' str]);shadeland(ix,iy,isnan(hfC1p(ix,iy)'),[.7 .7 .7]);
  subplot(3,4,(k-1)*4+2);mypcolor(ix,iy,b(ix,iy,k1)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         title(['Salt hconv']);shadeland(ix,iy,isnan(hfC1p(ix,iy)'),[.7 .7 .7]);
  subplot(3,4,(k-1)*4+3);mypcolor(ix,iy,c(ix,iy,k1)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         title(['Salt zconv']);shadeland(ix,iy,isnan(hfC1p(ix,iy)'),[.7 .7 .7]);
  subplot(3,4,(k-1)*4+4);mypcolor(ix,iy,a(ix,iy,k1)'-b(ix,iy,k1)'-c(ix,iy,k1)');cc1=0.05*max(abs(caxis));caxis([-cc1 cc1]);
         mythincolorbar;grid;title('Salt tend-hconv-zconv');shadeland(ix,iy,isnan(hfC1p(ix,iy)'),[.7 .7 .7]);
end;
figure(3);set(gcf,'paperunit','inches','paperposition',[0 0 14 9]);
fpr=[dirOut 'Salt_budget3' extfpr '.png'];print(fpr,'-dpng');
%/*}}}*/
end;	%test3d
a=get_aste_tracer(budgO.tend.*hfC1p ,nfx,nfy);	d=get_aste_tracer(budgI.tend.*hfC1p,nfx,nfy);%/*{{{*/
b=get_aste_tracer(budgO.hconv.*hfC1p,nfx,nfy); f=get_aste_tracer(budgI.hconv.*hfC1p,nfx,nfy);
c=get_aste_tracer(budgO.zconv.*hfC1p,nfx,nfy); g=get_aste_tracer(budgI.zconv.*hfC1p,nfx,nfy);
a(isnan(a))=0;b(isnan(b))=0;c(isnan(c))=0;
d(isnan(d))=0;f(isnan(f))=0;g(isnan(g))=0;
ix=75:540;iy=1:870;
k=1;
  str=['; k=' num2str(k)];
  figure(4);clf;colormap(seismic(21));
  subplot(241);mypcolor(ix,iy,a(ix,iy,k)');cc1=0.04*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ocn Salt tend, all z']);
  subplot(242);mypcolor(ix,iy,b(ix,iy,k)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ocn Salt hconv, all z']);
  subplot(243);mypcolor(ix,iy,c(ix,iy,k)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ocn Salt zconv, all z']);
  subplot(244);mypcolor(ix,iy,a(ix,iy,k)'-b(ix,iy,k)'-c(ix,iy,k)');cc1=0.05*max(abs(caxis));caxis([-cc1 cc1]);
               mythincolorbar;grid;title('ocean tend-hconv-zconv, all z');
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);
  subplot(245);mypcolor(ix,iy,d(ix,iy,k)');cc1=0.04*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ice Salt tend']);
  subplot(246);mypcolor(ix,iy,f(ix,iy,k)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid;
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ice Salt hconv']);
  subplot(247);mypcolor(ix,iy,g(ix,iy,k)');cc1=0.1*max(abs(caxis));caxis([-cc1 cc1]);mythincolorbar;grid
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);title(['ice Salt zconv']);
  subplot(248);mypcolor(ix,iy,d(ix,iy,k)'-f(ix,iy,k)'-g(ix,iy,k)');cc1=0.05*max(abs(caxis));caxis([-cc1 cc1]);
               mythincolorbar;grid;title('ice tend-hconv-zconv');
         shadeland(ix,iy,isnan(msk(ix,iy)'),[.7 .7 .7]);
figure(4);set(gcf,'paperunit','inches','paperposition',[0 0 14 9]);
fpr=[dirOut 'Salt_budget4' extfpr '.png'];print(fpr,'-dpng');
%/*}}}*/
fclose(fid);
%/*{{{*/
%conserve for rStar

%%load(['/net/nares/raid11/ecco-shared/ecco-version-4/output/r4/iter9/mat/diags_set_D_' num2str(t2) '.mat'],...
%load([dirIn '../../mat/diags_set_D/diags_set_D_' num2str(t2) '.mat'],...
%      'glo_heat_ice','glo_heat_ocn','glo_heat_tot');
%glo_heat_tot-budgetOI			%[0.288657986402541  -0.124384150538271   0.612843109593086] x 10^-13
%


%now plot profiles of each component to understand why ther's a "profile" of imbalance near the top
%momhdivS=read_slice([dirIn 'budg3d_zflux_set3.' t2str '.data'],nx,ny,[1:nz]+nz,'real*8');%psu.*m/s, vertical vel due to hconv
%TOTSTEND=read_slice([dirIn 'budg3d_zflux_set3.' t2str '.data'],nx,ny,[1:nz]+4*nz,'real*8');%psu/day, vertical vel due to hconv
%trW=momhdivS.*myparms.rhoconst.*RAC3;
%TOTSTEND=TOTSTEND.*24*3600.*myparms.rhoconst.*RAC3;	%g/s/m3/*}}}*/
