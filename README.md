# GCMFACES #

This repository is for the adaptation of gcmfaces to work on regional data sets without the 
excessive memory requirements imposed by the trick of expanding the data to the global grid before
performing calculations.

Before committing code, please test results for diagnostics against the old version to ensure
that they match.

For questions about the inner workings of gcmfaces, An is the best person to ask as she's
responsible for most of the modifications already made to the original version.

------
ATN 11/Oct/2017

Three stand-alone scripts can now be used from scratch to load in grid (still calls for mygrid but essentially does not need) and perform budgets in compact / regional face format instead of expanding to global.  These are modifications to gcmfaces/gcmfaces_diags/diags_set_D.m and gcmfaces_calc/calc_budget_[mass,heat,salt].m.  Modifications in the form of a new mask that masks out where obcs are are now applied to close budgets for regional domains.
do_gcmfaces_budget_mass.m
do_gcmfaces_budget_heat.m
do_gcmfaces_budget_salt.m

Some dependency, found in toolbox/ and toolbox/gcmfaces_mod:
grid_load_raw.m
rdmds_list_fld.m
calc_UV_conv_mod.m
llc_uv_addpadding.m
read_slice.m

-----
ATN 12/Oct/2017

Latest gcmfaces from An (last major update 11/Jul/2017).  Major hack to read
in daily record one slice at a time and remain in compact format:
gcmfaces/ecco_v4/
cost_altimeter.m
cost_sst.m
cost_seaicearea.m
cost_xx.m
cost_adxx.m

------
Quick note to get things started for beginner, to be able to checkout/clone this repository and commit:

%first, initialize git on your local machine:
git config --global user.name "Your Name"
git config --global user.email email@university <-- email you used when registered with bitbucket for the academic account
git init
	(above command "Initialized empty Git repository in /home/username/.git/" )
git clone https://bitbucket.org/crios-ices/crios-faces
cd crios-faces/
cp /local_directory/do_gcmfaces_budget_* ./

git status
	(shows red files, "Untracked files:")
git add *.m

git status
	(shows green files, "Changes to be committed:")

git commit
	(add comment on what this commit is about, then quit the vim session)
	[master ff608ac] First version of working code for llc270 ASTE
	 3 files changed, 1776 insertions(+)
	 create mode 100644 do_gcmfaces_budget_heat.m
	 create mode 100644 do_gcmfaces_budget_mass.m
	 create mode 100644 do_gcmfaces_budget_salt.m

git status
	On branch master
	Your branch is ahead of 'origin/master' by 1 commit.
	  (use "git push" to publish your local commits)
	nothing to commit, working tree clean

git push
(enter username and pswd)

