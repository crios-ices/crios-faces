function [up,vp]=llc_uv_addpadding(u,v,nfx,nfy)
%function [up,vp]=llc_uv_addpadding(u,v,nfx,nfy)
%
%this is the equivalence of gcmfaces/gcmfaces_exch/exch_T_N_llc.m
%but works on regional faces instead of expanding to global.
%
%Purpose: to pad an extra column/row (at the end) to the U/V fields
% to enable computation of gradients u(2,:,:)-u(1,:,:) or v(:,2,:)-v(:,1,:)
%
%Input:
%  u and v: in compact format
%  nfx: size of xdim of the 5 faces [90 90 90 270 270] for global llc90 for example,
%                                   [270 0 270 180 450] for ASTE llc270 for example,
%  nfy: size of ydim of the 5 faces [270 270 90 90 90] for global llc90
%                                   [450 0 270 270 270] for ASTE llc270.
%
%Output:
%  cells type u{1} to u{5} and v{1} to v{5}, example below for llc90 global
%  u{1}: [91 x 270 x nz]   v{1}: [90 x 271 x nz]
%  u{2}: [91 x 270 x nz]   v{2}: [90 x 271 x nz]
%  u{3}: [91 x 90  x nz]   v{3}: [90 x 91  x nz]
%  u{4}: [271 x 90 x nz]   v{4}: [270 x 91 x nz]
%  u{5}: [271 x 90 x nz]   v{5}: [270 x 91 x nz]
%
% or for ASTE llc270:
%  u{1}: [271 x 450 x nz]   v{1}: [270 x 451 x nz]
%  u{2}: [  0 x   0 x  0]   v{2}: [  0 x   0 x  0]
%  u{3}: [271 x 270 x nz]   v{3}: [270 x 271 x nz]
%  u{4}: [181 x 270 x nz]   v{4}: [180 x 271 x nz]
%  u{5}: [451 x 270 x nz]   v{5}: [450 x 271 x nz]
%
% atn 11/Oct/2017

sz=size(u);%compact format
if(length(sz)==2);sz=[sz 1];end;
nx=sz(1);ny=sz(2);nz=sz(3);	%270 1350 50 or 270 270*13 50 or 90 90*13 13
%if(nx==270&ny==1350);
%  nfx=[nx 0 nx 180 450];nfy=[450 0 nx nx nx];
%elseif(ny==(nx*13));
%  nfx=[nx nx nx 3*nx 3*nx];nfy=[3*nx 3*nx nx nx nx];
%end;

uf{1}=u(:,              1:    nfy(1)   ,1:nz);
uf{2}=u(:,    nfy(1)   +1:sum(nfy(1:2)),1:nz);
uf{3}=u(:,sum(nfy(1:2))+1:sum(nfy(1:3)),1:nz);
uf{4}=reshape(u(:,sum(nfy(1:3))       +1:sum(nfy(1:3))+    nfx(4)   ,1:nz),nfx(4),nfy(4),nz);
uf{5}=reshape(u(:,sum(nfy(1:3))+nfx(4)+1:sum(nfy(1:3))+sum(nfx(4:5)),1:nz),nfx(5),nfy(5),nz);

vf{1}=v(:,              1:    nfy(1)   ,1:nz);
vf{2}=v(:,    nfy(1)+   1:sum(nfy(1:2)),1:nz);
vf{3}=v(:,sum(nfy(1:2))+1:sum(nfy(1:3)),1:nz);
vf{4}=reshape(v(:,sum(nfy(1:3))+       1:sum(nfy(1:3))+    nfx(4)   ,1:nz),nfx(4),nfy(4),nz);
vf{5}=reshape(v(:,sum(nfy(1:3))+nfx(4)+1:sum(nfy(1:3))+sum(nfx(4:5)),1:nz),nfx(5),nfy(5),nz);


clear u v
for iface=1:5
  sz=size(uf{iface});if(length(sz)==2);sz=[sz 1];end;
  clear i0;i0=find(sz==0);
  if(length(i0)>0);
    u{iface}=[];
  else;
    u{iface}=zeros(sz(1)+1,sz(2)  ,sz(3));
    u{iface}(1:sz(1),1:sz(2),1:sz(3))=uf{iface};
  end;
  sz=size(vf{iface});if(length(sz)==2);sz=[sz 1];end;
  clear i0;i0=find(sz==0);
  if(length(i0)>0);
    v{iface}=[];
  else;
    v{iface}=zeros(sz(1)  ,sz(2)+1,sz(3));
    v{iface}(1:sz(1),1:sz(2),1:sz(3))=vf{iface};
  end;
end

%now u edge:
  if(nfx(2)>0&nfy(2)>0);
    clear ut;ut=u{2}(1,1:nfy(2),1:nz);u{1}(nfx(1)+1,1:nfy(1),1:nz)=ut;			%1&2 length must match
  end;
  clear ut;ut=sym_g_mod(v{4}(1:nfx(4),1,1:nz),7,0);
   if(nfx(2)>0&nfy(2)>0);
         if(nfx(4)>nfy(2));
           utp=zeros(1,nfy(2),nz);utp(1,:,:)=ut(nfx(4)-nfy(2)+1:nfx(4),1:n);
     elseif(nfx(4)<nfy(2));
           utp=zeros(1,nfy(2),nz);utp(1,nfy(2)-nfx(4)+1:nfy(2),1:nz)=ut;
     else;
           utp=ut;
     end;
     u{2}(nfx(2)+1,1:nfy(2),1:nz)=utp;
   end;
  clear ut;ut=u{4}(1,1:nfy(4),1:nz);u{3}(nfx(3)+1,1:nfy(3),1:nz)=ut;
%will not fill the last cells nx*3+1 for u{4} and u{5} because they're land, so assume zeros

%now v edge: (a bit more complicated that u)
  clear vt;vt=sym_g_mod(u{3}(1,1:nfy(3),1:nz),5,0);v{1}(1:nfx(1),nfy(1)+1,1:nz)=vt;
  if(nfx(2)>0&nfy(2)>0);
    clear vt;vt=v{3}(1:nfx(3),1,1:nz);v{2}(1:nfx(2),nfy(2)+1,1:nz)=vt;
  end;
  clear vt;vt=sym_g_mod(u{5}(1,1:nfy(5),1:nz),5,0);v{3}(1:nfx(3),nfy(3)+1,1:nz)=vt;
  clear vt;vt=v{5}(1:nfx(5),1,1:nz);
  if(nfx(5)>=nfx(4))
    v{4}(1:nfx(4),nfy(4)+1,1:nz)=vt(1:nfx(4),1,1:nz);
  elseif(nfx(5)<nfx(4));
    v{4}(1:nfx(5),nfy(4)+1,1:nz)=vt(1:nfx(5),1,1:nz);
  end;
  clear vt;vt=sym_g_mod(u{1}(1,1:nfy(1),1:nz),5,0);v{5}(1:nfx(5),nfy(5)+1,1:nz)=vt;

vp=v;
up=u;

return
