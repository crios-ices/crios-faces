function [fldOut]=rdmds_list_fld(fileName,varName,dirIn,nx,ny,nz,tt)
% function [fldOut]=rdmds_list_fld(fileName,varName,dirIn,nx,ny,nz,tt)
%varName: list of fields to read in
%[nx ny nz]: size of compact field
%tt: time-steps of files to read

%get meta:
%fileName='budg2d_snap_set1';
meta=rdmds_meta(fileName,dirIn);

%determine precision:
if strcmp(meta.dataprec,'float32')
  precIn='real*4';
elseif(strcmp(meta.dataprec,'float64'));
  precIn='real*8';
else;
  error('meta shows unknown dataprec');
end;

%trim fldList:
for k=1:length(meta.fldList)
  tmp=strtrim(meta.fldList(k));
  meta.fldList(k)=tmp;
end;

%varName={'ETAN','SIheff','SIhsnow','SIarea','sIceLoad','PHIBOT'};
%make sure varName is structure
aa=whos('varName');
if(strcmp(aa.class,'char'));varName={varName};end;

for ifld=1:length(varName)
  tmp=strfind(strtrim(meta.fldList),varName{ifld});
  cc=0;
  for kslice=1:length(tmp);
    if ~isempty(tmp{kslice})
      cc=cc+1;
      for it=1:length(tt)
        tsstr=sprintf('%10.10i',tt(it));
        tmp1=read_slice([dirIn fileName '.' tsstr '.data'],nx,ny,[1:nz]+(kslice-1)*nz,precIn);
        eval(['fldOut.' strtrim(varName{ifld}) '(:,:,:,it)=tmp1;']);
        fprintf('%i ',kslice);fprintf('%s ',varName{ifld});
        if(ifld==1);fldOut.tt(it)=tt(it);end;
      end;
    end;
  end;
  if(cc==0);
    fprintf('missing %s ',varName{ifld});
    for it=1:length(tt);
      eval(['fldOut.' strtrim(varName{ifld}) '(:,:,:,it)=zeros(nx,ny,nz);']);
    end;
  end
end;
fprintf('\n');

