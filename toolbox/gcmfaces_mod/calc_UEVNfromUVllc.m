function [fldUe,fldVn]=calc_UEVNfromUVllc(fldU,fldV,acs,asn,flagEN);
%-----------------------------------------------------------------
%function [fldUe,fldVn]=calc_UEVNfromUVllc(fldU,fldV,acs,asn,flagEN);
%
% cp ~/matlab_gcmfaces/gcmfaces/gcmfaces_calc/calc_UEVNfromUXVY.m
%
%object:    compute Eastward/Northward vectors form X/Y vectors
%inputs:    fldU/fldV are the llc vector fields at velocity points
%            where ALREADY mult fldU=fldU.*hFacW & fldV=fldV.*hFacS
%            asn,acs are AngleSN/CS, 5 faces
%		  flagEN: 1 for getting uE,vN, 0 for returning centered U/V
%
%outputs:   fldUe/fldVn are the Eastward/Northward vectors at tracer points
%            when flagEN = 1;
%           fldUc/fldVc are the llc centered U/V at tracer points
%		   when flagEN = 0;
%-----------------------------------------------------------------

%fldU(mygrid.hFacW==0)=NaN; fldV(mygrid.hFacS==0)=NaN;
nr=size(fldU{1},3); 

[FLDU,FLDV]=exch_UV_llc_mod(fldU,fldV);

fldUe=fldU; fldVn=fldV;
for iF=1:length(fldU); 
  tmp1=FLDU{iF}(1:end-1,:,:); tmp2=FLDU{iF}(2:end,:,:);
  fldUe{iF}=reshape(nanmean([tmp1(:) tmp2(:)],2),size(tmp1));
  tmp1=FLDV{iF}(:,1:end-1,:); tmp2=FLDV{iF}(:,2:end,:);
  fldVn{iF}=reshape(nanmean([tmp1(:) tmp2(:)],2),size(tmp1));
end;

if(flagEN==1);
  FLDU=fldUe; FLDV=fldVn;
  cs=mk3D_mod(acs,FLDU); sn=mk3D_mod(asn,FLDU);

  for iF=1:length(FLDU);
    fldUe{iF}=FLDU{iF}.*cs{iF}-FLDV{iF}.*sn{iF};
    fldVn{iF}=FLDU{iF}.*sn{iF}+FLDV{iF}.*cs{iF};
  end;
end;

