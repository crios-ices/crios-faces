function []=grid_load_mod(dirGrid,nFaces,fileFormat,list23D,list1D,omitNativeGrid);
%object:    load grid information, convert it to gcmfaces format
%           and encapsulate it in the global mygrid structure.
%inputs:    dirGrid is the directory where the grid files (gcm output) can be found.
%           nFaces is the number of faces in this gcm set-up of current interest.
%           fileFormat is the file format ('straight','cube','compact')
%list23D: {'XC','YC','hFacC',etc}
%list1D : {'DRF','RF',etc}
%optional:
%           omitNativeGrid is a flag (0 by default) to bypass (when flag 1)
%               grid_load_native and grid_load_native_RAZ calls (that can
%               complement mygrid based upon e.g. tile*.mitgrid files)
% mod 13-Jun-2012, atn, load less fields

%input_list_check('grid_load',nargin);

if isempty(whos('memoryLimit')); memoryLimit=0; end;
if isempty(whos('omitNativeGrid')); omitNativeGrid=0; end;

gcmfaces_global; mygrid=[];

mygrid.dirGrid=dirGrid;
mygrid.nFaces=nFaces;
mygrid.fileFormat=fileFormat;
mygrid.gcm2facesFast=false;
mygrid.memoryLimit=memoryLimit;
mygrid.missVal=NaN;

if ~isempty(dir([dirGrid 'grid.specs.mat']));
    specs=open([dirGrid 'grid.specs.mat']);
    mygrid.ioSize=specs.ioSize;
    mygrid.facesSize=specs.facesSize;
%check and expand only if in aste, if global, set to empty:
    if(specs.facesSize(1,2)==specs.facesSize(1,1)*3);
      mygrid.facesExpand=[];
    else;
      mygrid.facesExpand=specs.facesExpand;
    end;
    %example for creating grid.specs.mat, to put in dirGrid :
    %ioSize=[364500 1];
    %facesSize=[[270 450];[0 0];[270 270];[180 270];[450 270]];
    %facesExpand=[270 450];
    %save grid.specs.mat ioSize facesSize facesExpand;
elseif strcmp(fileFormat,'compact');
    v0=rdmds([dirGrid 'XC']);
    mygrid.ioSize=size(v0);
    nn=size(v0,1); pp=size(v0,2)/nn;
    mm=(pp+4-mygrid.nFaces)/4*nn;
    mygrid.facesSize=[[nn mm];[nn mm];[nn nn];[mm nn];[mm nn];[nn nn]];
    mygrid.facesExpand=[];
elseif strcmp(fileFormat,'cube');
    v0=rdmds([dirGrid 'XC']);
    mygrid.ioSize=size(v0);
    nn=size(v0,2);
    mygrid.facesSize=[[nn nn];[nn nn];[nn nn];[nn nn];[nn nn];[nn nn]];
    mygrid.facesExpand=[];
elseif strcmp(fileFormat,'straight');
    v0=rdmds([dirGrid 'XC']);
    mygrid.ioSize=size(v0);
    mygrid.facesSize=mygrid.ioSize;
    mygrid.facesExpand=[];
end;

if  ~(nFaces==1&strcmp(fileFormat,'straight'))&...
        ~(nFaces==6&strcmp(fileFormat,'cube'))&...
        ~(nFaces==6&strcmp(fileFormat,'compact'))&...
        ~(nFaces==5&strcmp(fileFormat,'compact'));
%     if myenv.verbose;
%         fprintf('\nconvert2gcmfaces.m init: there are several supported file conventions. \n');
%         fprintf('  By default gcmfaces assumes MITgcm type binary formats as follows: \n')
%         fprintf('  (1 face) straight global format; (4 or 5 faces) compact global format\n');
%         fprintf('  (6 faces) cube format with one face after the other. \n');
%         fprintf('  If this is inadequate, you can change the format below.\n\n');
%     end;
    error('non-tested topology/fileFormat');
end;

%the various grid fields:%{'XC','YC','RAC','DXC','DYC','hFacC','Depth'};
list0=list23D;
for iFld=1:length(list0);
   eval(['mygrid.' list0{iFld} '=rdmds2gcmfaces([dirGrid ''' list0{iFld} '*'']);']);
end;

list0=list1D;
for iFld=1:length(list0);
   eval(['mygrid.' list0{iFld} '=squeeze(rdmds([dirGrid ''' list0{iFld} '*'']));']);
end;

%list0={'AngleCS','AngleSN'};
%test0=~isempty(dir([dirGrid 'AngleCS*']));
%if test0;
%  for iFld=1:length(list0);
%     eval(['mygrid.' list0{iFld} '=rdmds2gcmfaces([dirGrid ''' list0{iFld} '*'']);']);
%  end;
%else;
%  warning('\n AngleCS/AngleSN not found; set to 1/0 assuming lat/lon grid.\n');
%  mygrid.AngleCS=mygrid.XC; mygrid.AngleCS(:)=1; 
%  mygrid.AngleSN=mygrid.XC; mygrid.AngleSN(:)=0;
%end;

test0=sum(isnan(mygrid.XC))>0;
test1=prod(mygrid.ioSize)~=sum(isnan(NaN*mygrid.XC(:)));
if (test0|test1)&~omitNativeGrid;
  %treat fields that are part of the native grid

%atn 04.Sep.2014: insert to take care of blank tiles for delaunay triangulation
  mygrid.mskC2Dblank=isnan(mygrid.XC);
%  if(mygrid.memoryLimit<2);
  if(isfield(mygrid,'hFacC')==1);
    mygrid.mskCblank=isnan(mygrid.hFacC);
%    if(mygrid.memoryLimit<1);
    if(isfield(mygrid,'hFacS')==1);
      mygrid.mskSblank=isnan(mygrid.hFacS);
      mygrid.mskWblank=isnan(mygrid.hFacW);
    end;
  end;
%atn --- done ---------

  mygrid1=mygrid; mygrid=[];
  grid_load_native(dirGrid,nFaces,0);
  mygrid1.XC=mygrid.XC; mygrid1.YC=mygrid.YC;
  mygrid1.XG=mygrid.XG; mygrid1.YG=mygrid.YG;
  mygrid1.RAC=mygrid.RAC;
  mygrid=mygrid1;
  %apply missing value for fields that aren't
  list0={'hFacC','hFacS','hFacW','Depth','AngleCS','AngleSN'};
  for ii=1:length(list0);
    if(isfield(mygrid,list0{ii}));
      eval(['tmp1=mygrid.' list0{ii} ';']);
      tmp1(isnan(tmp1))=0;
      eval(['mygrid.' list0{ii} '=tmp1;']);
    end;
  end;
  %and fix angles if needed
  if(isfield(mygrid,'AngleCS') & isfield(mygrid,'AngleSN'));
    tmp1=mygrid.AngleCS.^2+mygrid.AngleSN.^2;
    tmp1=1*(tmp1>0.999&tmp1<1.001);
    mygrid.AngleCS(tmp1==0)=1;
    mygrid.AngleSN(tmp1==0)=0;
  end;
end;

%{'RC','RF','DRC','DRF'};
%masks:
%mygrid.hFacCsurf=mygrid.hFacC;
%for ff=1:mygrid.hFacC.nFaces; mygrid.hFacCsurf{ff}=mygrid.hFacC{ff}(:,:,1); end;
%
%mskC=mygrid.hFacC; mskC(mskC==0)=NaN; mskC(mskC>0)=1; mygrid.mskC=mskC;
%mskW=mygrid.hFacW; mskW(mskW==0)=NaN; mskW(mskW>0)=1; mygrid.mskW=mskW;
%mskS=mygrid.hFacS; mskS(mskS==0)=NaN; mskS(mskS>0)=1; mygrid.mskS=mskS;
%

%to allow convert2gcmfaces/doFast: 
if isempty(mygrid.facesExpand)&mygrid.memoryLimit<2;
   tmp1=convert2gcmfaces(mygrid.XC);
   tmp1(:)=[1:length(tmp1(:))];
   nn=length(tmp1(:));
   mygrid.gcm2faces=convert2gcmfaces(tmp1);
   mygrid.faces2gcmSize=size(tmp1);
   mygrid.faces2gcm=convert2gcmfaces(tmp1);
   for iFace=1:mygrid.nFaces;
     n=length(mygrid.gcm2faces{iFace}(:));
     mygrid.faces2gcm{iFace}=mygrid.gcm2faces{iFace}(:);
     mygrid.gcm2faces{iFace}=sparse([1:n],mygrid.gcm2faces{iFace}(:),ones(1,n),n,nn);
   end;
   mygrid.gcm2facesFast=true;
end;

%reset missVal parameter to 0. 
%Note : this is only used by convert2widefaces, for runs with cropped grids.
%Note : 0 should not be used as a fill for the grid itself (NaN was used).
mygrid.missVal=0;
