% ATN 06.Feb.2012
% copy function gcmfaces_exch/exch_UV_N_llc.m
% but allow exchange U/V faces instead of just
% padding the edges using swapflag; When swapflag=1, 
% the output [FLDU,FLDV] can not be used
% with AngleCS/NS.
% Already centered.
% swapflag=0 -> use with AngleCS/NS to get uEvN
% swapflag=1 -> use similar to in get_aste_vector to view unrotated U/V

function [FLDU,FLDV]=exch_UV_N_llc_atn(fldU,fldV,swapflag,varargin);

if nargin==4; N=varargin{1}; else; N=1; end;

FLDUtmp=exch_T_N_llc_atn(fldU,N);
FLDVtmp=exch_T_N_llc_atn(fldV,N);

U=FLDUtmp;
V=FLDVtmp;

U{1}(1:N,:,:)=FLDVtmp{1}(1:N,:,:);
V{1}(1:N,:,:)=-FLDUtmp{1}(1:N,:,:);
U{1}(:,end-N+1:end,:)=-FLDVtmp{1}(:,end-N+1:end,:);
V{1}(:,end-N+1:end,:)=FLDUtmp{1}(:,end-N+1:end,:);

U{2}(end-N+1:end,:,:)=FLDVtmp{2}(end-N+1:end,:,:);
V{2}(end-N+1:end,:,:)=-FLDUtmp{2}(end-N+1:end,:,:);

U{3}(1:N,:,:)=FLDVtmp{3}(1:N,:,:);
V{3}(1:N,:,:)=-FLDUtmp{3}(1:N,:,:);
U{3}(:,end-N+1:end,:)=-FLDVtmp{3}(:,end-N+1:end,:);
V{3}(:,end-N+1:end,:)=FLDUtmp{3}(:,end-N+1:end,:);

U{4}(:,1:N,:)=-FLDVtmp{4}(:,1:N,:);
V{4}(:,1:N,:)=FLDUtmp{4}(:,1:N,:);

U{5}(1:N,:,:)=FLDVtmp{5}(1:N,:,:);
V{5}(1:N,:,:)=-FLDUtmp{5}(1:N,:,:);
U{5}(:,end-N+1:end,:)=-FLDVtmp{5}(:,end-N+1:end,:);
V{5}(:,end-N+1:end,:)=FLDUtmp{5}(:,end-N+1:end,:);

% now do exchange U/V if swapflag==1:

FLDU=fldU;FLDV=fldV;

FLDU{1}= (U{1}(2:end-1,2:end-1,:)+U{1}(3:end  ,2:end-1,:))./2;
FLDU{2}= (U{2}(2:end-1,2:end-1,:)+U{2}(3:end  ,2:end-1,:))./2;

FLDV{1}= (V{1}(2:end-1,2:end-1,:)+V{1}(2:end-1,3:end,:))./2;
FLDV{2}= (V{2}(2:end-1,2:end-1,:)+V{2}(2:end-1,3:end,:))./2;

if(swapflag==0);
  FLDV{3}=-(V{3}(2:end-1,2:end-1,:)+V{3}(2:end-1,3:end,:))./2;
  FLDV{4}= (V{4}(2:end-1,2:end-1,:)+V{4}(2:end-1,3:end,:))./2;
  FLDV{5}= (V{5}(2:end-1,2:end-1,:)+V{5}(2:end-1,3:end,:))./2;

  FLDU{3}= (U{3}(2:end-1,2:end-1,:)+U{3}(3:end,2:end-1,:))./2;
  FLDU{4}=-(U{4}(2:end-1,2:end-1,:)+U{4}(3:end,2:end-1,:))./2;
  FLDU{5}=-(U{5}(2:end-1,2:end-1,:)+U{5}(3:end,2:end-1,:))./2;
elseif(swapflag==1);
  FLDU{3}=-(V{3}(2:end-1,2:end-1,:)+V{3}(2:end-1,3:end,:))./2;
  FLDU{4}= (V{4}(2:end-1,2:end-1,:)+V{4}(2:end-1,3:end,:))./2;
  FLDU{5}= (V{5}(2:end-1,2:end-1,:)+V{5}(2:end-1,3:end,:))./2;

  FLDV{3}= (U{3}(2:end-1,2:end-1,:)+U{3}(3:end,2:end-1,:))./2;
  FLDV{4}=-(U{4}(2:end-1,2:end-1,:)+U{4}(3:end,2:end-1,:))./2;
  FLDV{5}=-(U{5}(2:end-1,2:end-1,:)+U{5}(3:end,2:end-1,:))./2;
end;
