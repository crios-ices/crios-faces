function fldOut=calc_UV_conv_mod(U,V,nfx,nfy)
% function fldOut=calc_UV_conv_mod(U,V)
%
% Modification of calc_UV_conv to deal with only compact format and faces, not gcmfaces
% Input: 
% [U,V]: the output of llc_uv_padding , should be as cell u{1} to u{5} and v{1} to v{5}
% [nfx,nfy]: dimensions in x and y , see llc_uv_addpadding for explanation
%
% Output:
%  fldOut: convergence field (scalar), cell structure with 5 faces
%  example of llc90 global    or  llc270-ASTE:
%  fldOut{1}: [90 x 270 x nz] or [270 x 450 x nz]
%        {2}: [90 x 270 x nz] or [  0 x   0 x  0]
%        {3}: [90 x  90 x nz] or [270 x 270 x nz]
%        {4}: [270 x 90 x nz] or [180 x 270 x nz]
%        {5}: [270 x 90 x nz] or [450 x 270 x nz]
%
% atn 11/Oct/2017

[up,vp]=llc_uv_addpadding(U,V,nfx,nfy);

%add 1 more column to rightmost of U and 1 more row to top of V
for k=1:length(nfx);
  sz=size(vp{k});
  if(length(sz)==2);sz=[sz 1];end;
  if(sz(1)>0&sz(2)>0);
    vc{k}(1:sz(1),1:sz(2)-1,1:sz(3))=(vp{k}(:,1:sz(2)-1,1:sz(3))-vp{k}(:,2:sz(2),1:sz(3)));
  else;
    vc{k}=[];
  end;

  sz=size(up{k});
  if(length(sz)==2);sz=[sz 1];end;
  if(sz(1)>0&sz(2)>0);
    uc{k}(1:sz(1)-1,1:sz(2),1:sz(3))=(up{k}(1:sz(1)-1,:,1:sz(3))-up{k}(2:sz(1),:,1:sz(3)));
  else;
    uc{k}=[];
  end;

%sum up convergence:
  fldOut{k}=uc{k}+vc{k};

end;

%do this outside, so that the function can be more general
%put back into compact format:
%fldOut=cat(2,tmp{1},tmp{3},reshape(tmp{4},nfy(4),nfx(4),nz),reshape(tmp{5},nfy(5),nfx(5),nz));

