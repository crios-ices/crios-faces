% copy from ~/matlab_gcmfaces/gcmfaces/gcmfaces_exch/exch_UV_llc.m
% switch gcmfaces to cell
function [FLDU,FLDV]=exch_UV_llc_mod(fldU,fldV);

FLDUtmp=exch_T_N_llc_mod(fldU);
FLDVtmp=exch_T_N_llc_mod(fldV);

FLDU=FLDUtmp;
FLDV=FLDVtmp;

FLDU{1}=FLDUtmp{1}(2:end,2:end-1,:);  
FLDV{1}=FLDVtmp{1}(2:end-1,2:end,:);
FLDV{1}(:,end,:)=FLDUtmp{1}(2:end-1,end,:);

FLDU{2}=FLDUtmp{2}(2:end,2:end-1,:);    
FLDU{2}(end,:,:)=FLDVtmp{2}(end,2:end-1,:);
FLDV{2}=FLDVtmp{2}(2:end-1,2:end,:);

FLDU{3}=FLDUtmp{3}(2:end,2:end-1,:);
FLDV{3}=FLDVtmp{3}(2:end-1,2:end,:);
FLDV{3}(:,end,:)=FLDUtmp{3}(2:end-1,end,:);

FLDU{4}=FLDUtmp{4}(2:end,2:end-1,:);
FLDV{4}=FLDVtmp{4}(2:end-1,2:end,:);

FLDU{5}=FLDUtmp{5}(2:end,2:end-1,:);
FLDV{5}=FLDVtmp{5}(2:end-1,2:end,:);
FLDV{5}(:,end,:)=FLDUtmp{5}(2:end-1,end,:);

