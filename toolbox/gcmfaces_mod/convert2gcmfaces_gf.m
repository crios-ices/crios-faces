function [v1]=convert2gcmfaces(v0,varargin);
%object:    converts model output to gcmfaces object 
%           or vice versa
%input:     v0 model output array (resp. gcmfaces object)
%output:    v1 gcmfaces object (model output array)
%
%note:      global mygrid parameters (nFaces, fileFormat) are used

input_list_check('convert2gcmfaces',nargin);

aa=whos('v0'); doGcm2Faces=strcmp(aa.class,'double');

global mygrid;

if doGcm2Faces&mygrid.gcm2facesFast;

  [n1,n2,n3,n4,n5]=size(v0);

  v0=reshape(v0,[n1*n2 n3*n4*n5]);
  for iFace=1:mygrid.nFaces;
    v1{iFace}=reshape(mygrid.gcm2faces{iFace}*v0,[size(mygrid.XC{iFace}) n3 n4 n5]);
  end;

  if mygrid.nFaces==1; gridType='ll'; 
  elseif mygrid.nFaces==5; gridType='llc'; 
  elseif mygrid.nFaces==6; gridType='cube'; 
  end;
  v1=gcmfaces(v1,gridType);

elseif ~doGcm2Faces&mygrid.gcm2facesFast;

  [n1,n2,n3,n4,n5]=size(v0{1});

  v1=zeros(mygrid.faces2gcmSize(1)*mygrid.faces2gcmSize(2),n3*n4*n5);
  for iFace=1:mygrid.nFaces;
    nn=size(mygrid.XC{iFace});
    v1(mygrid.faces2gcm{iFace},:)=reshape(v0{iFace},[nn(1)*nn(2) n3*n4*n5]);
  end;
  v1=reshape(v1,[mygrid.faces2gcmSize n3 n4 n5]);

elseif doGcm2Faces;

  [n1,n2,n3,n4,n5]=size(v0);

  if strcmp(mygrid.fileFormat,'straight');
    v1={v0};
  elseif strcmp(mygrid.fileFormat,'cube');
    for ii=1:6; v1{ii}=v0(n2*(ii-1)+[1:n2],:,:,:,:); end;
  elseif strcmp(mygrid.fileFormat,'compact');

    nn=size(v0,1); 
    pp=size(v0,2)/nn;
    mm=(pp+4-mygrid.nFaces)/4*nn; 

    mygrid.facesSize=[[540 810];[0 0];[540 540];[270 540];[810 540]];
    v00=reshape(v0,[n1*n2 n3*n4*n5]);
    i0=1;     nn=mygrid.facesSize(1,1); mm=mygrid.facesSize(1,2);
    i1=nn*mm; v1{1}=reshape(v00(i0:i1,:),[nn mm n3 n4 n5]);
    i0=i1+1;  nn=mygrid.facesSize(2,1); mm=mygrid.facesSize(2,2);
    i1=i1+nn*mm; v1{2}=reshape(v00(i0:i1,:),[nn mm n3 n4 n5]);
    i0=i1+1;  nn=mygrid.facesSize(3,1); mm=mygrid.facesSize(3,2);
    i1=i1+nn*mm; v1{3}=reshape(v00(i0:i1,:),[nn mm n3 n4 n5]);
    i0=i1+1;  nn=mygrid.facesSize(4,1); mm=mygrid.facesSize(4,2);
    i1=i1+nn*mm; v1{4}=reshape(v00(i0:i1,:),[nn mm n3 n4 n5]);
    i0=i1+1;  nn=mygrid.facesSize(5,1); mm=mygrid.facesSize(5,2);
    i1=i1+nn*mm; v1{5}=reshape(v00(i0:i1,:),[nn mm n3 n4 n5]);
    if mygrid.nFaces==6;
       i0=i1+1;  nn=mygrid.facesSize(6,1); mm=mygrid.facesSize(6,2);
       i1=i1+nn*mm; v1{6}=reshape(v00(i0:i1,:),[nn mm n3 n4 n5]);
    end;

  end;

  if mygrid.nFaces==1; gridType='ll'; 
  elseif mygrid.nFaces==5; gridType='llc'; 
  elseif mygrid.nFaces==6; gridType='cube'; 
  end;
  v1=gcmfaces(v1,gridType);

else;

  [n1,n2,n3,n4,n5]=size(v0{1});

  if strcmp(mygrid.fileFormat,'straight');
    v1=v0{1};
  elseif strcmp(mygrid.fileFormat,'cube');
    v1=zeros(n2*6,n2,n3,n4,n5);
    for ii=1:6; v1([1:n2]+(ii-1)*n2,:,:,:,:)=v0{ii}; end;
  elseif strcmp(mygrid.fileFormat,'compact');

    v0_faces=v0; clear v0; 
    for iFace=1:mygrid.nFaces; eval(['v0{iFace}=get(v0_faces,''f' num2str(iFace) ''');']); end; 

    nn=size(v0{1},1); mm=size(v0{1},2); 
    pp=mm/nn*4+mygrid.nFaces-4;

    n3=size(v0{1},3); n4=size(v0{1},4); n5=size(v0{1},5);
    v1=NaN*zeros(nn,nn*pp,n3,n4,n5);

    v11=NaN*zeros(nn*nn*pp,n3*n4*n5);
  
   i0=1; i1=nn*mm; tmp1=reshape(v0{1},[nn*mm n3*n4*n5]); v11(i0:i1,:)=tmp1(:,:);
   i0=i1+1; i1=i1+nn*mm; tmp1=reshape(v0{2},[nn*mm n3*n4*n5]); v11(i0:i1,:)=tmp1(:,:);
   i0=i1+1; i1=i1+nn*nn; tmp1=reshape(v0{3},[nn*nn n3*n4*n5]); v11(i0:i1,:)=tmp1(:,:);
   i0=i1+1; i1=i1+nn*mm; tmp1=reshape(v0{4},[mm*nn n3*n4*n5]); v11(i0:i1,:)=tmp1(:,:);
   i0=i1+1; i1=i1+nn*mm; tmp1=reshape(v0{5},[mm*nn n3*n4*n5]); v11(i0:i1,:)=tmp1(:,:);
   if mygrid.nFaces==6; 
      i0=i1+1; i1=i1+nn*nn; tmp1=reshape(v0{6},[nn*nn n3*n4*n5]); v11(i0:i1,:)=tmp1(:,:);
   end;

   v1=reshape(v11,[nn nn*pp n3 n4 n5]);

  end;
 
end;


