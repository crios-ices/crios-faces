function [U,V]=getuEvN_uXvY_llc(ui,vi,Acos,Asine,lon,lat,proj_ctr);
%------------------------------------------------------------------------------
% function [U,V]=getuEvN_uXvY2(ui,vi,Acos,Asine,proj_ctr)
%
% function to rotate cube_sphere [ui,vi] to [uE,vN] or [uX,vY], size [510,510]
%          for face 3 = ARCTIC, if face~=3, need to redefine u0,v0 in program.
%          Here, the last row of uE and last colm of vN are un-usable due to zero-padding!!
% input:
%  [ui,vi]  : velocity in llc coord, 5 faces (unprimed)
%  [lon,lat]: 5 faces, (unprimed)
%  [Acos,Asine]: rotational angles, 5 faces (unprimed)
%  proj_ctr : center longitude for llssmi1p
%           : [-180,180]
%           : if = -1000, no [X,Y] projection will be done, [uE,vN] are returned.
%  ATN 013007, mod 041607, mod 24Feb2012 for llc
%  NOTE: if use llssmi1p: need to use lon',lat',uE',vN'
%------------------------------------------------------------------------------

% going through each face one at a time
face=3;

u0=zeros(511,510);v0=zeros(510,511);
u0(1:510,:)=ui;v0(:,1:510)=vi;

%centering:
u1=(u0(1:510,:)+u0(2:511,:))./2;
v1=(v0(:,1:510)+v0(:,2:511))./2;
%rotating:
uE0=Acos.*u1-Asine.*v1;
vN0=Asine.*u1+Acos.*v1;

if(proj_ctr~=-1000);
  uE=uE0;%unprimed
  vN=vN0;%unprimed

  x=reshape(lon',510*510,1);y=reshape(lat',510*510,1);
  [uX,vY]=get_uXvY(reshape(uE',510^2,1),reshape(vN',510^2,1),x,y,proj_ctr);%primed
  U=reshape(uX,510,510);
  V=reshape(vY,510,510);
else;
  U=uE0;%			%unprimed
  V=vN0;%			%unprimed
end;

return
