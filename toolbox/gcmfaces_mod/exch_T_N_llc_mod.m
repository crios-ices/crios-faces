% cp from gcmfaces/gcmfaces_exch/exch_T_N_llc.m
function [FLD]=exch_T_N_llc_mod(fld,varargin);

if nargin==2; N=varargin{1}; else; N=1; end;

FLD=fld;
s=size(FLD{1}); s(1:2)=s(1:2)+2*N; FLD{1}=NaN*zeros(s);
s=size(FLD{2}); s(1:2)=s(1:2)+2*N; FLD{2}=NaN*zeros(s);
s=size(FLD{3}); s(1:2)=s(1:2)+2*N; FLD{3}=NaN*zeros(s);
s=size(FLD{4}); s(1:2)=s(1:2)+2*N; FLD{4}=NaN*zeros(s);
s=size(FLD{5}); s(1:2)=s(1:2)+2*N; FLD{5}=NaN*zeros(s);

n3=max(size(fld{1},3),1); n4=max(size(fld{1},4),1);
for k3=1:n3; for k4=1:n4;

%f1245=[fld{1}(:,:,k3,k4);fld{2}(:,:,k3,k4);sym_g(fld{4}(:,:,k3,k4),7,0);sym_g(fld{5}(:,:,k3,k4),7,0)];
%f3=fld{3}(:,:,k3,k4); f3=[sym_g(f3,5,0);f3;sym_g(f3,7,0);sym_g(f3,6,0)];

%initial rotation for "LATLON" faces:
f1=fld{1}(:,:,k3,k4); 
f2=fld{2}(:,:,k3,k4);
f4=sym_g(fld{4}(:,:,k3,k4),7,0);
f5=sym_g(fld{5}(:,:,k3,k4),7,0);

nan1=NaN*ones(size(FLD{1},1),N); 
nan2=NaN*ones(N,N);
%face 1:
F1=[f5(end-N+1:end,:);f1;f2(1:N,:)];
f3=sym_g(fld{3}(:,:,k3,k4),5,0);
F1=[nan1 F1 [nan2;f3(:,1:N);nan2]];
%face 2:
F2=[f1(end-N+1:end,:);f2;f4(1:N,:)];
f3=fld{3}(:,:,k3,k4);
F2=[nan1 F2 [nan2;f3(:,1:N);nan2]];
%face 4:
F4=[f2(end-N+1:end,:);f4;f5(1:N,:)];
f3=sym_g(fld{3}(:,:,k3,k4),7,0);
F4=[nan1 F4 [nan2;f3(:,1:N);nan2]];
%face 5:
F5=[f4(end-N+1:end,:);f5;f1(1:N,:)];
f3=sym_g(fld{3}(:,:,k3,k4),6,0);
F5=[nan1 F5 [nan2;f3(:,1:N);nan2]];
%face 3:
f3=fld{3}(:,:,k3,k4); F3=FLD{3}(:,:,k3,k4);
F3(1+N:end-N,1+N:end-N)=f3;
F3=sym_g(F3,5,0); F3(1+N:end-N,1:N)=f1(:,end-N+1:end); F3=sym_g(F3,7,0);
F3(1+N:end-N,1:N)=f2(:,end-N+1:end);
F3=sym_g(F3,7,0); F3(1+N:end-N,1:N)=f4(:,end-N+1:end); F3=sym_g(F3,5,0);
F3=sym_g(F3,6,0); F3(1+N:end-N,1:N)=f5(:,end-N+1:end); F3=sym_g(F3,6,0);

%final rotation for "LATLON" faces:
F4=sym_g(F4,5,0);
F5=sym_g(F5,5,0);
%store:
FLD{1}(:,:,k3,k4)=F1;
FLD{2}(:,:,k3,k4)=F2;
FLD{3}(:,:,k3,k4)=F3;
FLD{4}(:,:,k3,k4)=F4;
FLD{5}(:,:,k3,k4)=F5;

end; end;

