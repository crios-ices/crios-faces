function []=m_map_gcmfaces_uEvN_atn(fldUe,fldVn,varargin);

%keyboard
global mygrid;

if nargin>2; choicePlot=varargin{1}; else; choicePlot=0; end;
if nargin>3; scaleFac=varargin{2}; else; scaleFac=[]; end;
if nargin>4; subFac=varargin{3}; else; subFac=1;; end;

%[fldUe,fldVn]=calc_UEVNfromUXVY(fldU,fldV);

if choicePlot==-1;
  %%m_proj('Miller Cylindrical','lat',[-90 90]);
  %%m_proj('Equidistant cylindrical','lat',[-90 90]);
  m_proj('mollweide','lon',[-180 180],'lat',[-89 89]);
  %m_proj('mollweide','lon',[-100 20],'lat',[0 70]);

  [uu,vv]=m_map_gcmfaces_uvrotate(fldUe,fldVn);
  [xx,yy,u]=convert2pcol(mygrid.XC,mygrid.YC,uu);
  [xx,yy,v]=convert2pcol(mygrid.XC,mygrid.YC,vv);
  [x,y]=m_ll2xy(xx,yy);

%ger rid of nans and subsample:
  ii=find(~isnan(u.*v)); ii=ii(1:subFac:end);
  x=x(ii); y=y(ii); u=u(ii); v=v(ii);
  m_coast('patch',[1 1 1]*.7,'edgecolor','none'); m_grid;
  hold on; if isempty(scaleFac); quiver(x,y,u,v); else; quiver(x,y,u,v,scaleFac); end;
end;%if choicePlot==0|choicePlot==1; 

if choicePlot==0; subplot(2,1,1); end;
if choicePlot==0|choicePlot==1; 
  m_proj('Mercator','lon',[-90 270],'lat',[-20 80]);
  %m_proj('Robinson','lon',[-90 270],'lat',[-20 88]);

  XC=mygrid.XC;XC(find(XC<-90))=XC(find(XC<-90))+360;
  [uu,vv]=m_map_gcmfaces_uvrotate_atn(fldUe,fldVn);
  %[xx,yy,u]=convert2pcol(mygrid.XC,mygrid.YC,uu);
  %[xx,yy,v]=convert2pcol(mygrid.XC,mygrid.YC,vv);
  [xx,yy,u]=convert2pcol(XC,mygrid.YC,uu);
  [xx,yy,v]=convert2pcol(XC,mygrid.YC,vv);
  [x,y]=m_ll2xy(xx,yy);

  %ger rid of nans and subsample:
  ii=find(~isnan(u.*v)); ii=ii(1:subFac:end);
  x=x(ii); y=y(ii); u=u(ii); v=v(ii);
  m_coast('patch',[1 1 1]*.7,'edgecolor','none'); m_grid;
  hold on; if isempty(scaleFac); quiver(x,y,u,v); else; quiver(x,y,u,v,scaleFac); end;
end;%if choicePlot==0|choicePlot==1; 

if choicePlot==0; subplot(2,2,3); end;
if choicePlot==0|(choicePlot>=2&choicePlot<3); 
  if(choicePlot==0|choicePlot==2);
    m_proj('Stereographic','lon',0,'lat',90,'rad',40);
  elseif choicePlot==2.1;
    m_proj('Stereographic','lon',-45,'lat',70,'rad',60);
    yloc='bottom'; ytic=[50:10:90];
  elseif choicePlot==2.2;
    m_proj('Stereographic','lon',0,'lat',80,'rad',30);
  elseif choicePlot==2.3;
    m_proj('Stereographic','lon',0,'lat',90,'rad',30);
    ytic=[70,80];
  elseif choicePlot==2.4;
    m_proj('Stereographic','lon',-40,'lat',60,'rad',50);
  elseif choicePlot==2.5;
    m_proj('Stereographic','lon',180,'lat',60,'rad',65);
  elseif choicePlot==2.6;
    m_proj('Stereographic','lon',180,'lat',60,'rad',25);
  elseif choicePlot==2.7;
    m_proj('Stereographic','lon',-168,'lat',60,'rad',15);
  end;

  [uu,vv]=m_map_gcmfaces_uvrotate(fldUe,fldVn);
  xx=convert2arctic_atn(mygrid.XC,0);
  yy=convert2arctic_atn(mygrid.YC,0);
  u=convert2arctic_atn(uu);
  v=convert2arctic_atn(vv);
  [x,y]=m_ll2xy(xx,yy);

%ger rid of nans and subsample:
  ii=find(~isnan(u.*v)); ii=ii(1:subFac:end);
  x=x(ii); y=y(ii); u=u(ii); v=v(ii);
  m_coast('patch',[1 1 1]*.7,'edgecolor','none'); m_grid;
  hold on; if isempty(scaleFac); quiver(x,y,u,v,'color',.3.*[1 1 1]); else; quiver(x,y,u,v,scaleFac,'color',.3.*[1 1 1]); end;
end;%if choicePlot==0|choicePlot==1; 

if choicePlot==0; subplot(2,2,4); end;
if choicePlot==0|choicePlot==3; 
  m_proj('Stereographic','lon',0,'lat',-90,'rad',40);

  [uu,vv]=m_map_gcmfaces_uvrotate(fldUe,fldVn);
  xx=convert2southern(mygrid.XC,0);
  yy=convert2southern(mygrid.YC,0);
  u=convert2southern(uu);
  v=convert2southern(vv);
  [x,y]=m_ll2xy(xx,yy);

%ger rid of nans and subsample:
  ii=find(~isnan(u.*v)); ii=ii(1:subFac:end);
  x=x(ii); y=y(ii); u=u(ii); v=v(ii);
  m_coast('patch',[1 1 1]*.7,'edgecolor','none'); m_grid;
  hold on; if isempty(scaleFac); quiver(x,y,u,v); else; quiver(x,y,u,v,scaleFac); end;
end;%if choicePlot==0|choicePlot==1; 



