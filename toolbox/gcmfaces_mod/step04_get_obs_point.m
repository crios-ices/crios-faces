% note: ik is obtained with xg oriented such that when do imagesc(xg)
% we get: in yaxis: 0top to 180@200, -180@200 to 0@360
%         in xaxis: arctic at x~275to360, y~1to90
% aaaa
% aaa
% aaa
% this grid is different than the climatology grid. In my case it will be nx=540;
% mimicking gcmfaces_bindata.m

if(strcmp(GridStr,'CS510')==1);
  XCgrid=read_cs_face([dirGRIDRun,'XC.data'],3,1);
  YCgrid=read_cs_face([dirGRIDRun,'YC.data'],3,1);
else;
  precstr='real*4';temp=dir([dirGRIDRun 'XC.data']);if(temp.bytes/nx/ny/4==2);precstr='real*8';end;
  XCgrid=readbin([dirGRIDRun,'XC.data'],[nx ny],1,precstr);
  YCgrid=readbin([dirGRIDRun,'YC.data'],[nx ny],1,precstr);
end;

if(length(strfind(GridStr,'llc'))>0);
  if(strcmp(RegionStr,'global')==1);
    xgrid=patchface3D(nx,ny,nz_surf,XCgrid,2);
    ygrid=patchface3D(nx,ny,nz_surf,YCgrid,2);
    if(newTri);
      inan=nx+1:4*nx;jnan=3*nx+1:4*nx;
      xgrid(inan,jnan)=nan;ygrid(inan,jnan)=nan;
    end;
  elseif(strcmp(RegionStr,'aste_270x450x180')==1);
    xgrid=get_aste_tracer(XCgrid,nfx,nfy);
    ygrid=get_aste_tracer(YCgrid,nfx,nfy);
  end;
else;
  xgrid=XCgrid;
  ygrid=YCgrid;
end;

%---------------------
%fDelaunay=[dirGRIDRun 'TRI_Stri' num2str(newTri) '.mat'];
%if(strcmp(RegionStr,'aste_270x450x180')==1);
%  fDelaunay=[dirRoot GridStr '/' RegionStr '/GRID_astemod/' 'TRI_Stri' num2str(newTri) '.mat'];
%end;
%---------------------
%------ 01feb2014 retire block below, now use get_profpoint.m ----
%clear kvalid
%if(exist(fDelaunay)>0);
%   fprintf('%s ',fDelaunay);fprintf('exist, loading\n');
%   load(fDelaunay);
%else;
%   fprintf('%s ',fDelaunay);fprintf('does not exist, producing\n');
%%profiles_proces_main_v2/profiles_prep_locate.m, gcmfaces_calc/gcmfaces_bindata.m
%   if(oldTri==0);			%old matlab, ross/weddell
%      kvalid=1:prod(size(xgrid));
%      TRI=delaunay(xgrid(kvalid),ygrid(kvalid)); nxy = length(kvalid)
%      Stri=sparse(TRI(:,[1 1 2 2 3 3]),TRI(:,[2 3 1 3 1 2]),1,nxy,nxy);
%%usage: ik=dsearch(xgrid,ygrid,TRI,lon,lat,Stri);
%   elseif(newTri);			%new matlab, 2010+after release
%      kvalid=find(~isnan(xgrid));
%      TRI=DelaunayTri(xgrid(kvalid),ygrid(kvalid));
%      Stri=[];
%% usage: ik=TRI.nearestNeighbor(lon,lat);
%   end;
%   save(fDelaunay,'TRI','Stri','kvalid');
%   fprintf('saving %s\n',fDelaunay);
%end;
%---------------------

if(do_interp==1);
  MITprof.prof_point=get_profpoint(xgrid,ygrid,MITprof.prof_lon,MITprof.prof_lat,dirGRIDRun);
end;

if(get_basinidglobal==1);
  temp=readbin([dirBasin fBasin],[nx ny]);
  temp=patchface3D(nx,ny,1,temp,2);
  ib=zeros(size(MITprof.prof_point));
  ib=temp(MITprof.prof_point);
  %kk=find(ib==0&MITprof.prof_basin>0);temp1=MITprof.prof_basin(kk);
  MITprof.prof_basin=ib;
  %MITprof.prof_basin(kk)=temp1;
end;

%----- 01feb2014 retire block below, now use get_profpoint.m
%  if(exist('kvalid','var')==0);kvalid=1:prod(size(xgrid));end;
%  if(oldTri==0);
%%102288, same as prof_point(1) in asof_test.nc
%% new way to store using only non-nan values:
%    ik=dsearch(xgrid(kvalid),ygrid(kvalid),TRI,MITprof.prof_lon,MITprof.prof_lat,Stri);
%  elseif(newTri);
%    ik=TRI.nearestNeighbor(MITprof.prof_lon,MITprof.prof_lat);
%  end;
%  MITprof.prof_point = kvalid(ik);

% new related to tiles maybe:
% profiles_process_main_v2/profiles_prep_mygrid.m, gcmfaces_calc/gcmface_loc_tile.m
% syntax:
% ncload(fData,'prof_lon','prof_lat');
% loc_tile=gcmfaces_loc_tile(ni,nj,prof_lon,prof_lat);
% list_in={'XC11','YC11','XCNINJ','YCNINJ','iTile','jTile','XC','YC'};
% list_out={'XC11','YC11','XCNINJ','YCNINJ','i','j','lon','lat'};
%for iF=1:length(list_out);
%    eval(['myVec.prof_interp_' list_out{iF} '=loc_tile.' list_in{iF} ';']);
%end;
%%use 1 as weight since we do nearest neighbor interp
%myVec.prof_interp_weights=ones(size(loc_tile.XC)); list_out={list_out{:},'weights'};

%  clear TRI Stri kvalid
%end;
% remove clear statement below b/c need grid info for step04a
%clear XCgrid YCgrid xgrid fx ygrid fy nxy ik fDelaunay

