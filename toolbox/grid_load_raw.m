function mygrid=grid_load_raw(dirGrid)
%
%function mygrid=grid_load_raw(dirGrid)
% Absolute bare minimum for loading a grid.
% Assume that directory to gcmfaces are already set.
% If not, it will fail when looking for "convert2gcmfaces"
%

global mygrid

grid_load(dirGrid,5,'compact',2);
str={'Depth','DXG','DYG','DXC','DYC','RAC','AngleCS','AngleSN'};
for istr=1:size(str,2);
  eval(['tmp=rdmds([dirGrid ''' str{istr} ''']);']);
  tmp=reshape(tmp,mygrid.ioSize(1),mygrid.ioSize(2),1);
  eval(['mygrid.' str{istr} '=convert2gcmfaces(tmp);']);
end;

str={'hFacC','hFacS','hFacW'};
for istr=1:size(str,2);
  eval(['tmp=rdmds([dirGrid ''' str{istr} ''']);']);
  tmp=reshape(tmp,mygrid.ioSize(1),mygrid.ioSize(2),50);
  eval(['mygrid.' str{istr} '=convert2gcmfaces(tmp);']);
end;
tmp=nan.*mygrid.hFacC;tmp(find(mygrid.hFacC>0))=1;mygrid.mskC=tmp;
tmp=nan.*mygrid.hFacW;tmp(find(mygrid.hFacW>0))=1;mygrid.mskW=tmp;
tmp=nan.*mygrid.hFacS;tmp(find(mygrid.hFacS>0))=1;mygrid.mskS=tmp;

clear tmp

return

